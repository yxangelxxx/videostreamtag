﻿using System;

namespace GLog
{
	public class StrMsg : IMessage
	{
	    private string Message { get; set; }

		public StrMsg(string message)
		{
		    Message = message;
		}

		public override string ToString()
		{
			return Message;
		}
	}
}