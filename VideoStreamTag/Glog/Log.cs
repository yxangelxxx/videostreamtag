﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using GLog;

namespace Glog
{
    public static class Log
    {
        private static Logger _inst = Logger.CreateInstanceBaseConfig();

        public static void Config(ConfigLog configLog)
        {
            _inst = new Logger(configLog);
        }

        [Conditional("DEBUG")]
        public static void Write(string msg)
        {
            _inst.Write(msg);
        }

        [Conditional("DEBUG")]
        public static void Write(Exception ex,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string sourceFilePath = "",
            [CallerLineNumber] int sourceLineNumber = 0)
        {
            _inst.Write(ex, memberName, sourceFilePath, sourceLineNumber);
        }

        [Conditional("TRACE")]
        public static void Trace(string str)
        {
            _inst.Trace(str);
        }

        [Conditional("DEBUG")]
        public static void Func([CallerMemberName] string methodName = "")
        {
            _inst.Func(methodName);
        }

        [Conditional("DEBUG")]
        public static void Field(string fieldName, object value, [CallerMemberName] string memberName = "")
        {
            _inst.Field(fieldName, value, memberName);
        }
    }
}