﻿#region

using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Timers;
using Glog;
using Timer = System.Timers.Timer;

#endregion

namespace GLog
{
    public abstract class BaseLog
    {
        #region Events

        public event EventHandler QueueEmpty;

        #endregion

        #region Private fields

        private ConfigLog _config;

        private DateTime _dateTimeOldDeleteFile;

        private bool _isQueryEmpty = true;

        private Timer _testEmptyTimer;
        private Timer _writeToConsoleTimer;

        #endregion

        #region Construct

        protected BaseLog(ConfigLog configLog)
        {
            Config(configLog);
        }

        #endregion

        #region Private methods

        private void Config(ConfigLog config)
        {
            _dateTimeOldDeleteFile = DateTime.Now;
            _isQueryEmpty = false;
            _config = config;

            if (config.ToFile)
            {
                if (!Directory.Exists(_config.LogPath.Path))
                {
                    Directory.CreateDirectory(_config.LogPath.Path ?? throw new InvalidOperationException());
                }

                if (File.Exists(_config.LogPath.ToString()))
                {
                    File.Delete(_config.LogPath.ToString());
                }
            }

            InitTimers();
        }

        private string GetMessage()
        {
            Messages.TryDequeue(out var msg);
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("");
            sb.AppendLine("{");
            sb.AppendLine("\"idxMsg\" : \"" + _config.GetNextIndex() + "\",");
            sb.AppendLine("\"date\" : \"" + DateTime.Now.ToString(_config.DateTimeFormat) + "\",");
            sb.AppendLine("\"msg\" : \"" + msg + "\"");
            sb.Append("},");
            var res = sb.ToString();
            return res;
        }

        private static readonly object Locker = new object();

        private void DebugWriteLine(string msg)  
        {
            lock (Locker)
            {
                Debug.Listeners.Clear();

                if (_config.ToConsole)
                    Debug.Listeners.Add(new TextWriterTraceListener(Console.Out));

                if (_config.ToDebug)
                    Debug.Listeners.Add(new DefaultTraceListener());

                Debug.AutoFlush = true;

                Debug.WriteLine(msg);
            }
        }

        private void WriteToConsoleTimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            if (_config.LogPath == null && _config.ToFile)
                ThreadPool.QueueUserWorkItem(_ => throw new Exception("Path for log is not set"));

            try
            {
                if (Messages.Count <= 0) return;
                _isQueryEmpty = false;

                if (_config.ToConsole && !_config.ToFile)
                {
                    for (var i = 0; i < _config.WriteLinesPerTik && i < Messages.Count; i++)
                    {
                        DebugWriteLine(GetMessage());
                    }
                }
                else if (!_config.ToConsole && _config.ToFile)
                {
                    if (_config.LogPath == null)
                        throw new NullReferenceException("_config.LogPath == null");

                    var pathToLog = _config.LogPath.ToString();

                    using (var streamwriter = new StreamWriter(pathToLog, true, Encoding.UTF8, 65536))
                    {
                        for (var i = 0; i < _config.WriteLinesPerTik && i < Messages.Count; i++)
                        {
                            streamwriter.WriteLine(GetMessage());
                        }
                    }
                }
                else if (_config.ToConsole && _config.ToFile)
                {
                    if (_config.LogPath == null)
                        throw new NullReferenceException("_config.LogPath == null");

                    var pathToLog = _config.LogPath.ToString();

                    using (var streamwriter = new StreamWriter(pathToLog, true, Encoding.UTF8, 65536))
                    {
                        for (var i = 0; i < _config.WriteLinesPerTik && i < Messages.Count; i++)
                        {
                            var msg = GetMessage();
                            streamwriter.WriteLine(msg);
                            DebugWriteLine(msg);
                        }
                    }
                }

                var totalSec = (DateTime.Now - _dateTimeOldDeleteFile).Milliseconds;

                if (File.Exists(_config.LogPath.ToString()) && Messages.Count > 0 && totalSec > _config.HistoryLenMs)
                {
                    _dateTimeOldDeleteFile = DateTime.Now;

                    var n = _config.LogPath.Num - _config.NumberOfFiles + 1;
                    if (n >= 0)
                    {
                        var fn = new FileName(_config.LogPath) { Num = n };
                        var fullPath = fn.ToString();
                        if (File.Exists(fullPath))
                        {
                            File.Delete(fullPath);
                        }
                    }

                    _config.LogPath.NextFileName();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                _writeToConsoleTimer.Start();
            }
        }

        private void TestEmptyTimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            if (Messages.Count == 0 && !_isQueryEmpty)
            {
                _isQueryEmpty = true;
                OnQueueEmpty();
            }
        }

        private void OnQueueEmpty()
        {
            QueueEmpty?.Invoke(this, EventArgs.Empty);
        }

        private void InitTimers()
        {
            if (_testEmptyTimer != null) return;
            if (_writeToConsoleTimer != null) return;

            _testEmptyTimer = new Timer(_config.TestEmptyInterval) {AutoReset = true};
            _testEmptyTimer.Elapsed += TestEmptyTimerOnElapsed;

            _writeToConsoleTimer = new Timer(_config.WriteInterval) {AutoReset = false};
            _writeToConsoleTimer.Elapsed += WriteToConsoleTimerOnElapsed;

            _testEmptyTimer.Start();
            _writeToConsoleTimer.Start();
        }

        #endregion

        #region Protected Fields

        protected readonly ConcurrentQueue<IMessage> Messages = new ConcurrentQueue<IMessage>();

        #endregion
    }
}