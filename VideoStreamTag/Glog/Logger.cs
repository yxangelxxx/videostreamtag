﻿#region

using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Glog;

#endregion

namespace GLog
{
    public class Logger : BaseLog
    {
        #region Construct

        protected internal Logger(ConfigLog configLog) : base(configLog)
        { }

        #endregion

        #region Public

        public static Logger CreateInstanceBaseConfig()
        {
            return new Logger(new ConfigLog(true, true, "logs\\log.txt"));
        }

        public static Logger CreateInstance(ConfigLog configLog)
        {
            return new Logger(configLog);
        }

        [Conditional("DEBUG")]
        public void Write(string msg)
        {
            Messages.Enqueue(new StrMsg(msg));
        }

        [Conditional("DEBUG")]
        public void Write(Exception ex,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string sourceFilePath = "",
            [CallerLineNumber] int sourceLineNumber = 0)
        {
            Messages.Enqueue(new StrMsg(ex.Message + " [" + sourceLineNumber + "]:" + sourceFilePath + ":" +
                                        memberName));
        }

        [Conditional("TRACE")]
        public void Trace(string str)
        {
            Messages.Enqueue(new StrMsg(str));
        }

        [Conditional("DEBUG")]
        public void Func([CallerMemberName] string methodName = "")
        {
            Messages.Enqueue(new StrMsg("Call: " + methodName));
        }

        [Conditional("DEBUG")]
        public void Field(string fieldName, object value, [CallerMemberName] string memberName = "")
        {
            if (value != null)
            {
                Messages.Enqueue(new StrMsg("Member name: " + memberName + " || " + fieldName + " = " + value));
            }
            else
            {
                Messages.Enqueue(new StrMsg("Member name: " + memberName + " || " + fieldName + " = null"));
            }
        }

        #endregion
    }
}