﻿#region

using System;
using System.IO;
using System.Text.RegularExpressions;

#endregion

namespace Glog
{
    public class ConfigLog
    {
        #region Private fields

        private long _idxMessage;

        #endregion

        #region Construct

        public ConfigLog(bool toConsole, bool toDebug, string logPath)
        {
            _idxMessage = 0;
            _idxFileName = 0;
            DateTimeFormat = "dd | h: mm:ss:ms";
            ToConsole = toConsole;
            ToDebug = toDebug;
            if (!string.IsNullOrEmpty(logPath))
            {
                ToFile = true;
                LogPath = FileName.ParseFileName(logPath);
            }
        }

        public ConfigLog(bool toConsole, bool toDebug, string logPath, string dateTimeFormat) : this(toConsole, toDebug, logPath)
        {
            DateTimeFormat = dateTimeFormat;
        }

        public ConfigLog(bool toConsole, bool toDebug, string logPath, int testEmptyInterval = 1, int writeInterval = 10,
            int historyLenMs = 5 * (1000 * 60), int numberOfFiles = 3, int writeLinesPerTik = 1000, string dateTimeFormat = "dd | h: mm:ss:ms")
            : this(toConsole, toDebug, logPath, dateTimeFormat)
        {
            if (testEmptyInterval <= 0) throw new ArgumentOutOfRangeException(nameof(testEmptyInterval));
            if (writeInterval <= 0) throw new ArgumentOutOfRangeException(nameof(writeInterval));

            TestEmptyInterval = testEmptyInterval;
            WriteInterval = writeInterval;
            WriteLinesPerTik = writeLinesPerTik;
            HistoryLenMs = historyLenMs;
            NumberOfFiles = numberOfFiles;
        }

        #endregion

        #region Public

        public long GetNextIndex()
        {
            return _idxMessage++;
        }

        private int _idxFileName = 0;

        public readonly bool ToConsole;
        public readonly bool ToDebug;
        public readonly bool ToFile;
        public readonly int TestEmptyInterval = 1;

        public readonly int WriteInterval = 10;

        public readonly int HistoryLenMs = 5 * (1000 * 60);
        public readonly int NumberOfFiles = 3;
        public readonly int WriteLinesPerTik = 10000;
        public FileName LogPath;
        public readonly string DateTimeFormat;

        #endregion
    }
}