﻿using System.Collections.Generic;

namespace Glog
{
    public static class AnimWait
    {
        private static readonly List<string> Str = new List<string>{ "\\", "|", "/", "-" };
        private static int _idx;

        public static string GetIco()
        {
            _idx++;
            if (_idx > Str.Count - 1)
                _idx = 0;

            return "|" + Str[_idx] + "|";
        }
    }
}