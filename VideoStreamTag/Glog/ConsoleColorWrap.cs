﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;

namespace GLog
{
	public class ConsoleColorWrap
	{
		[StructLayout(LayoutKind.Sequential)]
		public struct Coord
		{
			public short X;
			public short Y;

			public Coord(short x, short y)
			{
				this.X = x;
				this.Y = y;
			}
		};

		[StructLayout(LayoutKind.Explicit)]
		public struct CharUnion
		{
			[FieldOffset(0)] public char UnicodeChar;
			[FieldOffset(0)] public byte AsciiChar;
		}

		[StructLayout(LayoutKind.Explicit)]
		public struct CharInfo
		{
			[FieldOffset(0)] public CharUnion Char;
			[FieldOffset(2)] public short Attributes;
		}

		[StructLayout(LayoutKind.Sequential)]
		public struct SmallRect
		{
			public short Left;
			public short Top;
			public short Right;
			public short Bottom;
		}

		[DllImport("kernel32.dll")]
		static extern void OutputDebugString(string lpOutputString);

		[DllImport("kernel32.dll", SetLastError = true)]
		static extern bool WriteConsoleOutput(
			SafeFileHandle hConsoleOutput,
			CharInfo[] lpBuffer,
			Coord dwBufferSize,
			Coord dwBufferCoord,
			ref SmallRect lpWriteRegion);

		[DllImport("Kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
		static extern SafeFileHandle CreateFile(
			string fileName,
			[MarshalAs(UnmanagedType.U4)] uint fileAccess,
			[MarshalAs(UnmanagedType.U4)] uint fileShare,
			IntPtr securityAttributes,
			[MarshalAs(UnmanagedType.U4)] FileMode creationDisposition,
			[MarshalAs(UnmanagedType.U4)] int flags,
			IntPtr template);

		private static SafeFileHandle _handle;

		static ConsoleColorWrap()
		{
			_handle = CreateFile("CONOUT$", 0x40000000, 2, IntPtr.Zero, FileMode.Open, 0, IntPtr.Zero);
		}

		public static void WriteConsoleColor(string text, ConsoleColor fg, ConsoleColor bg)
		{



			short width = (short) text.Length;
			short height = (short) 1;

			Coord cur = new Coord((short)Console.CursorLeft, (short)Console.CursorTop);
			SmallRect writeRegion = new SmallRect() { Left = cur.X, Top = cur.Y, Right = (short) (cur.X + width), Bottom = height };
			Coord bufferSize = new Coord(width, height);
			Coord bufferCoord = new Coord(0, 0);
			CharInfo[] buffer = new CharInfo[width * height];

			for (int i = 0; i < text.Length; i++)
			{
				if (text[i] == '\n')
				{
					cur.X = 0;
					cur.Y++;
				}
				else
				{

					buffer[i].Attributes = (short)((int) fg | ((int) bg));
					buffer[i].Char.AsciiChar = (byte)text[i];
					cur.X++;
				}










			}

			WriteConsoleOutput(_handle, buffer, bufferSize, bufferCoord, ref writeRegion);
			Console.SetCursorPosition(cur.X, cur.Y);
		}
	}
}