﻿namespace GLog
{
	public interface IMessage
	{
		string ToString();
	}
}