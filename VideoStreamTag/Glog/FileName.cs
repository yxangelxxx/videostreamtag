﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace Glog
{
    public class FileName
    {
        public string Name { get; set; }
        public string Ext { get; set; }
        public string Path { get; set; }
        public int Num { get; set; }

        public FileName(FileName fileName)
        {
            Path = fileName.Path;
            Name = fileName.Name;
            Ext = fileName.Ext;
            Num = fileName.Num;
        }

        public FileName(string path, string name, string ext, int num)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentException("message", nameof(path));
            }

            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException("message", nameof(name));
            }

            if (string.IsNullOrEmpty(ext))
            {
                throw new ArgumentException("message", nameof(ext));
            }

            Path = path;
            Name = name;
            Ext = ext;
            Num = num;
        }

        public override string ToString()
        {
            return GetFileName();
        }

        public string GetFileName()
        {
            return Path + "\\" + Name + "_" + Num + Ext;
        }

        public FileName(string fullFileName)
        {
            if (string.IsNullOrEmpty(fullFileName))
            {
                throw new ArgumentException("message", nameof(fullFileName));
            }

            if (!string.IsNullOrEmpty(fullFileName))
            {
               Path = System.IO.Path.GetDirectoryName(fullFileName);
               Name = System.IO.Path.GetFileNameWithoutExtension(fullFileName);
               Ext = System.IO.Path.GetExtension(fullFileName);
            }
        }

        public static FileName ParseFileName(string fullFileName)
        {
            if (string.IsNullOrEmpty(fullFileName))
            {
                throw new ArgumentException("message", nameof(fullFileName));
            }

            var cutFileName = System.IO.Path.GetFullPath(fullFileName);
            var path = System.IO.Path.GetDirectoryName(cutFileName);
            var name = System.IO.Path.GetFileNameWithoutExtension(cutFileName);
            var ext = System.IO.Path.GetExtension(cutFileName);

            var reg = new Regex("^(?<name>.+)_(?<num>\\d+)$");
            var match = reg.Match(fullFileName);

            if (!match.Success)
                return new FileName(path, name, ext, 0);

            var nameg = match.Groups["name"].Value;
            var numg = short.Parse(match.Groups["num"].Value);

            return new FileName(path, nameg, ext, numg);
        }

        public string NextFileName()
        {
            Num++;
            return GetFileName();
        }
    }
}