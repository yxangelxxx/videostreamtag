﻿namespace Glog
{
    public static class Helper
    {
        public static int Minute(this int num)
        {
            return num * 1000 * 60;
        }
    }
}