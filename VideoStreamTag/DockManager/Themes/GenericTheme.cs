﻿#region

using System;

#endregion

namespace DockManager.Themes
{
    public class GenericTheme : Theme
    {
        #region Public

        public override Uri GetResourceUri()
        {
            return new Uri(
                "/Themes/generic.xaml",
                UriKind.Relative);
        }

        #endregion
    }
}