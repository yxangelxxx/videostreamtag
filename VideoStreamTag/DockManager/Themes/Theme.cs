﻿#region

using System;
using System.Windows;

#endregion

namespace DockManager.Themes
{
    public abstract class Theme : DependencyObject
    {
        #region Construct

        public Theme()
        { }

        #endregion

        #region Public

        public abstract Uri GetResourceUri();

        #endregion
    }
}