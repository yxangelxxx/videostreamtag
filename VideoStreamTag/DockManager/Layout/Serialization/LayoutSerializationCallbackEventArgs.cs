﻿#region

using System.ComponentModel;

#endregion

namespace DockManager.Layout.Serialization
{
    public class LayoutSerializationCallbackEventArgs : CancelEventArgs
    {
        #region Construct

        public LayoutSerializationCallbackEventArgs(LayoutContent model, object previousContent)
        {
            Cancel = false;
            Model = model;
            Content = previousContent;
        }

        #endregion

        #region Public

        public LayoutContent Model { get; private set; }

        public object Content { get; set; }

        #endregion
    }
}