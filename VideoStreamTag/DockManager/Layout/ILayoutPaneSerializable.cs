﻿namespace DockManager.Layout
{
    internal interface ILayoutPaneSerializable
    {
        #region Public

        string Id { get; set; }

        #endregion
    }
}