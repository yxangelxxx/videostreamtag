﻿namespace DockManager.Layout
{
    public interface ILayoutDocumentPane : ILayoutPanelElement, ILayoutPane
    { }
}