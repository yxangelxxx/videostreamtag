﻿namespace DockManager.Layout
{
    public interface ILayoutControl
    {
        #region Public

        ILayoutElement Model { get; }

        #endregion
    }
}