﻿#region

using System.Windows.Controls;

#endregion

namespace DockManager.Layout
{
    public interface ILayoutOrientableGroup : ILayoutGroup
    {
        #region Public

        Orientation Orientation { get; set; }

        #endregion
    }
}