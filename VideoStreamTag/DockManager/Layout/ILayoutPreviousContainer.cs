﻿namespace DockManager.Layout
{
    internal interface ILayoutPreviousContainer
    {
        #region Public

        ILayoutContainer PreviousContainer { get; set; }

        string PreviousContainerId { get; set; }

        #endregion
    }
}