﻿#region

using System;

#endregion

namespace DockManager.Layout
{
    public enum ChildrenTreeChange
    {
        DirectChildrenChanged,
        TreeChanged
    }

    public class ChildrenTreeChangedEventArgs : EventArgs
    {
        #region Construct

        public ChildrenTreeChangedEventArgs(ChildrenTreeChange change)
        {
            Change = change;
        }

        #endregion

        #region Public

        public ChildrenTreeChange Change { get; private set; }

        #endregion
    }
}