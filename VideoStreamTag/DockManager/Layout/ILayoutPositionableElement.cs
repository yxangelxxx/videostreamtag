﻿#region

using System.Windows;

#endregion

namespace DockManager.Layout
{
    internal interface ILayoutPositionableElement : ILayoutElement, ILayoutElementForFloatingWindow
    {
        #region Public

        GridLength DockWidth { get; set; }

        GridLength DockHeight { get; set; }

        double DockMinWidth { get; set; }
        double DockMinHeight { get; set; }


        bool IsVisible { get; }

        #endregion
    }


    internal interface ILayoutPositionableElementWithActualSize
    {
        #region Public

        double ActualWidth { get; set; }
        double ActualHeight { get; set; }

        #endregion
    }

    internal interface ILayoutElementForFloatingWindow
    {
        #region Public

        double FloatingWidth { get; set; }
        double FloatingHeight { get; set; }
        double FloatingLeft { get; set; }
        double FloatingTop { get; set; }
        bool IsMaximized { get; set; }

        #endregion
    }
}