﻿#region

using System;

#endregion

namespace DockManager.Layout
{
    public class LayoutElementEventArgs : EventArgs
    {
        #region Construct

        public LayoutElementEventArgs(LayoutElement element)
        {
            Element = element;
        }

        #endregion

        #region Public

        public LayoutElement Element { get; private set; }

        #endregion
    }
}