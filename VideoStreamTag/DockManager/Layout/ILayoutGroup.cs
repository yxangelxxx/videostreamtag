﻿#region

using System;

#endregion

namespace DockManager.Layout
{
    public interface ILayoutGroup : ILayoutContainer
    {
        #region Events

        event EventHandler ChildrenCollectionChanged;

        #endregion

        #region Public

        int IndexOfChild(ILayoutElement element);
        void InsertChildAt(int index, ILayoutElement element);
        void RemoveChildAt(int index);
        void ReplaceChildAt(int index, ILayoutElement element);

        #endregion
    }
}