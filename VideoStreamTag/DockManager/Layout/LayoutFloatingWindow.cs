﻿#region

using System;
using System.Collections.Generic;
using System.Xml.Serialization;

#endregion

namespace DockManager.Layout
{
    [Serializable]
    [XmlInclude(typeof(LayoutAnchorableFloatingWindow))]
    [XmlInclude(typeof(LayoutDocumentFloatingWindow))]
    public abstract class LayoutFloatingWindow : LayoutElement, ILayoutContainer
    {
        #region Construct

        public LayoutFloatingWindow()
        { }

        #endregion

        #region Public

        public abstract void RemoveChild(ILayoutElement element);

        public abstract void ReplaceChild(ILayoutElement oldElement, ILayoutElement newElement);


        public abstract IEnumerable<ILayoutElement> Children { get; }

        public abstract int ChildrenCount { get; }

        public abstract bool IsValid { get; }

        #endregion
    }
}