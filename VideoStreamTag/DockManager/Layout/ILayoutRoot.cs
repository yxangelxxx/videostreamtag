﻿#region

using System.Collections.ObjectModel;

#endregion

namespace DockManager.Layout
{
    public interface ILayoutRoot
    {
        #region Public

        void CollectGarbage();
        DockingManager Manager { get; }

        LayoutPanel RootPanel { get; }

        LayoutAnchorSide TopSide { get; }
        LayoutAnchorSide LeftSide { get; }
        LayoutAnchorSide RightSide { get; }
        LayoutAnchorSide BottomSide { get; }

        LayoutContent ActiveContent { get; set; }

        ObservableCollection<LayoutFloatingWindow> FloatingWindows { get; }
        ObservableCollection<LayoutAnchorable> Hidden { get; }

        #endregion
    }
}