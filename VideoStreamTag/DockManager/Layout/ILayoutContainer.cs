﻿#region

using System.Collections.Generic;

#endregion

namespace DockManager.Layout
{
    public interface ILayoutContainer : ILayoutElement
    {
        #region Public

        void RemoveChild(ILayoutElement element);
        void ReplaceChild(ILayoutElement oldElement, ILayoutElement newElement);
        IEnumerable<ILayoutElement> Children { get; }
        int ChildrenCount { get; }

        #endregion
    }
}