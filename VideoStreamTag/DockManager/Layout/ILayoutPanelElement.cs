﻿namespace DockManager.Layout
{
    public interface ILayoutPanelElement : ILayoutElement
    {
        #region Public

        bool IsVisible { get; }

        #endregion
    }
}