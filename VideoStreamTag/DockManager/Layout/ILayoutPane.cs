﻿namespace DockManager.Layout
{
    public interface ILayoutPane : ILayoutContainer, ILayoutElementWithVisibility
    {
        #region Public

        void MoveChild(int oldIndex, int newIndex);

        void RemoveChildAt(int childIndex);

        #endregion
    }
}