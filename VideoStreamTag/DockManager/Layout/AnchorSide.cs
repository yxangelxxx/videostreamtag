﻿namespace DockManager.Layout
{
    public enum AnchorSide
    {
        Left,

        Top,

        Right,

        Bottom
    }
}