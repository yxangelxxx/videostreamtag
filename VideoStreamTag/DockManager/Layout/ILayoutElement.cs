﻿#region

using System.ComponentModel;

#endregion

namespace DockManager.Layout
{
    public interface ILayoutElement : INotifyPropertyChanged, INotifyPropertyChanging
    {
        #region Public

        ILayoutContainer Parent { get; }
        ILayoutRoot Root { get; }

        #endregion
    }
}