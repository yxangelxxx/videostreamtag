﻿namespace DockManager.Layout
{
    public interface ILayoutContentSelector
    {
        #region Public

        int IndexOf(LayoutContent content);
        int SelectedContentIndex { get; set; }

        LayoutContent SelectedContent { get; }

        #endregion
    }
}