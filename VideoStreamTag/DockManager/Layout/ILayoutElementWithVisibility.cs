﻿namespace DockManager.Layout
{
    public interface ILayoutElementWithVisibility
    {
        #region Public

        void ComputeVisibility();

        #endregion
    }
}