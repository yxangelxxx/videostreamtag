﻿namespace DockManager.Layout
{
    public interface ILayoutAnchorablePane : ILayoutPanelElement, ILayoutPane
    { }
}