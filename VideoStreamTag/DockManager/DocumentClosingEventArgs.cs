﻿#region

using System.ComponentModel;
using DockManager.Layout;

#endregion

namespace DockManager
{
    public class DocumentClosingEventArgs : CancelEventArgs
    {
        #region Construct

        public DocumentClosingEventArgs(LayoutDocument document)
        {
            Document = document;
        }

        #endregion

        #region Public

        public LayoutDocument Document { get; private set; }

        #endregion
    }
}