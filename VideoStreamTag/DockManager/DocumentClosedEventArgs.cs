﻿#region

using System;
using DockManager.Layout;

#endregion

namespace DockManager
{
    public class DocumentClosedEventArgs : EventArgs
    {
        #region Construct

        public DocumentClosedEventArgs(LayoutDocument document)
        {
            Document = document;
        }

        #endregion

        #region Public

        public LayoutDocument Document { get; private set; }

        #endregion
    }
}