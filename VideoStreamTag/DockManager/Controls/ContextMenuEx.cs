﻿#region

using System.Windows.Controls;
using System.Windows.Data;

#endregion

namespace DockManager.Controls
{
    public class ContextMenuEx : ContextMenu
    {
        #region Construct

        static ContextMenuEx()
        { }

        public ContextMenuEx()
        { }

        #endregion

        #region Protected Методы

        protected override System.Windows.DependencyObject GetContainerForItemOverride()
        {
            return new MenuItemEx();
        }

        protected override void OnOpened(System.Windows.RoutedEventArgs e)
        {
            BindingOperations.GetBindingExpression(this, ItemsSourceProperty).UpdateTarget();

            base.OnOpened(e);
        }

        #endregion
    }
}