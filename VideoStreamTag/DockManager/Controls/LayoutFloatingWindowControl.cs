﻿#region

using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using DockManager.Controls.Shell;
using DockManager.Layout;
using DockManager.Themes;

#endregion

namespace DockManager.Controls
{
    public abstract class LayoutFloatingWindowControl : Window, ILayoutControl
    {
        #region Private fields

        private bool _attachDrag = false;


        private DragService _dragService = null;

        private HwndSource _hwndSrc;
        private HwndSourceHook _hwndSrcHook;

        private bool _internalCloseFlag = false;

        private ILayoutElement _model;

        #endregion

        #region Protected fields

        protected bool CloseInitiatedByUser
        {
            get { return !_internalCloseFlag; }
        }

        #endregion

        #region Internals

        internal virtual void UpdateThemeResources(Theme oldTheme = null)
        {
            if (oldTheme != null)
            {
                var resourceDictionaryToRemove =
                    Resources.MergedDictionaries.FirstOrDefault(r => r.Source == oldTheme.GetResourceUri());
                if (resourceDictionaryToRemove != null)
                    Resources.MergedDictionaries.Remove(
                        resourceDictionaryToRemove);
            }

            var manager = _model.Root.Manager;
            if (manager.Theme != null)
            {
                Resources.MergedDictionaries.Add(new ResourceDictionary() {Source = manager.Theme.GetResourceUri()});
            }
        }

        internal void AttachDrag(bool onActivated = true)
        {
            if (onActivated)
            {
                _attachDrag = true;
                this.Activated += OnActivated;
            }
            else
            {
                IntPtr windowHandle = new WindowInteropHelper(this).Handle;
                IntPtr lParam = new IntPtr(((int) Left & (int) 0xFFFF) | ((int) Top << 16));
                Win32Helper.SendMessage(windowHandle, Win32Helper.WM_NCLBUTTONDOWN, new IntPtr(Win32Helper.HT_CAPTION), lParam);
            }
        }

        internal void InternalClose()
        {
            _internalCloseFlag = true;
            Close();
        }

        internal bool KeepContentVisibleOnClose { get; set; }

        #endregion

        #region Construct

        static LayoutFloatingWindowControl()
        {
            LayoutFloatingWindowControl.ContentProperty.OverrideMetadata(typeof(LayoutFloatingWindowControl),
                new FrameworkPropertyMetadata(null, null, CoerceContentValue));
            AllowsTransparencyProperty.OverrideMetadata(typeof(LayoutFloatingWindowControl), new FrameworkPropertyMetadata(false));
            ShowInTaskbarProperty.OverrideMetadata(typeof(LayoutFloatingWindowControl), new FrameworkPropertyMetadata(false));
        }

        protected LayoutFloatingWindowControl(ILayoutElement model)
        {
            this.Loaded += OnLoaded;
            this.Unloaded += OnUnloaded;
            _model = model;
            UpdateThemeResources();
        }

        #endregion

        #region Public

        public abstract ILayoutElement Model { get; }

        #endregion

        #region Private methods

        private static object CoerceContentValue(DependencyObject sender, object content)
        {
            return new FloatingWindowContentHost(sender as LayoutFloatingWindowControl) {Content = content as UIElement};
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= OnLoaded;

            this.SetParentToMainWindowOf(Model.Root.Manager);


            _hwndSrc = HwndSource.FromDependencyObject(this) as HwndSource;
            _hwndSrcHook = FilterMessage;
            _hwndSrc.AddHook(_hwndSrcHook);
        }

        private void OnUnloaded(object sender, RoutedEventArgs e)
        {
            this.Unloaded -= OnUnloaded;

            if (_hwndSrc != null)
            {
                _hwndSrc.RemoveHook(_hwndSrcHook);
                _hwndSrc.Dispose();
                _hwndSrc = null;
            }
        }

        private void OnActivated(object sender, EventArgs e)
        {
            this.Activated -= OnActivated;

            if (_attachDrag && Mouse.LeftButton == MouseButtonState.Pressed)
            {
                IntPtr windowHandle = new WindowInteropHelper(this).Handle;
                var mousePosition = this.PointToScreenDpi(Mouse.GetPosition(this));
                var clientArea = Win32Helper.GetClientRect(windowHandle);
                var windowArea = Win32Helper.GetWindowRect(windowHandle);

                Left = mousePosition.X - windowArea.Width / 2.0;
                Top = mousePosition.Y - (windowArea.Height - clientArea.Height) / 2.0;
                _attachDrag = false;

                IntPtr lParam = new IntPtr(((int) mousePosition.X & (int) 0xFFFF) | ((int) mousePosition.Y << 16));
                Win32Helper.SendMessage(windowHandle, Win32Helper.WM_NCLBUTTONDOWN, new IntPtr(Win32Helper.HT_CAPTION), lParam);
            }
        }

        private void UpdatePositionAndSizeOfPanes()
        {
            foreach (var posElement in Model.Descendents().OfType<ILayoutElementForFloatingWindow>())
            {
                posElement.FloatingLeft = Left;
                posElement.FloatingTop = Top;
                posElement.FloatingWidth = Width;
                posElement.FloatingHeight = Height;
            }
        }

        private void UpdateMaximizedState(bool isMaximized)
        {
            foreach (var posElement in Model.Descendents().OfType<ILayoutElementForFloatingWindow>())
            {
                posElement.IsMaximized = isMaximized;
            }
        }

        private void UpdateDragPosition()
        {
            if (_dragService == null)
            {
                _dragService = new DragService(this);
                SetIsDragging(true);
            }

            var mousePosition = this.TransformToDeviceDpi(Win32Helper.GetMousePosition());
            _dragService.UpdateMouseLocation(mousePosition);
        }

        #endregion

        #region Protected Методы

        protected override void OnClosed(EventArgs e)
        {
            if (Content != null)
            {
                var host = Content as FloatingWindowContentHost;
                host.Dispose();

                if (_hwndSrc != null)
                {
                    _hwndSrc.RemoveHook(_hwndSrcHook);
                    _hwndSrc.Dispose();
                    _hwndSrc = null;
                }
            }

            base.OnClosed(e);
        }


        protected override void OnInitialized(EventArgs e)
        {
            CommandBindings.Add(new CommandBinding(SystemCommands.CloseWindowCommand,
                (s, args) => SystemCommands.CloseWindow((Window) args.Parameter)));
            CommandBindings.Add(new CommandBinding(SystemCommands.MaximizeWindowCommand,
                (s, args) => SystemCommands.MaximizeWindow((Window) args.Parameter)));
            CommandBindings.Add(new CommandBinding(SystemCommands.MinimizeWindowCommand,
                (s, args) => SystemCommands.MinimizeWindow((Window) args.Parameter)));
            CommandBindings.Add(new CommandBinding(SystemCommands.RestoreWindowCommand,
                (s, args) => SystemCommands.RestoreWindow((Window) args.Parameter)));
            //Debug.Assert(this.Owner != null);
            base.OnInitialized(e);
        }


        protected virtual IntPtr FilterMessage(
            IntPtr hwnd,
            int msg,
            IntPtr wParam,
            IntPtr lParam,
            ref bool handled
        )
        {
            handled = false;

            switch (msg)
            {
                case Win32Helper.WM_ACTIVATE:
                    if (((int) wParam & 0xFFFF) == Win32Helper.WA_INACTIVE)
                    {
                        if (lParam == this.GetParentWindowHandle())
                        {
                            Win32Helper.SetActiveWindow(_hwndSrc.Handle);
                            handled = true;
                        }
                    }

                    break;
                case Win32Helper.WM_EXITSIZEMOVE:
                    UpdatePositionAndSizeOfPanes();

                    if (_dragService != null)
                    {
                        bool dropFlag;
                        var mousePosition = this.TransformToDeviceDpi(Win32Helper.GetMousePosition());
                        _dragService.Drop(mousePosition, out dropFlag);
                        _dragService = null;
                        SetIsDragging(false);

                        if (dropFlag)
                            InternalClose();
                    }

                    break;
                case Win32Helper.WM_MOVING:
                {
                    UpdateDragPosition();
                }
                    break;
                case Win32Helper.WM_LBUTTONUP: //set as handled right button click on title area (after showing context menu)
                    if (_dragService != null && Mouse.LeftButton == MouseButtonState.Released)
                    {
                        _dragService.Abort();
                        _dragService = null;
                        SetIsDragging(false);
                    }

                    break;
                case Win32Helper.WM_SYSCOMMAND:
                    IntPtr wMaximize = new IntPtr(Win32Helper.SC_MAXIMIZE);
                    IntPtr wRestore = new IntPtr(Win32Helper.SC_RESTORE);
                    if (wParam == wMaximize || wParam == wRestore)
                    {
                        UpdateMaximizedState(wParam == wMaximize);
                    }

                    break;
            }


            return IntPtr.Zero;
        }

        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonUp(e);
        }

        #endregion

        #region Other members

        protected class FloatingWindowContentHost : HwndHost
        {
            #region Private fields

            private DockingManager _manager = null;
            private LayoutFloatingWindowControl _owner;
            private Border _rootPresenter = null;


            private HwndSource _wpfContentHost = null;

            #endregion

            #region Construct

            public FloatingWindowContentHost(LayoutFloatingWindowControl owner)
            {
                _owner = owner;
                var manager = _owner.Model.Root.Manager;
            }

            #endregion

            #region Public

            public Visual RootVisual
            {
                get { return _rootPresenter; }
            }

            #endregion

            #region Protected Методы

            protected override System.Runtime.InteropServices.HandleRef BuildWindowCore(System.Runtime.InteropServices.HandleRef hwndParent)
            {
                _wpfContentHost = new HwndSource(new HwndSourceParameters()
                {
                    ParentWindow = hwndParent.Handle,
                    WindowStyle = Win32Helper.WS_CHILD | Win32Helper.WS_VISIBLE | Win32Helper.WS_CLIPSIBLINGS | Win32Helper.WS_CLIPCHILDREN,
                    Width = 1,
                    Height = 1
                });

                _rootPresenter = new Border() {Child = new AdornerDecorator() {Child = Content}, Focusable = true};
                _rootPresenter.SetBinding(Border.BackgroundProperty, new Binding("Background") {Source = _owner});
                _wpfContentHost.RootVisual = _rootPresenter;
                _wpfContentHost.SizeToContent = SizeToContent.Manual;
                _manager = _owner.Model.Root.Manager;
                _manager.InternalAddLogicalChild(_rootPresenter);

                return new HandleRef(this, _wpfContentHost.Handle);
            }


            protected override void OnGotKeyboardFocus(KeyboardFocusChangedEventArgs e)
            {
                Trace.WriteLine("FloatingWindowContentHost.GotKeyboardFocus");
                base.OnGotKeyboardFocus(e);
            }

            protected override void DestroyWindowCore(HandleRef hwnd)
            {
                _manager.InternalRemoveLogicalChild(_rootPresenter);
                if (_wpfContentHost != null)
                {
                    _wpfContentHost.Dispose();
                    _wpfContentHost = null;
                }
            }

            protected override IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
            {
                switch (msg)
                {
                    case Win32Helper.WM_SETFOCUS:
                        Trace.WriteLine("FloatingWindowContentHost.WM_SETFOCUS");
                        break;
                    case Win32Helper.WM_KILLFOCUS:
                        Trace.WriteLine("FloatingWindowContentHost.WM_KILLFOCUS");
                        break;
                }

                return base.WndProc(hwnd, msg, wParam, lParam, ref handled);
            }

            protected override Size MeasureOverride(Size constraint)
            {
                if (Content == null)
                    return base.MeasureOverride(constraint);

                Content.Measure(constraint);
                return Content.DesiredSize;
            }

            #endregion

            #region Content

            public static readonly DependencyProperty ContentProperty =
                DependencyProperty.Register("Content", typeof(UIElement), typeof(FloatingWindowContentHost),
                    new FrameworkPropertyMetadata((UIElement) null,
                        OnContentChanged));

            public UIElement Content
            {
                get { return (UIElement) GetValue(ContentProperty); }
                set { SetValue(ContentProperty, value); }
            }

            private static void OnContentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
            {
                ((FloatingWindowContentHost) d).OnContentChanged(e);
            }

            protected virtual void OnContentChanged(DependencyPropertyChangedEventArgs e)
            {
                if (_rootPresenter != null)
                    _rootPresenter.Child = Content;
            }

            #endregion
        }

        #endregion


        #region IsDragging

        private static readonly DependencyPropertyKey IsDraggingPropertyKey
            = DependencyProperty.RegisterReadOnly("IsDragging", typeof(bool), typeof(LayoutFloatingWindowControl),
                new FrameworkPropertyMetadata((bool) false,
                    OnIsDraggingChanged));

        public static readonly DependencyProperty IsDraggingProperty
            = IsDraggingPropertyKey.DependencyProperty;

        public bool IsDragging
        {
            get { return (bool) GetValue(IsDraggingProperty); }
        }

        protected void SetIsDragging(bool value)
        {
            SetValue(IsDraggingPropertyKey, value);
        }

        private static void OnIsDraggingChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((LayoutFloatingWindowControl) d).OnIsDraggingChanged(e);
        }

        protected virtual void OnIsDraggingChanged(DependencyPropertyChangedEventArgs e)
        {
            //Trace.WriteLine("IsDragging={0}", e.NewValue);
        }

        #endregion

        #region IsMaximized

        private static readonly DependencyPropertyKey IsMaximizedPropertyKey
            = DependencyProperty.RegisterReadOnly("IsMaximized", typeof(bool), typeof(LayoutFloatingWindowControl),
                new FrameworkPropertyMetadata((bool) false));

        public static readonly DependencyProperty IsMaximizedProperty
            = IsMaximizedPropertyKey.DependencyProperty;

        public bool IsMaximized
        {
            get { return (bool) GetValue(IsMaximizedProperty); }
        }

        protected void SetIsMaximized(bool value)
        {
            SetValue(IsMaximizedPropertyKey, value);
        }

        protected override void OnStateChanged(EventArgs e)
        {
            SetIsMaximized(WindowState == System.Windows.WindowState.Maximized);
            base.OnStateChanged(e);
        }

        #endregion
    }
}