﻿#region

using System.Windows;

#endregion

namespace DockManager.Controls
{
    public class DocumentPaneControlOverlayArea : OverlayArea
    {
        #region Private fields

        private LayoutDocumentPaneControl _documentPaneControl;

        #endregion

        #region Internals

        internal DocumentPaneControlOverlayArea(
            IOverlayWindow overlayWindow,
            LayoutDocumentPaneControl documentPaneControl)
            : base(overlayWindow)
        {
            _documentPaneControl = documentPaneControl;
            base.SetScreenDetectionArea(new Rect(
                _documentPaneControl.PointToScreenDpi(new Point()),
                _documentPaneControl.TransformActualSizeToAncestor()));
        }

        #endregion
    }
}