﻿#region

using System.Windows;
using System.Windows.Controls;
using DockManager.Layout;

#endregion

namespace DockManager.Controls
{
    public class LayoutAnchorablePaneGroupControl : LayoutGridControl<ILayoutAnchorablePane>, ILayoutControl
    {
        #region Private fields

        private LayoutAnchorablePaneGroup _model;

        #endregion

        #region Internals

        internal LayoutAnchorablePaneGroupControl(LayoutAnchorablePaneGroup model)
            : base(model, model.Orientation)
        {
            _model = model;
        }

        #endregion

        #region Protected Методы

        protected override void OnFixChildrenDockLengths()
        {
            #region Setup DockWidth/Height for children

            if (_model.Orientation == Orientation.Horizontal)
            {
                for (int i = 0; i < _model.Children.Count; i++)
                {
                    var childModel = _model.Children[i] as ILayoutPositionableElement;
                    if (!childModel.DockWidth.IsStar)
                    {
                        childModel.DockWidth = new GridLength(1.0, GridUnitType.Star);
                    }
                }
            }
            else
            {
                for (int i = 0; i < _model.Children.Count; i++)
                {
                    var childModel = _model.Children[i] as ILayoutPositionableElement;
                    if (!childModel.DockHeight.IsStar)
                    {
                        childModel.DockHeight = new GridLength(1.0, GridUnitType.Star);
                    }
                }
            }

            #endregion
        }

        #endregion
    }
}