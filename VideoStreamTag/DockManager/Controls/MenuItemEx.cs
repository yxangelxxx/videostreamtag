﻿#region

using System.Windows;
using System.Windows.Controls;

#endregion

namespace DockManager.Controls
{
    public class MenuItemEx : MenuItem
    {
        #region Private fields

        private bool _reentrantFlag = false;

        #endregion

        #region Construct

        static MenuItemEx()
        {
            IconProperty.OverrideMetadata(typeof(MenuItemEx), new FrameworkPropertyMetadata(OnIconPropertyChanged));
        }


        public MenuItemEx()
        { }

        #endregion

        #region Private methods

        private static void OnIconPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null)
            {
                ((MenuItemEx) sender).UpdateIcon();
            }
        }

        private void UpdateIcon()
        {
            if (_reentrantFlag)
                return;
            _reentrantFlag = true;
            if (IconTemplateSelector != null)
            {
                var dataTemplateToUse = IconTemplateSelector.SelectTemplate(Icon, this);
                if (dataTemplateToUse != null)
                    Icon = dataTemplateToUse.LoadContent();
            }
            else if (IconTemplate != null)
                Icon = IconTemplate.LoadContent();

            _reentrantFlag = false;
        }

        #endregion

        #region IconTemplate

        public static readonly DependencyProperty IconTemplateProperty =
            DependencyProperty.Register("IconTemplate", typeof(DataTemplate), typeof(MenuItemEx),
                new FrameworkPropertyMetadata((DataTemplate) null,
                    OnIconTemplateChanged));

        public DataTemplate IconTemplate
        {
            get { return (DataTemplate) GetValue(IconTemplateProperty); }
            set { SetValue(IconTemplateProperty, value); }
        }

        private static void OnIconTemplateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((MenuItemEx) d).OnIconTemplateChanged(e);
        }

        protected virtual void OnIconTemplateChanged(DependencyPropertyChangedEventArgs e)
        {
            UpdateIcon();
        }

        #endregion

        #region IconTemplateSelector

        public static readonly DependencyProperty IconTemplateSelectorProperty =
            DependencyProperty.Register("IconTemplateSelector", typeof(DataTemplateSelector), typeof(MenuItemEx),
                new FrameworkPropertyMetadata((DataTemplateSelector) null,
                    OnIconTemplateSelectorChanged));

        public DataTemplateSelector IconTemplateSelector
        {
            get { return (DataTemplateSelector) GetValue(IconTemplateSelectorProperty); }
            set { SetValue(IconTemplateSelectorProperty, value); }
        }

        private static void OnIconTemplateSelectorChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((MenuItemEx) d).OnIconTemplateSelectorChanged(e);
        }

        protected virtual void OnIconTemplateSelectorChanged(DependencyPropertyChangedEventArgs e)
        {
            UpdateIcon();
        }

        #endregion
    }
}