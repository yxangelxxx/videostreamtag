﻿#region

using System.Collections.Generic;
using System.Windows;

#endregion

namespace DockManager.Controls
{
    internal interface IOverlayWindowHost
    {
        #region Public

        bool HitTest(Point dragPoint);

        IOverlayWindow ShowOverlayWindow(LayoutFloatingWindowControl draggingWindow);

        void HideOverlayWindow();

        IEnumerable<IDropArea> GetDropAreas(LayoutFloatingWindowControl draggingWindow);

        DockingManager Manager { get; }

        #endregion
    }
}