﻿#region

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Threading;
using DockManager.Controls.Shell.Standard;
using Point = System.Windows.Point;
using Rect = DockManager.Controls.Shell.Standard.Rect;
using Size = System.Windows.Size;

#endregion

namespace DockManager.Controls.Shell
{
    #region

    using HANDLE_MESSAGE = System.Collections.Generic.KeyValuePair<Standard.Wm, Standard.MessageHandler>;

    #endregion

    internal class WindowChromeWorker : DependencyObject
    {
        #region Static

        public static readonly DependencyProperty WindowChromeWorkerProperty = DependencyProperty.RegisterAttached(
            "WindowChromeWorker",
            typeof(WindowChromeWorker),
            typeof(WindowChromeWorker),
            new PropertyMetadata(null, _OnChromeWorkerChanged));

        [SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Member")]
        private static readonly Ht[,] HitTestBorders = new[,]
        {
            {Ht.Topleft, Ht.Top, Ht.Topright},
            {Ht.Left, Ht.Client, Ht.Right},
            {Ht.Bottomleft, Ht.Bottom, Ht.Bottomright},
        };

        #endregion

        #region Private fields

        // Windows tries hard to hide this state from applications.
        // Generally you can tell that the window is in a docked position because the restore bounds from GetWindowPlacement
        // don't match the current window location and it's not in a maximized or minimized state.
        // Because this isn't doced or supported, it's also not incredibly consistent.  Sometimes some things get updated in
        // different orders, so this isn't absolutely reliable.
        private bool IsWindowDocked
        {
            get
            {
                // We're only detecting this state to work around .Net 3.5 issues.
                // This logic won't work correctly when those issues are fixed.
                Assert.IsTrue(Utility.IsPresentationFrameworkVersionLessThan4);

                if (_window.WindowState != WindowState.Normal)
                {
                    return false;
                }

                Rect adjustedOffset = _GetAdjustedWindowRect(new Rect {Bottom = 100, Right = 100});
                Point windowTopLeft = new Point(_window.Left, _window.Top);
                windowTopLeft -= (Vector) DpiHelper.DevicePixelsToLogical(new Point(adjustedOffset.Left, adjustedOffset.Top));

                return _window.RestoreBounds.Location != windowTopLeft;
            }
        }

        #endregion

        #region Construct

        public WindowChromeWorker()
        {
            _messageTable = new List<HANDLE_MESSAGE>
            {
                new HANDLE_MESSAGE(Wm.Settext, _HandleSetTextOrIcon),
                new HANDLE_MESSAGE(Wm.Seticon, _HandleSetTextOrIcon),
                new HANDLE_MESSAGE(Wm.Ncactivate, _HandleNCActivate),
                new HANDLE_MESSAGE(Wm.Nccalcsize, _HandleNCCalcSize),
                new HANDLE_MESSAGE(Wm.Nchittest, _HandleNCHitTest),
                new HANDLE_MESSAGE(Wm.Ncrbuttonup, _HandleNCRButtonUp),
                new HANDLE_MESSAGE(Wm.Size, _HandleSize),
                new HANDLE_MESSAGE(Wm.Windowposchanged, _HandleWindowPosChanged),
                new HANDLE_MESSAGE(Wm.Dwmcompositionchanged, _HandleDwmCompositionChanged),
            };

            if (Utility.IsPresentationFrameworkVersionLessThan4)
            {
                _messageTable.AddRange(new[]
                {
                    new HANDLE_MESSAGE(Wm.Settingchange, _HandleSettingChange),
                    new HANDLE_MESSAGE(Wm.Entersizemove, _HandleEnterSizeMove),
                    new HANDLE_MESSAGE(Wm.Exitsizemove, _HandleExitSizeMove),
                    new HANDLE_MESSAGE(Wm.Move, _HandleMove),
                });
            }
        }

        #endregion

        #region Public

        public void SetWindowChrome(WindowChrome newChrome)
        {
            VerifyAccess();
            Assert.IsNotNull(_window);

            if (newChrome == _chromeInfo)
            {
                // Nothing's changed.
                return;
            }

            if (_chromeInfo != null)
            {
                _chromeInfo.PropertyChangedThatRequiresRepaint -= _OnChromePropertyChangedThatRequiresRepaint;
            }

            _chromeInfo = newChrome;
            if (_chromeInfo != null)
            {
                _chromeInfo.PropertyChangedThatRequiresRepaint += _OnChromePropertyChangedThatRequiresRepaint;
            }

            _ApplyNewCustomChrome();
        }

        [SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters")]
        public static WindowChromeWorker GetWindowChromeWorker(Window window)
        {
            Verify.IsNotNull(window, "window");
            return (WindowChromeWorker) window.GetValue(WindowChromeWorkerProperty);
        }

        [SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters")]
        public static void SetWindowChromeWorker(Window window, WindowChromeWorker chrome)
        {
            Verify.IsNotNull(window, "window");
            window.SetValue(WindowChromeWorkerProperty, chrome);
        }

        #endregion

        #region Private methods

        private static IntPtr _CreateRoundRectRgn(System.Windows.Rect region, double radius)
        {
            if (DoubleUtilities.AreClose(0, radius))
            {
                return NativeMethods.CreateRectRgn(
                    (int) Math.Floor(region.Left),
                    (int) Math.Floor(region.Top),
                    (int) Math.Ceiling(region.Right),
                    (int) Math.Ceiling(region.Bottom));
            }

            return NativeMethods.CreateRoundRectRgn(
                (int) Math.Floor(region.Left),
                (int) Math.Floor(region.Top),
                (int) Math.Ceiling(region.Right) + 1,
                (int) Math.Ceiling(region.Bottom) + 1,
                (int) Math.Ceiling(radius),
                (int) Math.Ceiling(radius));
        }

        [SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "HRGNs")]
        private static void _CreateAndCombineRoundRectRgn(IntPtr hrgnSource, System.Windows.Rect region, double radius)
        {
            IntPtr hrgn = IntPtr.Zero;
            try
            {
                hrgn = _CreateRoundRectRgn(region, radius);
                CombineRgnResult result = NativeMethods.CombineRgn(hrgnSource, hrgnSource, hrgn, Rgn.Or);
                if (result == CombineRgnResult.Error)
                {
                    throw new InvalidOperationException("Unable to combine two HRGNs.");
                }
            }
            catch
            {
                Utility.SafeDeleteObject(ref hrgn);
                throw;
            }
        }

        private static bool _IsUniform(CornerRadius cornerRadius)
        {
            if (!DoubleUtilities.AreClose(cornerRadius.BottomLeft, cornerRadius.BottomRight))
            {
                return false;
            }

            if (!DoubleUtilities.AreClose(cornerRadius.TopLeft, cornerRadius.TopRight))
            {
                return false;
            }

            if (!DoubleUtilities.AreClose(cornerRadius.BottomLeft, cornerRadius.TopRight))
            {
                return false;
            }

            return true;
        }

        private void _SetWindow(Window window)
        {
            Assert.IsNull(_window);
            Assert.IsNotNull(window);

            _window = window;

            _hwnd = new WindowInteropHelper(_window).Handle;

            if (Utility.IsPresentationFrameworkVersionLessThan4)
            {
                Utility.AddDependencyPropertyChangeListener(_window, Window.TemplateProperty,
                    _OnWindowPropertyChangedThatRequiresTemplateFixup);
                Utility.AddDependencyPropertyChangeListener(_window, Window.FlowDirectionProperty,
                    _OnWindowPropertyChangedThatRequiresTemplateFixup);
            }

            _window.Closed += _UnsetWindow;

            if (IntPtr.Zero != _hwnd)
            {
                _hwndSource = HwndSource.FromHwnd(_hwnd);
                Assert.IsNotNull(_hwndSource);
                _window.ApplyTemplate();

                if (_chromeInfo != null)
                {
                    _ApplyNewCustomChrome();
                }
            }
            else
            {
                _window.SourceInitialized += (sender, e) =>
                {
                    _hwnd = new WindowInteropHelper(_window).Handle;
                    Assert.IsNotDefault(_hwnd);
                    _hwndSource = HwndSource.FromHwnd(_hwnd);
                    Assert.IsNotNull(_hwndSource);

                    if (_chromeInfo != null)
                    {
                        _ApplyNewCustomChrome();
                    }
                };
            }
        }

        private void _UnsetWindow(object sender, EventArgs e)
        {
            if (Utility.IsPresentationFrameworkVersionLessThan4)
            {
                Utility.RemoveDependencyPropertyChangeListener(_window, Window.TemplateProperty,
                    _OnWindowPropertyChangedThatRequiresTemplateFixup);
                Utility.RemoveDependencyPropertyChangeListener(_window, Window.FlowDirectionProperty,
                    _OnWindowPropertyChangedThatRequiresTemplateFixup);
            }

            if (_chromeInfo != null)
            {
                _chromeInfo.PropertyChangedThatRequiresRepaint -= _OnChromePropertyChangedThatRequiresRepaint;
            }

            _RestoreStandardChromeState(true);
        }

        private void _ApplyNewCustomChrome()
        {
            if (_hwnd == IntPtr.Zero)
            {
                return;
            }

            if (_chromeInfo == null)
            {
                _RestoreStandardChromeState(false);
                return;
            }

            if (!_isHooked)
            {
                _hwndSource.AddHook(_WndProc);
                _isHooked = true;
            }

            _FixupFrameworkIssues();

            _UpdateSystemMenu(_window.WindowState);
            _UpdateFrameState(true);

            NativeMethods.SetWindowPos(_hwnd, IntPtr.Zero, 0, 0, 0, 0, SWP_FLAGS);
        }

        private void _FixupFrameworkIssues()
        {
            Assert.IsNotNull(_chromeInfo);
            Assert.IsNotNull(_window);

            if (!Utility.IsPresentationFrameworkVersionLessThan4)
            {
                return;
            }

            if (_window.Template == null)
            {
                return;
            }

            if (VisualTreeHelper.GetChildrenCount(_window) == 0)
            {
                _window.Dispatcher.BeginInvoke(DispatcherPriority.Loaded, (Action) _FixupFrameworkIssues);
                return;
            }

            var rootElement = (FrameworkElement) VisualTreeHelper.GetChild(_window, 0);

            Rect rcWindow = NativeMethods.GetWindowRect(_hwnd);
            Rect rcAdjustedClient = _GetAdjustedWindowRect(rcWindow);

            System.Windows.Rect rcLogicalWindow = DpiHelper.DeviceRectToLogical(new System.Windows.Rect(rcWindow.Left, rcWindow.Top, rcWindow.Width, rcWindow.Height));
            System.Windows.Rect rcLogicalClient = DpiHelper.DeviceRectToLogical(new System.Windows.Rect(rcAdjustedClient.Left, rcAdjustedClient.Top,
                rcAdjustedClient.Width, rcAdjustedClient.Height));

            Thickness nonClientThickness = new Thickness(
                rcLogicalWindow.Left - rcLogicalClient.Left,
                rcLogicalWindow.Top - rcLogicalClient.Top,
                rcLogicalClient.Right - rcLogicalWindow.Right,
                rcLogicalClient.Bottom - rcLogicalWindow.Bottom);

            rootElement.Margin = new Thickness(
                0,
                0,
                -(nonClientThickness.Left + nonClientThickness.Right),
                -(nonClientThickness.Top + nonClientThickness.Bottom));

            if (_window.FlowDirection == FlowDirection.RightToLeft)
            {
                rootElement.RenderTransform = new MatrixTransform(1, 0, 0, 1, -(nonClientThickness.Left + nonClientThickness.Right), 0);
            }
            else
            {
                rootElement.RenderTransform = null;
            }

            if (!_isFixedUp)
            {
                _hasUserMovedWindow = false;
                _window.StateChanged += _FixupRestoreBounds;

                _isFixedUp = true;
            }
        }

        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        private void _FixupWindows7Issues()
        {
            if (_blackGlassFixupAttemptCount > 5)
            {
                return;
            }

            if (Utility.IsOsWindows7OrNewer && NativeMethods.DwmIsCompositionEnabled())
            {
                ++_blackGlassFixupAttemptCount;

                bool success = false;
                try
                {
                    Dwm_Timing_Info? dti = NativeMethods.DwmGetCompositionTimingInfo(_hwnd);
                    success = dti != null;
                }
                catch (Exception)
                {
                    // ignored
                }

                if (!success)
                {
                    Dispatcher.BeginInvoke(DispatcherPriority.Loaded, (Action) _FixupWindows7Issues);
                }
                else
                {
                    _blackGlassFixupAttemptCount = 0;
                }
            }
        }

        private void _FixupRestoreBounds(object sender, EventArgs e)
        {
            Assert.IsTrue(Utility.IsPresentationFrameworkVersionLessThan4);
            if (_window.WindowState == WindowState.Maximized || _window.WindowState == WindowState.Minimized)
            {
                if (_hasUserMovedWindow)
                {
                    _hasUserMovedWindow = false;
                    Windowplacement wp = NativeMethods.GetWindowPlacement(_hwnd);

                    Rect adjustedDeviceRc = _GetAdjustedWindowRect(new Rect {Bottom = 100, Right = 100});
                    Point adjustedTopLeft = DpiHelper.DevicePixelsToLogical(
                        new Point(
                            wp.rcNormalPosition.Left - adjustedDeviceRc.Left,
                            wp.rcNormalPosition.Top - adjustedDeviceRc.Top));

                    _window.Top = adjustedTopLeft.Y;
                    _window.Left = adjustedTopLeft.X;
                }
            }
        }

        private Rect _GetAdjustedWindowRect(Rect rcWindow)
        {
            Assert.IsTrue(Utility.IsPresentationFrameworkVersionLessThan4);

            var style = (Ws) NativeMethods.GetWindowLongPtr(_hwnd, Gwl.Style);
            var exstyle = (Ws_Ex) NativeMethods.GetWindowLongPtr(_hwnd, Gwl.Exstyle);

            return NativeMethods.AdjustWindowRectEx(rcWindow, style, false, exstyle);
        }

        private bool _ModifyStyle(Ws removeStyle, Ws addStyle)
        {
            Assert.IsNotDefault(_hwnd);
            var dwStyle = (Ws) NativeMethods.GetWindowLongPtr(_hwnd, Gwl.Style).ToInt32();
            var dwNewStyle = (dwStyle & ~removeStyle) | addStyle;
            if (dwStyle == dwNewStyle)
            {
                return false;
            }

            NativeMethods.SetWindowLongPtr(_hwnd, Gwl.Style, new IntPtr((int) dwNewStyle));
            return true;
        }

        private WindowState _GetHwndState()
        {
            var wpl = NativeMethods.GetWindowPlacement(_hwnd);
            switch (wpl.showCmd)
            {
                case Sw.Showminimized: return WindowState.Minimized;
                case Sw.Showmaximized: return WindowState.Maximized;
            }

            return WindowState.Normal;
        }

        private System.Windows.Rect _GetWindowRect()
        {
            Rect windowPosition = NativeMethods.GetWindowRect(_hwnd);
            return new System.Windows.Rect(windowPosition.Left, windowPosition.Top, windowPosition.Width, windowPosition.Height);
        }

        private void _UpdateSystemMenu(WindowState? assumeState)
        {
            const Mf mfEnabled = Mf.Enabled | Mf.Bycommand;
            const Mf mfDisabled = Mf.Grayed | Mf.Disabled | Mf.Bycommand;

            WindowState state = assumeState ?? _GetHwndState();

            if (null != assumeState || _lastMenuState != state)
            {
                _lastMenuState = state;

                bool modified = _ModifyStyle(Ws.Visible, 0);
                IntPtr hmenu = NativeMethods.GetSystemMenu(_hwnd, false);
                if (IntPtr.Zero != hmenu)
                {
                    var dwStyle = (Ws) NativeMethods.GetWindowLongPtr(_hwnd, Gwl.Style).ToInt32();

                    bool canMinimize = Utility.IsFlagSet((int) dwStyle, (int) Ws.Minimizebox);
                    bool canMaximize = Utility.IsFlagSet((int) dwStyle, (int) Ws.Maximizebox);
                    bool canSize = Utility.IsFlagSet((int) dwStyle, (int) Ws.Thickframe);

                    switch (state)
                    {
                        case WindowState.Maximized:
                            NativeMethods.EnableMenuItem(hmenu, Sc.Restore, mfEnabled);
                            NativeMethods.EnableMenuItem(hmenu, Sc.Move, mfDisabled);
                            NativeMethods.EnableMenuItem(hmenu, Sc.Size, mfDisabled);
                            NativeMethods.EnableMenuItem(hmenu, Sc.Minimize, canMinimize ? mfEnabled : mfDisabled);
                            NativeMethods.EnableMenuItem(hmenu, Sc.Maximize, mfDisabled);
                            break;
                        case WindowState.Minimized:
                            NativeMethods.EnableMenuItem(hmenu, Sc.Restore, mfEnabled);
                            NativeMethods.EnableMenuItem(hmenu, Sc.Move, mfDisabled);
                            NativeMethods.EnableMenuItem(hmenu, Sc.Size, mfDisabled);
                            NativeMethods.EnableMenuItem(hmenu, Sc.Minimize, mfDisabled);
                            NativeMethods.EnableMenuItem(hmenu, Sc.Maximize, canMaximize ? mfEnabled : mfDisabled);
                            break;
                        default:
                            NativeMethods.EnableMenuItem(hmenu, Sc.Restore, mfDisabled);
                            NativeMethods.EnableMenuItem(hmenu, Sc.Move, mfEnabled);
                            NativeMethods.EnableMenuItem(hmenu, Sc.Size, canSize ? mfEnabled : mfDisabled);
                            NativeMethods.EnableMenuItem(hmenu, Sc.Minimize, canMinimize ? mfEnabled : mfDisabled);
                            NativeMethods.EnableMenuItem(hmenu, Sc.Maximize, canMaximize ? mfEnabled : mfDisabled);
                            break;
                    }
                }

                if (modified)
                {
                    _ModifyStyle(0, Ws.Visible);
                }
            }
        }

        private void _UpdateFrameState(bool force)
        {
            if (IntPtr.Zero == _hwnd)
            {
                return;
            }

            bool frameState = NativeMethods.DwmIsCompositionEnabled();

            if (force || frameState != _isGlassEnabled)
            {
                _isGlassEnabled = frameState && _chromeInfo.GlassFrameThickness != default(Thickness);

                if (!_isGlassEnabled)
                {
                    _SetRoundingRegion(null);
                }
                else
                {
                    _ClearRoundingRegion();
                    _ExtendGlassFrame();

                    _FixupWindows7Issues();
                }

                NativeMethods.SetWindowPos(_hwnd, IntPtr.Zero, 0, 0, 0, 0, SWP_FLAGS);
            }
        }

        private void _ClearRoundingRegion()
        {
            NativeMethods.SetWindowRgn(_hwnd, IntPtr.Zero, NativeMethods.IsWindowVisible(_hwnd));
        }

        private void _SetRoundingRegion(Windowpos? wp)
        {
            const int monitorDefaulttonearest = 0x00000002;

            Windowplacement wpl = NativeMethods.GetWindowPlacement(_hwnd);

            if (wpl.showCmd == Sw.Showmaximized)
            {
                int left;
                int top;

                if (wp.HasValue)
                {
                    left = wp.Value.x;
                    top = wp.Value.y;
                }
                else
                {
                    System.Windows.Rect r = _GetWindowRect();
                    left = (int) r.Left;
                    top = (int) r.Top;
                }

                IntPtr hMon = NativeMethods.MonitorFromWindow(_hwnd, monitorDefaulttonearest);

                Monitorinfo mi = NativeMethods.GetMonitorInfo(hMon);
                Rect rcMax = mi.rcWork;
                rcMax.Offset(-left, -top);

                IntPtr hrgn = IntPtr.Zero;
                try
                {
                    hrgn = NativeMethods.CreateRectRgnIndirect(rcMax);
                    NativeMethods.SetWindowRgn(_hwnd, hrgn, NativeMethods.IsWindowVisible(_hwnd));
                    hrgn = IntPtr.Zero;
                }
                finally
                {
                    Utility.SafeDeleteObject(ref hrgn);
                }
            }
            else
            {
                Size windowSize;

                if (null != wp && !Utility.IsFlagSet(wp.Value.flags, (int) Swp.Nosize))
                {
                    windowSize = new Size((double) wp.Value.cx, (double) wp.Value.cy);
                }
                else if (null != wp && _lastRoundingState == _window.WindowState)
                {
                    return;
                }
                else
                {
                    windowSize = _GetWindowRect().Size;
                }

                _lastRoundingState = _window.WindowState;

                IntPtr hrgn = IntPtr.Zero;
                try
                {
                    double shortestDimension = Math.Min(windowSize.Width, windowSize.Height);

                    double topLeftRadius = DpiHelper.LogicalPixelsToDevice(new Point(_chromeInfo.CornerRadius.TopLeft, 0)).X;
                    topLeftRadius = Math.Min(topLeftRadius, shortestDimension / 2);

                    if (_IsUniform(_chromeInfo.CornerRadius))
                    {
                        hrgn = _CreateRoundRectRgn(new System.Windows.Rect(windowSize), topLeftRadius);
                    }
                    else
                    {
                        hrgn = _CreateRoundRectRgn(
                            new System.Windows.Rect(0, 0, windowSize.Width / 2 + topLeftRadius, windowSize.Height / 2 + topLeftRadius), topLeftRadius);

                        double topRightRadius = DpiHelper.LogicalPixelsToDevice(new Point(_chromeInfo.CornerRadius.TopRight, 0)).X;
                        topRightRadius = Math.Min(topRightRadius, shortestDimension / 2);
                        System.Windows.Rect topRightRegionRect = new System.Windows.Rect(0, 0, windowSize.Width / 2 + topRightRadius,
                            windowSize.Height / 2 + topRightRadius);
                        topRightRegionRect.Offset(windowSize.Width / 2 - topRightRadius, 0);
                        Assert.AreEqual(topRightRegionRect.Right, windowSize.Width);

                        _CreateAndCombineRoundRectRgn(hrgn, topRightRegionRect, topRightRadius);

                        double bottomLeftRadius = DpiHelper.LogicalPixelsToDevice(new Point(_chromeInfo.CornerRadius.BottomLeft, 0)).X;
                        bottomLeftRadius = Math.Min(bottomLeftRadius, shortestDimension / 2);
                        System.Windows.Rect bottomLeftRegionRect = new System.Windows.Rect(0, 0, windowSize.Width / 2 + bottomLeftRadius,
                            windowSize.Height / 2 + bottomLeftRadius);
                        bottomLeftRegionRect.Offset(0, windowSize.Height / 2 - bottomLeftRadius);
                        Assert.AreEqual(bottomLeftRegionRect.Bottom, windowSize.Height);

                        _CreateAndCombineRoundRectRgn(hrgn, bottomLeftRegionRect, bottomLeftRadius);

                        double bottomRightRadius = DpiHelper.LogicalPixelsToDevice(new Point(_chromeInfo.CornerRadius.BottomRight, 0)).X;
                        bottomRightRadius = Math.Min(bottomRightRadius, shortestDimension / 2);
                        System.Windows.Rect bottomRightRegionRect = new System.Windows.Rect(0, 0, windowSize.Width / 2 + bottomRightRadius,
                            windowSize.Height / 2 + bottomRightRadius);
                        bottomRightRegionRect.Offset(windowSize.Width / 2 - bottomRightRadius, windowSize.Height / 2 - bottomRightRadius);
                        Assert.AreEqual(bottomRightRegionRect.Right, windowSize.Width);
                        Assert.AreEqual(bottomRightRegionRect.Bottom, windowSize.Height);

                        _CreateAndCombineRoundRectRgn(hrgn, bottomRightRegionRect, bottomRightRadius);
                    }

                    NativeMethods.SetWindowRgn(_hwnd, hrgn, NativeMethods.IsWindowVisible(_hwnd));
                    hrgn = IntPtr.Zero;
                }
                finally
                {
                    Utility.SafeDeleteObject(ref hrgn);
                }
            }
        }

        private void _ExtendGlassFrame()
        {
            Assert.IsNotNull(_window);

            if (!Utility.IsOsVistaOrNewer)
            {
                return;
            }

            if (IntPtr.Zero == _hwnd)
            {
                return;
            }

            if (!NativeMethods.DwmIsCompositionEnabled())
            {
                _hwndSource.CompositionTarget.BackgroundColor = SystemColors.WindowColor;
            }
            else
            {
                _hwndSource.CompositionTarget.BackgroundColor = Colors.Transparent;

                Point deviceTopLeft =
                    DpiHelper.LogicalPixelsToDevice(new Point(_chromeInfo.GlassFrameThickness.Left, _chromeInfo.GlassFrameThickness.Top));
                Point deviceBottomRight =
                    DpiHelper.LogicalPixelsToDevice(
                        new Point(_chromeInfo.GlassFrameThickness.Right, _chromeInfo.GlassFrameThickness.Bottom));

                var dwmMargin = new Margins
                {
                    cxLeftWidth = (int) Math.Ceiling(deviceTopLeft.X),
                    cxRightWidth = (int) Math.Ceiling(deviceBottomRight.X),
                    cyTopHeight = (int) Math.Ceiling(deviceTopLeft.Y),
                    cyBottomHeight = (int) Math.Ceiling(deviceBottomRight.Y),
                };

                NativeMethods.DwmExtendFrameIntoClientArea(_hwnd, ref dwmMargin);
            }
        }

        private Ht _HitTestNca(System.Windows.Rect windowPosition, Point mousePosition)
        {
            int uRow = 1;
            int uCol = 1;
            bool onResizeBorder = false;

            if (mousePosition.Y >= windowPosition.Top &&
                mousePosition.Y < windowPosition.Top + _chromeInfo.ResizeBorderThickness.Top + _chromeInfo.CaptionHeight)
            {
                onResizeBorder = mousePosition.Y < windowPosition.Top + _chromeInfo.ResizeBorderThickness.Top;
                uRow = 0; 
            }
            else if (mousePosition.Y < windowPosition.Bottom &&
                     mousePosition.Y >= windowPosition.Bottom - (int) _chromeInfo.ResizeBorderThickness.Bottom)
            {
                uRow = 2; 
            }

            if (mousePosition.X >= windowPosition.Left &&
                mousePosition.X < windowPosition.Left + (int) _chromeInfo.ResizeBorderThickness.Left)
            {
                uCol = 0;
            }
            else if (mousePosition.X < windowPosition.Right &&
                     mousePosition.X >= windowPosition.Right - _chromeInfo.ResizeBorderThickness.Right)
            {
                uCol = 2; 
            }

            if (uRow == 0 && uCol != 1 && !onResizeBorder)
            {
                uRow = 1;
            }

            Ht ht = HitTestBorders[uRow, uCol];

            if (ht == Ht.Top && !onResizeBorder)
            {
                ht = Ht.Caption;
            }

            return ht;
        }

        #endregion

        #region Messages

        private void _OnChromePropertyChangedThatRequiresRepaint(object sender, EventArgs e)
        {
            _UpdateFrameState(true);
        }

        private static void _OnChromeWorkerChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var w = (Window) d;
            var cw = (WindowChromeWorker) e.NewValue;

            // The WindowChromeWorker object should only be set on the window once, and never to null.
            Assert.IsNotNull(w);
            Assert.IsNotNull(cw);
            Assert.IsNull(cw._window);

            cw._SetWindow(w);
        }

        private void _OnWindowPropertyChangedThatRequiresTemplateFixup(object sender, EventArgs e)
        {
            Assert.IsTrue(Utility.IsPresentationFrameworkVersionLessThan4);

            if (_chromeInfo != null && _hwnd != IntPtr.Zero)
            {
                // Assume that when the template changes it's going to be applied.
                // We don't have a good way to externally hook into the template
                // actually being applied, so we asynchronously post the fixup operation
                // at Loaded priority, so it's expected that the visual tree will be
                // updated before _FixupFrameworkIssues is called.
                _window.Dispatcher.BeginInvoke(DispatcherPriority.Loaded, (Action) _FixupFrameworkIssues);
            }
        }

        #endregion

        #region Other members

        // Delegate signature used for Dispatcher.BeginInvoke.
        private delegate void Action();

        #endregion

        #region Fields

        private const Swp SWP_FLAGS = Swp.Framechanged | Swp.Nosize | Swp.Nomove | Swp.Nozorder | Swp.Noownerzorder | Swp.Noactivate;

        private readonly List<HANDLE_MESSAGE> _messageTable;
        private Window _window;
        private IntPtr _hwnd;
        private HwndSource _hwndSource = null;
        private bool _isHooked = false;

        // These fields are for tracking workarounds for WPF 3.5SP1 behaviors.
        private bool _isFixedUp = false;
        private bool _isUserResizing = false;
        private bool _hasUserMovedWindow = false;
        private Point _windowPosAtStartOfUserMove = default(Point);

        // Field to track attempts to force off Device Bitmaps on Win7.
        private int _blackGlassFixupAttemptCount;
        private WindowChrome _chromeInfo;

        // Keep track of this so we can detect when we need to apply changes.  Tracking these separately
        // as I've seen using just one cause things to get enough out of sync that occasionally the caption will redraw.
        private WindowState _lastRoundingState;
        private WindowState _lastMenuState;
        private bool _isGlassEnabled;

        #endregion

        #region WindowProc and Message Handlers

        private IntPtr _WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            // Only expecting messages for our cached HWND.
            Assert.AreEqual(hwnd, _hwnd);

            var message = (Wm) msg;
            foreach (var handlePair in _messageTable)
            {
                if (handlePair.Key == message)
                {
                    return handlePair.Value(message, wParam, lParam, out handled);
                }
            }

            return IntPtr.Zero;
        }

        private IntPtr _HandleSetTextOrIcon(Wm uMsg, IntPtr wParam, IntPtr lParam, out bool handled)
        {
            bool modified = _ModifyStyle(Ws.Visible, 0);

            // Setting the caption text and icon cause Windows to redraw the caption.
            // Letting the default WndProc handle the message without the WS_VISIBLE
            // style applied bypasses the redraw.
            IntPtr lRet = NativeMethods.DefWindowProc(_hwnd, uMsg, wParam, lParam);

            // Put back the style we removed.
            if (modified)
            {
                _ModifyStyle(0, Ws.Visible);
            }

            handled = true;
            return lRet;
        }

        private IntPtr _HandleNCActivate(Wm uMsg, IntPtr wParam, IntPtr lParam, out bool handled)
        {
            // Despite MSDN's documentation of lParam not being used,
            // calling DefWindowProc with lParam set to -1 causes Windows not to draw over the caption.

            // Directly call DefWindowProc with a custom parameter
            // which bypasses any other handling of the message.
            IntPtr lRet = NativeMethods.DefWindowProc(_hwnd, Wm.Ncactivate, wParam, new IntPtr(-1));
            handled = true;
            return lRet;
        }

        private IntPtr _HandleNCCalcSize(Wm uMsg, IntPtr wParam, IntPtr lParam, out bool handled)
        {
            // lParam is an [in, out] that can be either a RECT* (wParam == FALSE) or an NCCALCSIZE_PARAMS*.
            // Since the first field of NCCALCSIZE_PARAMS is a RECT and is the only field we care about
            // we can unconditionally treat it as a RECT.

            // Since we always want the client size to equal the window size, we can unconditionally handle it
            // without having to modify the parameters.

            handled = true;
            return new IntPtr((int) Wvr.Redraw);
        }

        private IntPtr _HandleNCHitTest(Wm uMsg, IntPtr wParam, IntPtr lParam, out bool handled)
        {
            IntPtr lRet = IntPtr.Zero;
            handled = false;

            // Give DWM a chance at this first.
            if (Utility.IsOsVistaOrNewer && _chromeInfo.GlassFrameThickness != default(Thickness) && _isGlassEnabled)
            {
                // If we're on Vista, give the DWM a chance to handle the message first.
                handled = NativeMethods.DwmDefWindowProc(_hwnd, uMsg, wParam, lParam, out lRet);
            }

            // Handle letting the system know if we consider the mouse to be in our effective non-client area.
            // If DWM already handled this by way of DwmDefWindowProc, then respect their call.
            if (IntPtr.Zero == lRet)
            {
                var mousePosScreen = new Point(Utility.GET_X_LPARAM(lParam), Utility.GET_Y_LPARAM(lParam));
                System.Windows.Rect windowPosition = _GetWindowRect();

                Ht ht = _HitTestNca(
                    DpiHelper.DeviceRectToLogical(windowPosition),
                    DpiHelper.DevicePixelsToLogical(mousePosScreen));

                // Don't blindly respect HTCAPTION.
                // We want UIElements in the caption area to be actionable so run through a hittest first.
                if (ht != Ht.Client)
                {
                    Point mousePosWindow = mousePosScreen;
                    mousePosWindow.Offset(-windowPosition.X, -windowPosition.Y);
                    mousePosWindow = DpiHelper.DevicePixelsToLogical(mousePosWindow);
                    IInputElement inputElement = _window.InputHitTest(mousePosWindow);
                    if (inputElement != null && WindowChrome.GetIsHitTestVisibleInChrome(inputElement))
                    {
                        ht = Ht.Client;
                    }
                }

                handled = true;
                lRet = new IntPtr((int) ht);
            }

            return lRet;
        }

        private IntPtr _HandleNCRButtonUp(Wm uMsg, IntPtr wParam, IntPtr lParam, out bool handled)
        {
            // Emulate the system behavior of clicking the right mouse button over the caption area
            // to bring up the system menu.
            if (Ht.Caption == (Ht) wParam.ToInt32())
            {
                if (_window.ContextMenu != null)
                {
                    _window.ContextMenu.Placement = PlacementMode.MousePoint;
                    _window.ContextMenu.IsOpen = true;
                }
                else if (WindowChrome.GetWindowChrome(_window).ShowSystemMenu)
                    SystemCommands.ShowSystemMenuPhysicalCoordinates(_window,
                        new Point(Utility.GET_X_LPARAM(lParam), Utility.GET_Y_LPARAM(lParam)));
            }

            handled = false;
            return IntPtr.Zero;
        }

        private IntPtr _HandleSize(Wm uMsg, IntPtr wParam, IntPtr lParam, out bool handled)
        {
            const int sizeMaximized = 2;

            // Force when maximized.
            // We can tell what's happening right now, but the Window doesn't yet know it's
            // maximized.  Not forcing this update will eventually cause the
            // default caption to be drawn.
            WindowState? state = null;
            if (wParam.ToInt32() == sizeMaximized)
            {
                state = WindowState.Maximized;
            }

            _UpdateSystemMenu(state);

            // Still let the default WndProc handle this.
            handled = false;
            return IntPtr.Zero;
        }

        private IntPtr _HandleWindowPosChanged(Wm uMsg, IntPtr wParam, IntPtr lParam, out bool handled)
        {
            // http://blogs.msdn.com/oldnewthing/archive/2008/01/15/7113860.aspx
            // The WM_WINDOWPOSCHANGED message is sent at the end of the window
            // state change process. It sort of combines the other state change
            // notifications, WM_MOVE, WM_SIZE, and WM_SHOWWINDOW. But it doesn't
            // suffer from the same limitations as WM_SHOWWINDOW, so you can
            // reliably use it to react to the window being shown or hidden.

            _UpdateSystemMenu(null);

            if (!_isGlassEnabled)
            {
                Assert.IsNotDefault(lParam);
                var wp = (Windowpos) Marshal.PtrToStructure(lParam, typeof(Windowpos));
                _SetRoundingRegion(wp);
            }

            // Still want to pass this to DefWndProc
            handled = false;
            return IntPtr.Zero;
        }

        private IntPtr _HandleDwmCompositionChanged(Wm uMsg, IntPtr wParam, IntPtr lParam, out bool handled)
        {
            _UpdateFrameState(false);

            handled = false;
            return IntPtr.Zero;
        }

        private IntPtr _HandleSettingChange(Wm uMsg, IntPtr wParam, IntPtr lParam, out bool handled)
        {
            // There are several settings that can cause fixups for the template to become invalid when changed.
            // These shouldn't be required on the v4 framework.
            Assert.IsTrue(Utility.IsPresentationFrameworkVersionLessThan4);

            _FixupFrameworkIssues();

            handled = false;
            return IntPtr.Zero;
        }

        private IntPtr _HandleEnterSizeMove(Wm uMsg, IntPtr wParam, IntPtr lParam, out bool handled)
        {
            // This is only intercepted to deal with bugs in Window in .Net 3.5 and below.
            Assert.IsTrue(Utility.IsPresentationFrameworkVersionLessThan4);

            _isUserResizing = true;

            // On Win7 if the user is dragging the window out of the maximized state then we don't want to use that location
            // as a restore point.
            Assert.Implies(_window.WindowState == WindowState.Maximized, Utility.IsOsWindows7OrNewer);
            if (_window.WindowState != WindowState.Maximized)
            {
                // Check for the docked window case.  The window can still be restored when it's in this position so 
                // try to account for that and not update the start position.
                if (!IsWindowDocked)
                {
                    _windowPosAtStartOfUserMove = new Point(_window.Left, _window.Top);
                }

                // Realistically we also don't want to update the start position when moving from one docked state to another (or to and from maximized),
                // but it's tricky to detect and this is already a workaround for a bug that's fixed in newer versions of the framework.
                // Not going to try to handle all cases.
            }

            handled = false;
            return IntPtr.Zero;
        }

        private IntPtr _HandleExitSizeMove(Wm uMsg, IntPtr wParam, IntPtr lParam, out bool handled)
        {
            // This is only intercepted to deal with bugs in Window in .Net 3.5 and below.
            Assert.IsTrue(Utility.IsPresentationFrameworkVersionLessThan4);

            _isUserResizing = false;

            // On Win7 the user can change the Window's state by dragging the window to the top of the monitor.
            // If they did that, then we need to try to update the restore bounds or else WPF will put the window at the maximized location (e.g. (-8,-8)).
            if (_window.WindowState == WindowState.Maximized)
            {
                Assert.IsTrue(Utility.IsOsWindows7OrNewer);
                _window.Top = _windowPosAtStartOfUserMove.Y;
                _window.Left = _windowPosAtStartOfUserMove.X;
            }

            handled = false;
            return IntPtr.Zero;
        }

        private IntPtr _HandleMove(Wm uMsg, IntPtr wParam, IntPtr lParam, out bool handled)
        {
            // This is only intercepted to deal with bugs in Window in .Net 3.5 and below.
            Assert.IsTrue(Utility.IsPresentationFrameworkVersionLessThan4);

            if (_isUserResizing)
            {
                _hasUserMovedWindow = true;
            }

            handled = false;
            return IntPtr.Zero;
        }

        #endregion

        #region Remove Custom Chrome Methods

        private void _RestoreStandardChromeState(bool isClosing)
        {
            VerifyAccess();

            _UnhookCustomChrome();

            if (!isClosing)
            {
                _RestoreFrameworkIssueFixups();
                _RestoreGlassFrame();
                _RestoreHrgn();

                _window.InvalidateMeasure();
            }
        }

        private void _UnhookCustomChrome()
        {
            Assert.IsNotDefault(_hwnd);
            Assert.IsNotNull(_window);

            if (_isHooked)
            {
                _hwndSource.RemoveHook(_WndProc);
                _isHooked = false;
            }
        }

        private void _RestoreFrameworkIssueFixups()
        {
            // This margin is only necessary if the client rect is going to be calculated incorrectly by WPF.
            // This bug was fixed in V4 of the framework.
            if (Utility.IsPresentationFrameworkVersionLessThan4)
            {
                Assert.IsTrue(_isFixedUp);

                var rootElement = (FrameworkElement) VisualTreeHelper.GetChild(_window, 0);
                // Undo anything that was done before.
                rootElement.Margin = new Thickness();

                _window.StateChanged -= _FixupRestoreBounds;
                _isFixedUp = false;
            }
        }

        private void _RestoreGlassFrame()
        {
            Assert.IsNull(_chromeInfo);
            Assert.IsNotNull(_window);

            // Expect that this might be called on OSes other than Vista
            // and if the window hasn't yet been shown, then we don't need to undo anything.
            if (!Utility.IsOsVistaOrNewer || _hwnd == IntPtr.Zero)
            {
                return;
            }

            _hwndSource.CompositionTarget.BackgroundColor = SystemColors.WindowColor;

            if (NativeMethods.DwmIsCompositionEnabled())
            {
                // If glass is enabled, push it back to the normal bounds.
                var dwmMargin = new Margins();
                NativeMethods.DwmExtendFrameIntoClientArea(_hwnd, ref dwmMargin);
            }
        }

        private void _RestoreHrgn()
        {
            _ClearRoundingRegion();
            NativeMethods.SetWindowPos(_hwnd, IntPtr.Zero, 0, 0, 0, 0, SWP_FLAGS);
        }

        #endregion
    }
}