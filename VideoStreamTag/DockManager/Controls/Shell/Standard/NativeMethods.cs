﻿#region

using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Permissions;
using System.Text;
using Microsoft.Win32.SafeHandles;

#endregion

namespace DockManager.Controls.Shell.Standard
{
    #region Native Values

    internal static class Win32Value
    {
        #region Const

        public const uint MAX_PATH = 260;
        public const uint INFOTIPSIZE = 1024;
        public const uint TRUE = 1;
        public const uint FALSE = 0;
        public const uint SIZEOF_WCHAR = 2;
        public const uint SIZEOF_CHAR = 1;
        public const uint SIZEOF_BOOL = 4;

        #endregion
    }

    [Flags]
    internal enum Hcf
    {
        Highcontraston = 0x00000001,
        Available = 0x00000002,
        Hotkeyactive = 0x00000004,
        Confirmhotkey = 0x00000008,
        Hotkeysound = 0x00000010,
        Indicator = 0x00000020,
        Hotkeyavailable = 0x00000040,
    }

    internal enum Bi
    {
        Rgb = 0,
    }

    internal enum Rgn
    {
        And = 1,
        Or = 2,
        Xor = 3,
        Diff = 4,
        Copy = 5,
    }

    internal enum CombineRgnResult
    {
        Error = 0,
        Nullregion = 1,
        Simpleregion = 2,
        Complexregion = 3,
    }

    internal enum Olecmdexecopt
    {
        Dodefault = 0,
        Promptuser = 1,
        Dontpromptuser = 2,
        Showhelp = 3
    }

    internal enum Olecmdf
    {
        Supported = 1,
        Enabled = 2,
        Latched = 4,
        Ninched = 8,
        Invisible = 16,
        Defhideonctxtmenu = 32
    }

    internal enum Olecmdid
    {
        Open = 1,
        New = 2,
        Save = 3,
        Saveas = 4,
        Savecopyas = 5,
        Print = 6,
        Printpreview = 7,
        Pagesetup = 8,
        Spell = 9,
        Properties = 10,
        Cut = 11,
        Copy = 12,
        Paste = 13,
        Pastespecial = 14,
        Undo = 15,
        Redo = 16,
        Selectall = 17,
        Clearselection = 18,
        Zoom = 19,
        Getzoomrange = 20,
        Updatecommands = 21,
        Refresh = 22,
        Stop = 23,
        Hidetoolbars = 24,
        Setprogressmax = 25,
        Setprogresspos = 26,
        Setprogresstext = 27,
        Settitle = 28,
        Setdownloadstate = 29,
        Stopdownload = 30,
        Ontoolbaractivated = 31,
        Find = 32,
        Delete = 33,
        Httpequiv = 34,
        HttpequivDone = 35,
        EnableInteraction = 36,
        Onunload = 37,
        Propertybag2 = 38,
        Prerefresh = 39,
        Showscripterror = 40,
        Showmessage = 41,
        Showfind = 42,
        Showpagesetup = 43,
        Showprint = 44,
        Close = 45,
        Allowuilesssaveas = 46,
        Dontdownloadcss = 47,
        Updatepagestatus = 48,
        Print2 = 49,
        Printpreview2 = 50,
        Setprinttemplate = 51,
        Getprinttemplate = 52,
        Pageactionblocked = 55,
        Pageactionuiquery = 56,
        Focusviewcontrols = 57,
        Focusviewcontrolsquery = 58,
        Showpageactionmenu = 59
    }

    internal enum Readystate
    {
        Uninitialized = 0,
        Loading = 1,
        Loaded = 2,
        Interactive = 3,
        Complete = 4
    }

    internal enum Dogif
    {
        Default = 0x0000,
        TraverseLink = 0x0001, 
        NoHdrop = 0x0002, 
        NoUrl = 0x0004,
        OnlyIfOne = 0x0008, 
    }

    internal enum Dwm_Sit
    {
        None,
        Displayframe = 1,
    }

    [Flags]
    internal enum ErrorModes
    {
        Default = 0x0,
        FailCriticalErrors = 0x1,
        NoGpFaultErrorBox = 0x2,
        NoAlignmentFaultExcept = 0x4,
        NoOpenFileErrorBox = 0x8000
    }

    internal enum Ht
    {
        Error = -2,
        Transparent = -1,
        Nowhere = 0,
        Client = 1,
        Caption = 2,
        Sysmenu = 3,
        Growbox = 4,
        Size = Growbox,
        Menu = 5,
        Hscroll = 6,
        Vscroll = 7,
        Minbutton = 8,
        Maxbutton = 9,
        Left = 10,
        Right = 11,
        Top = 12,
        Topleft = 13,
        Topright = 14,
        Bottom = 15,
        Bottomleft = 16,
        Bottomright = 17,
        Border = 18,
        Reduce = Minbutton,
        Zoom = Maxbutton,
        Sizefirst = Left,
        Sizelast = Bottomright,
        Object = 19,
        Close = 20,
        Help = 21
    }

    internal enum Gclp
    {
        Hbrbackground = -10,
    }

    internal enum Gwl
    {
        Wndproc = -4,
        Hinstance = -6,
        Hwndparent = -8,
        Style = -16,
        Exstyle = -20,
        Userdata = -21,
        Id = -12
    }

    internal enum Sm
    {
        Cxscreen = 0,
        Cyscreen = 1,
        Cxvscroll = 2,
        Cyhscroll = 3,
        Cycaption = 4,
        Cxborder = 5,
        Cyborder = 6,
        Cxfixedframe = 7,
        Cyfixedframe = 8,
        Cyvthumb = 9,
        Cxhthumb = 10,
        Cxicon = 11,
        Cyicon = 12,
        Cxcursor = 13,
        Cycursor = 14,
        Cymenu = 15,
        Cxfullscreen = 16,
        Cyfullscreen = 17,
        Cykanjiwindow = 18,
        Mousepresent = 19,
        Cyvscroll = 20,
        Cxhscroll = 21,
        Debug = 22,
        Swapbutton = 23,
        Cxmin = 28,
        Cymin = 29,
        Cxsize = 30,
        Cysize = 31,
        Cxframe = 32,
        Cxsizeframe = Cxframe,
        Cyframe = 33,
        Cysizeframe = Cyframe,
        Cxmintrack = 34,
        Cymintrack = 35,
        Cxdoubleclk = 36,
        Cydoubleclk = 37,
        Cxiconspacing = 38,
        Cyiconspacing = 39,
        Menudropalignment = 40,
        Penwindows = 41,
        Dbcsenabled = 42,
        Cmousebuttons = 43,
        Secure = 44,
        Cxedge = 45,
        Cyedge = 46,
        Cxminspacing = 47,
        Cyminspacing = 48,
        Cxsmicon = 49,
        Cysmicon = 50,
        Cysmcaption = 51,
        Cxsmsize = 52,
        Cysmsize = 53,
        Cxmenusize = 54,
        Cymenusize = 55,
        Arrange = 56,
        Cxminimized = 57,
        Cyminimized = 58,
        Cxmaxtrack = 59,
        Cymaxtrack = 60,
        Cxmaximized = 61,
        Cymaximized = 62,
        Network = 63,
        Cleanboot = 67,
        Cxdrag = 68,
        Cydrag = 69,
        Showsounds = 70,
        Cxmenucheck = 71,
        Cymenucheck = 72,
        Slowmachine = 73,
        Mideastenabled = 74,
        Mousewheelpresent = 75,
        Xvirtualscreen = 76,
        Yvirtualscreen = 77,
        Cxvirtualscreen = 78,
        Cyvirtualscreen = 79,
        Cmonitors = 80,
        Samedisplayformat = 81,
        Immenabled = 82,
        Cxfocusborder = 83,
        Cyfocusborder = 84,
        Tabletpc = 86,
        Mediacenter = 87,
        Remotesession = 0x1000,
        Remotecontrol = 0x2001,
    }

    internal enum Spi
    {
        Getbeep = 0x0001,
        Setbeep = 0x0002,
        Getmouse = 0x0003,
        Setmouse = 0x0004,
        Getborder = 0x0005,
        Setborder = 0x0006,
        Getkeyboardspeed = 0x000A,
        Setkeyboardspeed = 0x000B,
        Langdriver = 0x000C,
        Iconhorizontalspacing = 0x000D,
        Getscreensavetimeout = 0x000E,
        Setscreensavetimeout = 0x000F,
        Getscreensaveactive = 0x0010,
        Setscreensaveactive = 0x0011,
        Getgridgranularity = 0x0012,
        Setgridgranularity = 0x0013,
        Setdeskwallpaper = 0x0014,
        Setdeskpattern = 0x0015,
        Getkeyboarddelay = 0x0016,
        Setkeyboarddelay = 0x0017,
        Iconverticalspacing = 0x0018,
        Geticontitlewrap = 0x0019,
        Seticontitlewrap = 0x001A,
        Getmenudropalignment = 0x001B,
        Setmenudropalignment = 0x001C,
        Setdoubleclkwidth = 0x001D,
        Setdoubleclkheight = 0x001E,
        Geticontitlelogfont = 0x001F,
        Setdoubleclicktime = 0x0020,
        Setmousebuttonswap = 0x0021,
        Seticontitlelogfont = 0x0022,
        Getfasttaskswitch = 0x0023,
        Setfasttaskswitch = 0x0024,

        Setdragfullwindows = 0x0025,
        Getdragfullwindows = 0x0026,
        Getnonclientmetrics = 0x0029,
        Setnonclientmetrics = 0x002A,
        Getminimizedmetrics = 0x002B,
        Setminimizedmetrics = 0x002C,
        Geticonmetrics = 0x002D,
        Seticonmetrics = 0x002E,
        Setworkarea = 0x002F,
        Getworkarea = 0x0030,
        Setpenwindows = 0x0031,
        Gethighcontrast = 0x0042,
        Sethighcontrast = 0x0043,
        Getkeyboardpref = 0x0044,
        Setkeyboardpref = 0x0045,
        Getscreenreader = 0x0046,
        Setscreenreader = 0x0047,
        Getanimation = 0x0048,
        Setanimation = 0x0049,
        Getfontsmoothing = 0x004A,
        Setfontsmoothing = 0x004B,
        Setdragwidth = 0x004C,
        Setdragheight = 0x004D,
        Sethandheld = 0x004E,
        Getlowpowertimeout = 0x004F,
        Getpowerofftimeout = 0x0050,
        Setlowpowertimeout = 0x0051,
        Setpowerofftimeout = 0x0052,
        Getlowpoweractive = 0x0053,
        Getpoweroffactive = 0x0054,
        Setlowpoweractive = 0x0055,
        Setpoweroffactive = 0x0056,
        Setcursors = 0x0057,
        Seticons = 0x0058,
        Getdefaultinputlang = 0x0059,
        Setdefaultinputlang = 0x005A,
        Setlangtoggle = 0x005B,
        Getwindowsextension = 0x005C,
        Setmousetrails = 0x005D,
        Getmousetrails = 0x005E,
        Setscreensaverrunning = 0x0061,
        Screensaverrunning = Setscreensaverrunning,
        Getfilterkeys = 0x0032,
        Setfilterkeys = 0x0033,
        Gettogglekeys = 0x0034,
        Settogglekeys = 0x0035,
        Getmousekeys = 0x0036,
        Setmousekeys = 0x0037,
        Getshowsounds = 0x0038,
        Setshowsounds = 0x0039,
        Getstickykeys = 0x003A,
        Setstickykeys = 0x003B,
        Getaccesstimeout = 0x003C,
        Setaccesstimeout = 0x003D,

        Getserialkeys = 0x003E,
        Setserialkeys = 0x003F,
        Getsoundsentry = 0x0040,
        Setsoundsentry = 0x0041,
        Getsnaptodefbutton = 0x005F,
        Setsnaptodefbutton = 0x0060,
        Getmousehoverwidth = 0x0062,
        Setmousehoverwidth = 0x0063,
        Getmousehoverheight = 0x0064,
        Setmousehoverheight = 0x0065,
        Getmousehovertime = 0x0066,
        Setmousehovertime = 0x0067,
        Getwheelscrolllines = 0x0068,
        Setwheelscrolllines = 0x0069,
        Getmenushowdelay = 0x006A,
        Setmenushowdelay = 0x006B,

        Getwheelscrollchars = 0x006C,
        Setwheelscrollchars = 0x006D,

        Getshowimeui = 0x006E,
        Setshowimeui = 0x006F,

        Getmousespeed = 0x0070,
        Setmousespeed = 0x0071,
        Getscreensaverrunning = 0x0072,
        Getdeskwallpaper = 0x0073,

        Getaudiodescription = 0x0074,
        Setaudiodescription = 0x0075,

        Getscreensavesecure = 0x0076,
        Setscreensavesecure = 0x0077,

        Gethungapptimeout = 0x0078,
        Sethungapptimeout = 0x0079,
        Getwaittokilltimeout = 0x007A,
        Setwaittokilltimeout = 0x007B,
        Getwaittokillservicetimeout = 0x007C,
        Setwaittokillservicetimeout = 0x007D,
        Getmousedockthreshold = 0x007E,
        Setmousedockthreshold = 0x007F,
        Getpendockthreshold = 0x0080,
        Setpendockthreshold = 0x0081,
        Getwinarranging = 0x0082,
        Setwinarranging = 0x0083,
        Getmousedragoutthreshold = 0x0084,
        Setmousedragoutthreshold = 0x0085,
        Getpendragoutthreshold = 0x0086,
        Setpendragoutthreshold = 0x0087,
        Getmousesidemovethreshold = 0x0088,
        Setmousesidemovethreshold = 0x0089,
        Getpensidemovethreshold = 0x008A,
        Setpensidemovethreshold = 0x008B,
        Getdragfrommaximize = 0x008C,
        Setdragfrommaximize = 0x008D,
        Getsnapsizing = 0x008E,
        Setsnapsizing = 0x008F,
        Getdockmoving = 0x0090,
        Setdockmoving = 0x0091,

        Getactivewindowtracking = 0x1000,
        Setactivewindowtracking = 0x1001,
        Getmenuanimation = 0x1002,
        Setmenuanimation = 0x1003,
        Getcomboboxanimation = 0x1004,
        Setcomboboxanimation = 0x1005,
        Getlistboxsmoothscrolling = 0x1006,
        Setlistboxsmoothscrolling = 0x1007,
        Getgradientcaptions = 0x1008,
        Setgradientcaptions = 0x1009,
        Getkeyboardcues = 0x100A,
        Setkeyboardcues = 0x100B,
        Getmenuunderlines = Getkeyboardcues,
        Setmenuunderlines = Setkeyboardcues,
        Getactivewndtrkzorder = 0x100C,
        Setactivewndtrkzorder = 0x100D,
        Gethottracking = 0x100E,
        Sethottracking = 0x100F,
        Getmenufade = 0x1012,
        Setmenufade = 0x1013,
        Getselectionfade = 0x1014,
        Setselectionfade = 0x1015,
        Gettooltipanimation = 0x1016,
        Settooltipanimation = 0x1017,
        Gettooltipfade = 0x1018,
        Settooltipfade = 0x1019,
        Getcursorshadow = 0x101A,
        Setcursorshadow = 0x101B,
        Getmousesonar = 0x101C,
        Setmousesonar = 0x101D,
        Getmouseclicklock = 0x101E,
        Setmouseclicklock = 0x101F,
        Getmousevanish = 0x1020,
        Setmousevanish = 0x1021,
        Getflatmenu = 0x1022,
        Setflatmenu = 0x1023,
        Getdropshadow = 0x1024,
        Setdropshadow = 0x1025,
        Getblocksendinputresets = 0x1026,
        Setblocksendinputresets = 0x1027,

        Getuieffects = 0x103E,
        Setuieffects = 0x103F,

        Getdisableoverlappedcontent = 0x1040,
        Setdisableoverlappedcontent = 0x1041,
        Getclientareaanimation = 0x1042,
        Setclientareaanimation = 0x1043,
        Getcleartype = 0x1048,
        Setcleartype = 0x1049,
        Getspeechrecognition = 0x104A,
        Setspeechrecognition = 0x104B,

        Getforegroundlocktimeout = 0x2000,
        Setforegroundlocktimeout = 0x2001,
        Getactivewndtrktimeout = 0x2002,
        Setactivewndtrktimeout = 0x2003,
        Getforegroundflashcount = 0x2004,
        Setforegroundflashcount = 0x2005,
        Getcaretwidth = 0x2006,
        Setcaretwidth = 0x2007,

        Getmouseclicklocktime = 0x2008,
        Setmouseclicklocktime = 0x2009,
        Getfontsmoothingtype = 0x200A,
        Setfontsmoothingtype = 0x200B,

        Getfontsmoothingcontrast = 0x200C,
        Setfontsmoothingcontrast = 0x200D,

        Getfocusborderwidth = 0x200E,
        Setfocusborderwidth = 0x200F,
        Getfocusborderheight = 0x2010,
        Setfocusborderheight = 0x2011,

        Getfontsmoothingorientation = 0x2012,
        Setfontsmoothingorientation = 0x2013,

        Getminimumhitradius = 0x2014,
        Setminimumhitradius = 0x2015,
        Getmessageduration = 0x2016,
        Setmessageduration = 0x2017,
    }

    [Flags]
    internal enum Spif
    {
        None = 0,
        Updateinifile = 0x01,
        Sendchange = 0x02,
        Sendwininichange = Sendchange,
    }

    [Flags]
    internal enum State_System
    {
        Unavailable = 0x00000001, // Disabled
        Selected = 0x00000002,
        Focused = 0x00000004,
        Pressed = 0x00000008,
        Checked = 0x00000010,
        Mixed = 0x00000020, // 3-state checkbox or toolbar button
        Indeterminate = Mixed,
        Readonly = 0x00000040,
        Hottracked = 0x00000080,
        Default = 0x00000100,
        Expanded = 0x00000200,
        Collapsed = 0x00000400,
        Busy = 0x00000800,
        Floating = 0x00001000, // Children "owned" not "contained" by parent
        Marqueed = 0x00002000,
        Animated = 0x00004000,
        Invisible = 0x00008000,
        Offscreen = 0x00010000,
        Sizeable = 0x00020000,
        Moveable = 0x00040000,
        Selfvoicing = 0x00080000,
        Focusable = 0x00100000,
        Selectable = 0x00200000,
        Linked = 0x00400000,
        Traversed = 0x00800000,
        Multiselectable = 0x01000000, // Supports multiple selection
        Extselectable = 0x02000000, // Supports extended selection
        AlertLow = 0x04000000, // This information is of low priority
        AlertMedium = 0x08000000, // This information is of medium priority
        AlertHigh = 0x10000000, // This information is of high priority
        Protected = 0x20000000, // access to this is restricted
        Valid = 0x3FFFFFFF,
    }

    internal enum StockObject : int
    {
        WhiteBrush = 0,
        LtgrayBrush = 1,
        GrayBrush = 2,
        DkgrayBrush = 3,
        BlackBrush = 4,
        NullBrush = 5,
        HollowBrush = NullBrush,
        WhitePen = 6,
        BlackPen = 7,
        NullPen = 8,
        SystemFont = 13,
        DefaultPalette = 15,
    }

    [Flags]
    internal enum Cs : uint
    {
        Vredraw = 0x0001,
        Hredraw = 0x0002,
        Dblclks = 0x0008,
        Owndc = 0x0020,
        Classdc = 0x0040,
        Parentdc = 0x0080,
        Noclose = 0x0200,
        Savebits = 0x0800,
        Bytealignclient = 0x1000,
        Bytealignwindow = 0x2000,
        Globalclass = 0x4000,
        Ime = 0x00010000,
        Dropshadow = 0x00020000
    }

    [Flags]
    internal enum Ws : uint
    {
        Overlapped = 0x00000000,
        Popup = 0x80000000,
        Child = 0x40000000,
        Minimize = 0x20000000,
        Visible = 0x10000000,
        Disabled = 0x08000000,
        Clipsiblings = 0x04000000,
        Clipchildren = 0x02000000,
        Maximize = 0x01000000,
        Border = 0x00800000,
        Dlgframe = 0x00400000,
        Vscroll = 0x00200000,
        Hscroll = 0x00100000,
        Sysmenu = 0x00080000,
        Thickframe = 0x00040000,
        Group = 0x00020000,
        Tabstop = 0x00010000,

        Minimizebox = 0x00020000,
        Maximizebox = 0x00010000,

        Caption = Border | Dlgframe,
        Tiled = Overlapped,
        Iconic = Minimize,
        Sizebox = Thickframe,
        Tiledwindow = Overlappedwindow,

        Overlappedwindow = Overlapped | Caption | Sysmenu | Thickframe | Minimizebox | Maximizebox,
        Popupwindow = Popup | Border | Sysmenu,
        Childwindow = Child,
    }

    internal enum Wm
    {
        Null = 0x0000,
        Create = 0x0001,
        Destroy = 0x0002,
        Move = 0x0003,
        Size = 0x0005,
        Activate = 0x0006,
        Setfocus = 0x0007,
        Killfocus = 0x0008,
        Enable = 0x000A,
        Setredraw = 0x000B,
        Settext = 0x000C,
        Gettext = 0x000D,
        Gettextlength = 0x000E,
        Paint = 0x000F,
        Close = 0x0010,
        Queryendsession = 0x0011,
        Quit = 0x0012,
        Queryopen = 0x0013,
        Erasebkgnd = 0x0014,
        Syscolorchange = 0x0015,
        Showwindow = 0x0018,
        Ctlcolor = 0x0019,
        Wininichange = 0x001A,
        Settingchange = 0x001A,
        Activateapp = 0x001C,
        Setcursor = 0x0020,
        Mouseactivate = 0x0021,
        Childactivate = 0x0022,
        Queuesync = 0x0023,
        Getminmaxinfo = 0x0024,

        Windowposchanging = 0x0046,
        Windowposchanged = 0x0047,

        Contextmenu = 0x007B,
        Stylechanging = 0x007C,
        Stylechanged = 0x007D,
        Displaychange = 0x007E,
        Geticon = 0x007F,
        Seticon = 0x0080,
        Nccreate = 0x0081,
        Ncdestroy = 0x0082,
        Nccalcsize = 0x0083,
        Nchittest = 0x0084,
        Ncpaint = 0x0085,
        Ncactivate = 0x0086,
        Getdlgcode = 0x0087,
        Syncpaint = 0x0088,
        Ncmousemove = 0x00A0,
        Nclbuttondown = 0x00A1,
        Nclbuttonup = 0x00A2,
        Nclbuttondblclk = 0x00A3,
        Ncrbuttondown = 0x00A4,
        Ncrbuttonup = 0x00A5,
        Ncrbuttondblclk = 0x00A6,
        Ncmbuttondown = 0x00A7,
        Ncmbuttonup = 0x00A8,
        Ncmbuttondblclk = 0x00A9,

        Syskeydown = 0x0104,
        Syskeyup = 0x0105,
        Syschar = 0x0106,
        Sysdeadchar = 0x0107,
        Command = 0x0111,
        Syscommand = 0x0112,

        Mousemove = 0x0200,
        Lbuttondown = 0x0201,
        Lbuttonup = 0x0202,
        Lbuttondblclk = 0x0203,
        Rbuttondown = 0x0204,
        Rbuttonup = 0x0205,
        Rbuttondblclk = 0x0206,
        Mbuttondown = 0x0207,
        Mbuttonup = 0x0208,
        Mbuttondblclk = 0x0209,
        Mousewheel = 0x020A,
        Xbuttondown = 0x020B,
        Xbuttonup = 0x020C,
        Xbuttondblclk = 0x020D,
        Mousehwheel = 0x020E,
        Parentnotify = 0x0210,

        Capturechanged = 0x0215,
        Powerbroadcast = 0x0218,
        Devicechange = 0x0219,

        Entersizemove = 0x0231,
        Exitsizemove = 0x0232,

        ImeSetcontext = 0x0281,
        ImeNotify = 0x0282,
        ImeControl = 0x0283,
        ImeCompositionfull = 0x0284,
        ImeSelect = 0x0285,
        ImeChar = 0x0286,
        ImeRequest = 0x0288,
        ImeKeydown = 0x0290,
        ImeKeyup = 0x0291,

        Ncmouseleave = 0x02A2,

        TabletDefbase = 0x02C0,
        //WM_TABLET_MAXOFFSET = 0x20,

        TabletAdded = TabletDefbase + 8,
        TabletDeleted = TabletDefbase + 9,
        TabletFlick = TabletDefbase + 11,
        TabletQuerysystemgesturestatus = TabletDefbase + 12,

        Cut = 0x0300,
        Copy = 0x0301,
        Paste = 0x0302,
        Clear = 0x0303,
        Undo = 0x0304,
        Renderformat = 0x0305,
        Renderallformats = 0x0306,
        Destroyclipboard = 0x0307,
        Drawclipboard = 0x0308,
        Paintclipboard = 0x0309,
        Vscrollclipboard = 0x030A,
        Sizeclipboard = 0x030B,
        Askcbformatname = 0x030C,
        Changecbchain = 0x030D,
        Hscrollclipboard = 0x030E,
        Querynewpalette = 0x030F,
        Paletteischanging = 0x0310,
        Palettechanged = 0x0311,
        Hotkey = 0x0312,
        Print = 0x0317,
        Printclient = 0x0318,
        Appcommand = 0x0319,
        Themechanged = 0x031A,

        Dwmcompositionchanged = 0x031E,
        Dwmncrenderingchanged = 0x031F,
        Dwmcolorizationcolorchanged = 0x0320,
        Dwmwindowmaximizedchange = 0x0321,

        Gettitlebarinfoex = 0x033F,

        #region Windows 7

        Dwmsendiconicthumbnail = 0x0323,
        Dwmsendiconiclivepreviewbitmap = 0x0326,

        #endregion

        User = 0x0400,

        // This is the hard-coded message value used by WinForms for Shell_NotifyIcon.
        // It's relatively safe to reuse.
        Traymousemessage = 0x800, //WM_USER + 1024
        App = 0x8000,
    }

    [Flags]
    internal enum Ws_Ex : uint
    {
        None = 0,
        Dlgmodalframe = 0x00000001,
        Noparentnotify = 0x00000004,
        Topmost = 0x00000008,
        Acceptfiles = 0x00000010,
        Transparent = 0x00000020,
        Mdichild = 0x00000040,
        Toolwindow = 0x00000080,
        Windowedge = 0x00000100,
        Clientedge = 0x00000200,
        Contexthelp = 0x00000400,
        Right = 0x00001000,
        Left = 0x00000000,
        Rtlreading = 0x00002000,
        Ltrreading = 0x00000000,
        Leftscrollbar = 0x00004000,
        Rightscrollbar = 0x00000000,
        Controlparent = 0x00010000,
        Staticedge = 0x00020000,
        Appwindow = 0x00040000,
        Layered = 0x00080000,
        Noinheritlayout = 0x00100000, // Disable inheritence of mirroring by children
        Layoutrtl = 0x00400000, // Right to left mirroring
        Composited = 0x02000000,
        Noactivate = 0x08000000,
        Overlappedwindow = Windowedge | Clientedge,
        Palettewindow = Windowedge | Toolwindow | Topmost,
    }

    internal enum DeviceCap
    {
        Bitspixel = 12,
        Planes = 14,
        Logpixelsx = 88,
        Logpixelsy = 90,
    }

    internal enum Fo : int
    {
        Move = 0x0001,
        Copy = 0x0002,
        Delete = 0x0003,
        Rename = 0x0004,
    }

    internal enum Fof : ushort
    {
        Multidestfiles = 0x0001,
        Confirmmouse = 0x0002,
        Silent = 0x0004,
        Renameoncollision = 0x0008,
        Noconfirmation = 0x0010,
        Wantmappinghandle = 0x0020,
        Allowundo = 0x0040,
        Filesonly = 0x0080,
        Simpleprogress = 0x0100,
        Noconfirmmkdir = 0x0200,
        Noerrorui = 0x0400,
        Nocopysecurityattribs = 0x0800,
        Norecursion = 0x1000,
        NoConnectedElements = 0x2000,
        Wantnukewarning = 0x4000,
        Norecursereparse = 0x8000,
    }

    [Flags]
    internal enum Mf : uint
    {
        DoesNotExist = unchecked((uint) -1),
        Enabled = 0,
        Bycommand = 0,
        Grayed = 1,
        Disabled = 2,
    }

    internal enum Windowthemeattributetype : uint
    {
        WtaNonclient = 1,
    }

    internal enum Dwmflip3D
    {
        Default,
        Excludebelow,
        Excludeabove,
        //LAST
    }

    internal enum Dwmncrp
    {
        Usewindowstyle,
        Disabled,
        Enabled,
        //LAST
    }

    internal enum Dwmwa
    {
        NcrenderingEnabled = 1,
        NcrenderingPolicy,
        TransitionsForcedisabled,
        AllowNcpaint,
        CaptionButtonBounds,
        NonclientRtlLayout,
        ForceIconicRepresentation,
        Flip3DPolicy,
        ExtendedFrameBounds,

        // New to Windows 7:

        HasIconicBitmap,
        DisallowPeek,
        ExcludedFromPeek,

        // LAST
    }

    [Flags]
    internal enum Wtnca : uint
    {
        Nodrawcaption = 0x00000001,
        Nodrawicon = 0x00000002,
        Nosysmenu = 0x00000004,
        Nomirrorhelp = 0x00000008,
        Validbits = Nodrawcaption | Nodrawicon | Nomirrorhelp | Nosysmenu,
    }

    [Flags]
    internal enum Swp
    {
        Asyncwindowpos = 0x4000,
        Defererase = 0x2000,
        Drawframe = 0x0020,
        Framechanged = 0x0020,
        Hidewindow = 0x0080,
        Noactivate = 0x0010,
        Nocopybits = 0x0100,
        Nomove = 0x0002,
        Noownerzorder = 0x0200,
        Noredraw = 0x0008,
        Noreposition = 0x0200,
        Nosendchanging = 0x0400,
        Nosize = 0x0001,
        Nozorder = 0x0004,
        Showwindow = 0x0040,
    }

    internal enum Sw
    {
        Hide = 0,
        Shownormal = 1,
        Normal = 1,
        Showminimized = 2,
        Showmaximized = 3,
        Maximize = 3,
        Shownoactivate = 4,
        Show = 5,
        Minimize = 6,
        Showminnoactive = 7,
        Showna = 8,
        Restore = 9,
        Showdefault = 10,
        Forceminimize = 11,
    }

    internal enum Sc
    {
        Size = 0xF000,
        Move = 0xF010,
        Minimize = 0xF020,
        Maximize = 0xF030,
        Nextwindow = 0xF040,
        Prevwindow = 0xF050,
        Close = 0xF060,
        Vscroll = 0xF070,
        Hscroll = 0xF080,
        Mousemenu = 0xF090,
        Keymenu = 0xF100,
        Arrange = 0xF110,
        Restore = 0xF120,
        Tasklist = 0xF130,
        Screensave = 0xF140,
        Hotkey = 0xF150,
        Default = 0xF160,
        Monitorpower = 0xF170,
        Contexthelp = 0xF180,
        Separator = 0xF00F,
        FIssecure = 0x00000001,
        Icon = Minimize,
        Zoom = Maximize,
    }

    internal enum Status
    {
        Ok = 0,
        GenericError = 1,
        InvalidParameter = 2,
        OutOfMemory = 3,
        ObjectBusy = 4,
        InsufficientBuffer = 5,
        NotImplemented = 6,
        Win32Error = 7,
        WrongState = 8,
        Aborted = 9,
        FileNotFound = 10,
        ValueOverflow = 11,
        AccessDenied = 12,
        UnknownImageFormat = 13,
        FontFamilyNotFound = 14,
        FontStyleNotFound = 15,
        NotTrueTypeFont = 16,
        UnsupportedGdiplusVersion = 17,
        GdiplusNotInitialized = 18,
        PropertyNotFound = 19,
        PropertyNotSupported = 20,
        ProfileNotFound = 21,
    }

    internal enum Mouseeventf : int
    {
        //mouse event constants
        Leftdown = 2,
        Leftup = 4
    }

    internal enum Msgflt
    {
        // Win7 versions of this enum:
        Reset = 0,
        Allow = 1,
        Disallow = 2,

        // Vista versions of this enum:
        // ADD = 1,
        // REMOVE = 2,
    }

    internal enum Msgfltinfo
    {
        None = 0,
        AlreadyallowedForwnd = 1,
        AlreadydisallowedForwnd = 2,
        AllowedHigher = 3,
    }

    internal enum Input_Type : uint
    {
        Mouse = 0,
    }

    internal enum Nim : uint
    {
        Add = 0,
        Modify = 1,
        Delete = 2,
        Setfocus = 3,
        Setversion = 4,
    }

    internal enum Shard
    {
        Pidl = 0x00000001,
        Patha = 0x00000002,
        Pathw = 0x00000003,
        Appidinfo = 0x00000004, // indicates the data type is a pointer to a SHARDAPPIDINFO structure
        Appidinfoidlist = 0x00000005, // indicates the data type is a pointer to a SHARDAPPIDINFOIDLIST structure
        Link = 0x00000006, // indicates the data type is a pointer to an IShellLink instance
        Appidinfolink = 0x00000007, // indicates the data type is a pointer to a SHARDAPPIDINFOLINK structure 
    }

    [Flags]
    internal enum Slgp
    {
        Shortpath = 0x1,
        Uncpriority = 0x2,
        Rawpath = 0x4
    }

    [Flags]
    internal enum Nif : uint
    {
        Message = 0x0001,
        Icon = 0x0002,
        Tip = 0x0004,
        State = 0x0008,
        Info = 0x0010,
        Guid = 0x0020,
        Realtime = 0x0040,
        Showtip = 0x0080,

        XpMask = Message | Icon | State | Info | Guid,
        VistaMask = XpMask | Realtime | Showtip,
    }

    internal enum Niif
    {
        None = 0x00000000,
        Info = 0x00000001,
        Warning = 0x00000002,
        Error = 0x00000003,
        User = 0x00000004,
        Nosound = 0x00000010,
        LargeIcon = 0x00000020,
        NiifRespectQuietTime = 0x00000080,
        XpIconMask = 0x0000000F,
    }

    internal enum Ac : byte
    {
        SrcOver = 0,
        SrcAlpha = 1,
    }

    internal enum Ulw
    {
        Alpha = 2,
        Colorkey = 1,
        Opaque = 4,
    }

    internal enum Wvr
    {
        Aligntop = 0x0010,
        Alignleft = 0x0020,
        Alignbottom = 0x0040,
        Alignright = 0x0080,
        Hredraw = 0x0100,
        Vredraw = 0x0200,
        Validrects = 0x0400,
        Redraw = Hredraw | Vredraw,
    }

    #endregion

    #region SafeHandles

    internal sealed class SafeFindHandle : SafeHandleZeroOrMinusOneIsInvalid
    {
        #region Construct

        [SecurityPermission(SecurityAction.LinkDemand, UnmanagedCode = true)]
        private SafeFindHandle() : base(true)
        { }

        #endregion

        #region Protected Методы

        protected override bool ReleaseHandle()
        {
            return NativeMethods.FindClose(handle);
        }

        #endregion
    }

    internal sealed class SafeDc : SafeHandleZeroOrMinusOneIsInvalid
    {
        #region Private fields

        private bool _created;

        private IntPtr? _hwnd;

        #endregion

        #region Construct

        private SafeDc() : base(true)
        { }

        #endregion

        #region Public

        [SuppressMessage("Microsoft.Usage", "CA2201:DoNotRaiseReservedExceptionTypes")]
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static SafeDc CreateDc(string deviceName)
        {
            SafeDc dc = null;
            try
            {
                // Should this really be on the driver parameter?
                dc = NativeMethods.CreateDC(deviceName, null, IntPtr.Zero, IntPtr.Zero);
            }
            finally
            {
                if (dc != null)
                {
                    dc._created = true;
                }
            }

            if (dc.IsInvalid)
            {
                dc.Dispose();
                throw new SystemException("Unable to create a device context from the specified device information.");
            }

            return dc;
        }

        [SuppressMessage("Microsoft.Usage", "CA2201:DoNotRaiseReservedExceptionTypes")]
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static SafeDc CreateCompatibleDc(SafeDc hdc)
        {
            SafeDc dc = null;
            try
            {
                IntPtr hPtr = IntPtr.Zero;
                if (hdc != null)
                {
                    hPtr = hdc.handle;
                }

                dc = NativeMethods.CreateCompatibleDC(hPtr);
                if (dc == null)
                {
                    Hresult.ThrowLastError();
                }
            }
            finally
            {
                if (dc != null)
                {
                    dc._created = true;
                }
            }

            if (dc.IsInvalid)
            {
                dc.Dispose();
                throw new SystemException("Unable to create a device context from the specified device information.");
            }

            return dc;
        }

        public static SafeDc GetDc(IntPtr hwnd)
        {
            SafeDc dc = null;
            try
            {
                dc = NativeMethods.GetDC(hwnd);
            }
            finally
            {
                if (dc != null)
                {
                    dc.Hwnd = hwnd;
                }
            }

            if (dc.IsInvalid)
            {
                // GetDC does not set the last error...
                Hresult.E_FAIL.ThrowIfFailed();
            }

            return dc;
        }

        public static SafeDc GetDesktop()
        {
            return GetDc(IntPtr.Zero);
        }

        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static SafeDc WrapDc(IntPtr hdc)
        {
            // This won't actually get released by the class, but it allows an IntPtr to be converted for signatures.
            return new SafeDc
            {
                handle = hdc,
                _created = false,
                _hwnd = IntPtr.Zero,
            };
        }

        public IntPtr Hwnd
        {
            set
            {
                Assert.NullableIsNull(_hwnd);
                _hwnd = value;
            }
        }

        #endregion

        #region Protected Методы

        [ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
        protected override bool ReleaseHandle()
        {
            if (_created)
            {
                return NativeMethods.DeleteDC(handle);
            }

            if (!_hwnd.HasValue || _hwnd.Value == IntPtr.Zero)
            {
                return true;
            }

            return NativeMethods.ReleaseDC(_hwnd.Value, handle) == 1;
        }

        #endregion

        #region  Class

        private static class NativeMethods
        {
            #region Public

            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
            [DllImport("user32.dll")]
            public static extern int ReleaseDC(IntPtr hWnd, IntPtr hDc);

            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
            [DllImport("user32.dll")]
            public static extern SafeDc GetDC(IntPtr hwnd);

            // Weird legacy function, documentation is unclear about how to use it...
            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
            [DllImport("gdi32.dll", CharSet = CharSet.Unicode)]
            public static extern SafeDc CreateDC([MarshalAs(UnmanagedType.LPWStr)] string lpszDriver,
                [MarshalAs(UnmanagedType.LPWStr)] string lpszDevice, IntPtr lpszOutput, IntPtr lpInitData);

            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
            [DllImport("gdi32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
            public static extern SafeDc CreateCompatibleDC(IntPtr hdc);

            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
            [DllImport("gdi32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            public static extern bool DeleteDC(IntPtr hdc);

            #endregion
        }

        #endregion
    }

    internal sealed class SafeHbitmap : SafeHandleZeroOrMinusOneIsInvalid
    {
        #region Construct

        private SafeHbitmap() : base(true)
        { }

        #endregion

        #region Protected Методы

        [ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
        protected override bool ReleaseHandle()
        {
            return NativeMethods.DeleteObject(handle);
        }

        #endregion
    }

    internal sealed class SafeGdiplusStartupToken : SafeHandleZeroOrMinusOneIsInvalid
    {
        #region Construct

        private SafeGdiplusStartupToken() : base(true)
        { }

        #endregion

        #region Public

        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [SuppressMessage("Microsoft.Usage", "CA2201:DoNotRaiseReservedExceptionTypes")]
        public static SafeGdiplusStartupToken Startup()
        {
            SafeGdiplusStartupToken safeHandle = new SafeGdiplusStartupToken();
            IntPtr unsafeHandle;
            StartupOutput output;
            Status s = NativeMethods.GdiplusStartup(out unsafeHandle, new StartupInput(), out output);
            if (s == Status.Ok)
            {
                safeHandle.handle = unsafeHandle;
                return safeHandle;
            }

            safeHandle.Dispose();
            throw new Exception("Unable to initialize GDI+");
        }

        #endregion

        #region Protected Методы

        [ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
        protected override bool ReleaseHandle()
        {
            Status s = NativeMethods.GdiplusShutdown(this.handle);
            return s == Status.Ok;
        }

        #endregion
    }

    internal sealed class SafeConnectionPointCookie : SafeHandleZeroOrMinusOneIsInvalid
    {
        #region Private fields

        private IConnectionPoint _cp;

        #endregion

        #region Construct

        // handle holds the cookie value.

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "IConnectionPoint")]
        public SafeConnectionPointCookie(IConnectionPointContainer target, object sink, Guid eventId)
            : base(true)
        {
            Verify.IsNotNull(target, "target");
            Verify.IsNotNull(sink, "sink");
            Verify.IsNotDefault(eventId, "eventId");

            handle = IntPtr.Zero;

            IConnectionPoint cp = null;
            try
            {
                int dwCookie;
                target.FindConnectionPoint(ref eventId, out cp);
                cp.Advise(sink, out dwCookie);
                if (dwCookie == 0)
                {
                    throw new InvalidOperationException("IConnectionPoint::Advise returned an invalid cookie.");
                }

                handle = new IntPtr(dwCookie);
                _cp = cp;
                cp = null;
            }
            finally
            {
                Utility.SafeRelease(ref cp);
            }
        }

        #endregion

        #region Public

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public void Disconnect()
        {
            ReleaseHandle();
        }

        #endregion

        #region Protected Методы

        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        [ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
        protected override bool ReleaseHandle()
        {
            try
            {
                if (!this.IsInvalid)
                {
                    int dwCookie = handle.ToInt32();
                    handle = IntPtr.Zero;

                    Assert.IsNotNull(_cp);
                    try
                    {
                        _cp.Unadvise(dwCookie);
                    }
                    finally
                    {
                        Utility.SafeRelease(ref _cp);
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion
    }

    #endregion

    #region Native Types

    [StructLayout(LayoutKind.Sequential)]
    internal struct Blendfunction
    {
        // Must be AC_SRC_OVER
        public Ac BlendOp;

        // Must be 0.
        public byte BlendFlags;

        // Alpha transparency between 0 (transparent) - 255 (opaque)
        public byte SourceConstantAlpha;

        // Must be AC_SRC_ALPHA
        public Ac AlphaFormat;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct Highcontrast
    {
        public int cbSize;

        public Hcf dwFlags;

        //[MarshalAs(UnmanagedType.LPWStr, SizeConst=80)]
        //public String lpszDefaultScheme;
        public IntPtr lpszDefaultScheme;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct Rgbquad
    {
        public byte rgbBlue;
        public byte rgbGreen;
        public byte rgbRed;
        public byte rgbReserved;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 2)]
    internal struct Bitmapinfoheader
    {
        public int biSize;
        public int biWidth;
        public int biHeight;
        public short biPlanes;
        public short biBitCount;
        public Bi biCompression;
        public int biSizeImage;
        public int biXPelsPerMeter;
        public int biYPelsPerMeter;
        public int biClrUsed;
        public int biClrImportant;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct Bitmapinfo
    {
        public Bitmapinfoheader bmiHeader;
        public Rgbquad bmiColors;
    }

    // Win7 only.
    [StructLayout(LayoutKind.Sequential)]
    internal struct Changefilterstruct
    {
        public uint cbSize;
        public Msgfltinfo ExtStatus;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    internal struct Createstruct
    {
        public IntPtr lpCreateParams;
        public IntPtr hInstance;
        public IntPtr hMenu;
        public IntPtr hwndParent;
        public int cy;
        public int cx;
        public int y;
        public int x;
        public Ws style;
        [MarshalAs(UnmanagedType.LPWStr)] public string lpszName;
        [MarshalAs(UnmanagedType.LPWStr)] public string lpszClass;
        public Ws_Ex dwExStyle;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode, Pack = 1)]
    internal struct Shfileopstruct
    {
        public IntPtr hwnd;

        [MarshalAs(UnmanagedType.U4)] public Fo wFunc;

        // double-null terminated arrays of LPWSTRS
        public string pFrom;
        public string pTo;
        [MarshalAs(UnmanagedType.U2)] public Fof fFlags;
        [MarshalAs(UnmanagedType.Bool)] public int fAnyOperationsAborted;
        public IntPtr hNameMappings;
        public string lpszProgressTitle;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct Titlebarinfo
    {
        public int cbSize;
        public Rect rcTitleBar;
        public State_System rgstate_TitleBar;
        public State_System rgstate_Reserved;
        public State_System rgstate_MinimizeButton;
        public State_System rgstate_MaximizeButton;
        public State_System rgstate_HelpButton;
        public State_System rgstate_CloseButton;
    }

    // New to Vista.
    [StructLayout(LayoutKind.Sequential)]
    internal struct Titlebarinfoex
    {
        public int cbSize;
        public Rect rcTitleBar;
        public State_System rgstate_TitleBar;
        public State_System rgstate_Reserved;
        public State_System rgstate_MinimizeButton;
        public State_System rgstate_MaximizeButton;
        public State_System rgstate_HelpButton;
        public State_System rgstate_CloseButton;
        public Rect rgrect_TitleBar;
        public Rect rgrect_Reserved;
        public Rect rgrect_MinimizeButton;
        public Rect rgrect_MaximizeButton;
        public Rect rgrect_HelpButton;
        public Rect rgrect_CloseButton;
    }

    [SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses")]
    [StructLayout(LayoutKind.Sequential)]
    internal class Notifyicondata
    {
        #region Private fields

        // Vista only
        private IntPtr hBalloonIcon;

        #endregion

        #region Public

        public int cbSize;
        public IntPtr hWnd;
        public int uID;
        public Nif uFlags;
        public int uCallbackMessage;
        public IntPtr hIcon;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
        public char[] szTip = new char[128];

        public uint dwState;
        public uint dwStateMask;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 256)]
        public char[] szInfo = new char[256];

        // Prior to Vista this was a union of uTimeout and uVersion.  As of Vista, uTimeout has been deprecated.
        public uint uVersion; // Used with Shell_NotifyIcon flag NIM_SETVERSION.

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public char[] szInfoTitle = new char[64];

        public uint dwInfoFlags;
        public Guid guidItem;

        #endregion
    }

    [SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses")]
    [StructLayout(LayoutKind.Explicit)]
    internal class Propvariant : IDisposable
    {
        #region Private fields

        [SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")] [FieldOffset(8)]
        private short boolVal;

        [SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")] [FieldOffset(8)]
        private byte byteVal;

        [SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")] [FieldOffset(8)]
        private long longVal;

        [SuppressMessage("Microsoft.Reliability", "CA2006:UseSafeHandleToEncapsulateNativeResources")]
        [SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        [FieldOffset(8)]
        private IntPtr pointerVal;

        [SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")] [FieldOffset(0)]
        private ushort vt;

        #endregion

        #region Public

        // Right now only using this for strings.
        [SuppressMessage("Microsoft.Security", "CA2122:DoNotIndirectlyExposeMethodsWithLinkDemands")]
        public string GetValue()
        {
            if (vt == (ushort) VarEnum.VT_LPWSTR)
            {
                return Marshal.PtrToStringUni(pointerVal);
            }

            return null;
        }

        public void SetValue(bool f)
        {
            Clear();
            vt = (ushort) VarEnum.VT_BOOL;
            boolVal = (short) (f ? -1 : 0);
        }

        [SuppressMessage("Microsoft.Security", "CA2122:DoNotIndirectlyExposeMethodsWithLinkDemands")]
        public void SetValue(string val)
        {
            Clear();
            vt = (ushort) VarEnum.VT_LPWSTR;
            pointerVal = Marshal.StringToCoTaskMemUni(val);
        }

        public void Clear()
        {
            Hresult hr = NativeMethods.PropVariantClear(this);
            Assert.IsTrue(hr.Succeeded);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public VarEnum VarType
        {
            get { return (VarEnum) vt; }
        }

        #endregion

        #region  Class

        private static class NativeMethods
        {
            #region Internals

            [DllImport("ole32.dll")]
            internal static extern Hresult PropVariantClear(Propvariant pvar);

            #endregion
        }

        #endregion

        #region IDisposable Pattern

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~Propvariant()
        {
            Dispose(false);
        }

        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "disposing")]
        private void Dispose(bool disposing)
        {
            Clear();
        }

        #endregion
    }

    [SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses")]
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    internal class Shardappidinfo
    {
        #region Private fields

        [MarshalAs(UnmanagedType.Interface)]
        private object psi; // The namespace location of the the item that should be added to the recent docs folder.

        [MarshalAs(UnmanagedType.LPWStr)]
        private string pszAppID; // The id of the application that should be associated with this recent doc.

        #endregion
    }

    [SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses")]
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    internal class Shardappidinfoidlist
    {
        #region Private fields

        private IntPtr pidl;

        [MarshalAs(UnmanagedType.LPWStr)] private string pszAppID;

        #endregion
    }

    [SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses")]
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    internal class Shardappidinfolink
    {
        #region Private fields

        private IntPtr psl; // An IShellLink instance that when launched opens a recently used item in the specified 

        // application. This link is not added to the recent docs folder, but will be added to the
        // specified application's destination list.
        [MarshalAs(UnmanagedType.LPWStr)]
        private string pszAppID; // The id of the application that should be associated with this recent doc.

        #endregion
    }


    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    internal struct Logfont
    {
        public int lfHeight;
        public int lfWidth;
        public int lfEscapement;
        public int lfOrientation;
        public int lfWeight;
        public byte lfItalic;
        public byte lfUnderline;
        public byte lfStrikeOut;
        public byte lfCharSet;
        public byte lfOutPrecision;
        public byte lfClipPrecision;
        public byte lfQuality;
        public byte lfPitchAndFamily;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string lfFaceName;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct Minmaxinfo
    {
        public Point ptReserved;
        public Point ptMaxSize;
        public Point ptMaxPosition;
        public Point ptMinTrackSize;
        public Point ptMaxTrackSize;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct Nonclientmetrics
    {
        public int cbSize;
        public int iBorderWidth;
        public int iScrollWidth;
        public int iScrollHeight;
        public int iCaptionWidth;
        public int iCaptionHeight;
        public Logfont lfCaptionFont;
        public int iSmCaptionWidth;
        public int iSmCaptionHeight;
        public Logfont lfSmCaptionFont;
        public int iMenuWidth;
        public int iMenuHeight;
        public Logfont lfMenuFont;
        public Logfont lfStatusFont;

        public Logfont lfMessageFont;

        // Vista only
        public int iPaddedBorderWidth;

        [SuppressMessage("Microsoft.Security", "CA2122:DoNotIndirectlyExposeMethodsWithLinkDemands")]
        public static Nonclientmetrics VistaMetricsStruct
        {
            get
            {
                var ncm = new Nonclientmetrics();
                ncm.cbSize = Marshal.SizeOf(typeof(Nonclientmetrics));
                return ncm;
            }
        }

        [SuppressMessage("Microsoft.Security", "CA2122:DoNotIndirectlyExposeMethodsWithLinkDemands")]
        public static Nonclientmetrics XpMetricsStruct
        {
            get
            {
                var ncm = new Nonclientmetrics();
                // Account for the missing iPaddedBorderWidth
                ncm.cbSize = Marshal.SizeOf(typeof(Nonclientmetrics)) - sizeof(int);
                return ncm;
            }
        }
    }

    [StructLayout(LayoutKind.Explicit)]
    internal struct Wta_Options
    {
        // public static readonly uint Size = (uint)Marshal.SizeOf(typeof(WTA_OPTIONS));
        public const uint SIZE = 8;

        [SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Justification = "Used by native code.")]
        [FieldOffset(0)]
        public Wtnca dwFlags;

        [SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Justification = "Used by native code.")]
        [FieldOffset(4)]
        public Wtnca dwMask;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct Margins
    {
        public int cxLeftWidth;
        public int cxRightWidth;
        public int cyTopHeight;
        public int cyBottomHeight;
    };

    [StructLayout(LayoutKind.Sequential)]
    internal class Monitorinfo
    {
        #region Public

        public int cbSize = Marshal.SizeOf(typeof(Monitorinfo));
        public Rect rcMonitor;
        public Rect rcWork;
        public int dwFlags;

        #endregion
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct Point
    {
        public int x;
        public int y;
    }

    [SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses")]
    [StructLayout(LayoutKind.Sequential)]
    internal class RefPoint
    {
        #region Public

        public int x;
        public int y;

        #endregion
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct Rect
    {
        private int _left;
        private int _top;
        private int _right;
        private int _bottom;

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public void Offset(int dx, int dy)
        {
            _left += dx;
            _top += dy;
            _right += dx;
            _bottom += dy;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public int Left
        {
            get { return _left; }
            set { _left = value; }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public int Right
        {
            get { return _right; }
            set { _right = value; }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public int Top
        {
            get { return _top; }
            set { _top = value; }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public int Bottom
        {
            get { return _bottom; }
            set { _bottom = value; }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public int Width
        {
            get { return _right - _left; }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public int Height
        {
            get { return _bottom - _top; }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public Point Position
        {
            get { return new Point {x = _left, y = _top}; }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public Size Size
        {
            get { return new Size {cx = Width, cy = Height}; }
        }

        public static Rect Union(Rect rect1, Rect rect2)
        {
            return new Rect
            {
                Left = Math.Min(rect1.Left, rect2.Left),
                Top = Math.Min(rect1.Top, rect2.Top),
                Right = Math.Max(rect1.Right, rect2.Right),
                Bottom = Math.Max(rect1.Bottom, rect2.Bottom),
            };
        }

        public override bool Equals(object obj)
        {
            try
            {
                var rc = (Rect) obj;
                return rc._bottom == _bottom
                       && rc._left == _left
                       && rc._right == _right
                       && rc._top == _top;
            }
            catch (InvalidCastException)
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return ((_left << 16) | Utility.Loword(_right)) ^ ((_top << 16) | Utility.Loword(_bottom));
        }
    }

    [SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses")]
    [StructLayout(LayoutKind.Sequential)]
    internal class RefRect
    {
        #region Private fields

        private int _bottom;
        private int _left;
        private int _right;
        private int _top;

        #endregion

        #region Construct

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public RefRect(int left, int top, int right, int bottom)
        {
            _left = left;
            _top = top;
            _right = right;
            _bottom = bottom;
        }

        #endregion

        #region Public

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public void Offset(int dx, int dy)
        {
            _left += dx;
            _top += dy;
            _right += dx;
            _bottom += dy;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public int Width
        {
            get { return _right - _left; }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public int Height
        {
            get { return _bottom - _top; }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public int Left
        {
            get { return _left; }
            set { _left = value; }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public int Right
        {
            get { return _right; }
            set { _right = value; }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public int Top
        {
            get { return _top; }
            set { _top = value; }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public int Bottom
        {
            get { return _bottom; }
            set { _bottom = value; }
        }

        #endregion
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct Size
    {
        public int cx;
        public int cy;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct StartupOutput
    {
        public IntPtr hook;
        public IntPtr unhook;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal class StartupInput
    {
        #region Public

        public int GdiplusVersion = 1;
        public IntPtr DebugEventCallback;
        public bool SuppressBackgroundThread;
        public bool SuppressExternalCodecs;

        #endregion
    }

    [SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses")]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    [BestFitMapping(false)]
    internal class Win32_Find_Dataw
    {
        #region Public

        public FileAttributes dwFileAttributes;
        public System.Runtime.InteropServices.ComTypes.FILETIME ftCreationTime;
        public System.Runtime.InteropServices.ComTypes.FILETIME ftLastAccessTime;
        public System.Runtime.InteropServices.ComTypes.FILETIME ftLastWriteTime;
        public int nFileSizeHigh;
        public int nFileSizeLow;
        public int dwReserved0;
        public int dwReserved1;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
        public string cFileName;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 14)]
        public string cAlternateFileName;

        #endregion
    }

    [StructLayout(LayoutKind.Sequential)]
    internal class Windowplacement
    {
        #region Public

        public int length = Marshal.SizeOf(typeof(Windowplacement));
        public int flags;
        public Sw showCmd;
        public Point ptMinPosition;
        public Point ptMaxPosition;
        public Rect rcNormalPosition;

        #endregion
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct Windowpos
    {
        public IntPtr hwnd;
        public IntPtr hwndInsertAfter;
        public int x;
        public int y;
        public int cx;
        public int cy;
        public int flags;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    internal struct Wndclassex
    {
        public int cbSize;
        public Cs style;
        public WndProc lpfnWndProc;
        public int cbClsExtra;
        public int cbWndExtra;
        public IntPtr hInstance;
        public IntPtr hIcon;
        public IntPtr hCursor;
        public IntPtr hbrBackground;
        [MarshalAs(UnmanagedType.LPWStr)] public string lpszMenuName;
        [MarshalAs(UnmanagedType.LPWStr)] public string lpszClassName;
        public IntPtr hIconSm;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct Mouseinput
    {
        public int dx;
        public int dy;
        public int mouseData;
        public int dwFlags;
        public int time;
        public IntPtr dwExtraInfo;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct Input
    {
        public uint type;
        public Mouseinput mi;
    };

    [StructLayout(LayoutKind.Sequential)]
    internal struct Unsigned_Ratio
    {
        public uint uiNumerator;
        public uint uiDenominator;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    internal struct Dwm_Timing_Info
    {
        public int cbSize;
        public Unsigned_Ratio rateRefresh;
        public ulong qpcRefreshPeriod;
        public Unsigned_Ratio rateCompose;
        public ulong qpcVBlank;
        public ulong cRefresh;
        public uint cDXRefresh;
        public ulong qpcCompose;
        public ulong cFrame;
        public uint cDXPresent;
        public ulong cRefreshFrame;
        public ulong cFrameSubmitted;
        public uint cDXPresentSubmitted;
        public ulong cFrameConfirmed;
        public uint cDXPresentConfirmed;
        public ulong cRefreshConfirmed;
        public uint cDXRefreshConfirmed;
        public ulong cFramesLate;
        public uint cFramesOutstanding;
        public ulong cFrameDisplayed;
        public ulong qpcFrameDisplayed;
        public ulong cRefreshFrameDisplayed;
        public ulong cFrameComplete;
        public ulong qpcFrameComplete;
        public ulong cFramePending;
        public ulong qpcFramePending;
        public ulong cFramesDisplayed;
        public ulong cFramesComplete;
        public ulong cFramesPending;
        public ulong cFramesAvailable;
        public ulong cFramesDropped;
        public ulong cFramesMissed;
        public ulong cRefreshNextDisplayed;
        public ulong cRefreshNextPresented;
        public ulong cRefreshesDisplayed;
        public ulong cRefreshesPresented;
        public ulong cRefreshStarted;
        public ulong cPixelsReceived;
        public ulong cPixelsDrawn;
        public ulong cBuffersEmpty;
    }

    #endregion

    internal delegate IntPtr WndProc(IntPtr hwnd, Wm uMsg, IntPtr wParam, IntPtr lParam);

    internal delegate IntPtr WndProcHook(IntPtr hwnd, Wm uMsg, IntPtr wParam, IntPtr lParam, ref bool handled);

    internal delegate IntPtr MessageHandler(Wm uMsg, IntPtr wParam, IntPtr lParam, out bool handled);

    internal static class NativeMethods
    {
        #region Public

        public static Rect AdjustWindowRectEx(Rect lpRect, Ws dwStyle, bool bMenu, Ws_Ex dwExStyle)
        {
            // Native version modifies the parameter in place.
            if (!_AdjustWindowRectEx(ref lpRect, dwStyle, bMenu, dwExStyle))
            {
                Hresult.ThrowLastError();
            }

            return lpRect;
        }

        // Note that processes at or below SECURITY_MANDATORY_LOW_RID are not allowed to change the message filter.
        // If those processes call this function, it will fail and generate the extended error code, ERROR_ACCESS_DENIED.
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static Hresult ChangeWindowMessageFilterEx(IntPtr hwnd, Wm message, Msgflt action, out Msgfltinfo filterInfo)
        {
            filterInfo = Msgfltinfo.None;

            bool ret;

            // This origins of this API were added for Vista.  The Ex version was added for Windows 7.
            // If we're not on either, then this message filter isolation doesn't exist.
            if (!Utility.IsOsVistaOrNewer)
            {
                return Hresult.S_FALSE;
            }

            // If we're on Vista rather than Win7 then we can't use the Ex version of this function.
            // The Ex version is preferred if possible because this results in process-wide modifications of the filter
            // and is deprecated as of Win7.
            if (!Utility.IsOsWindows7OrNewer)
            {
                // Note that the Win7 MSGFLT_ALLOW/DISALLOW enum values map to the Vista MSGFLT_ADD/REMOVE
                ret = _ChangeWindowMessageFilter(message, action);
                if (!ret)
                {
                    return (Hresult) Win32Error.GetLastError();
                }

                return Hresult.S_OK;
            }

            var filterstruct = new Changefilterstruct {cbSize = (uint) Marshal.SizeOf(typeof(Changefilterstruct))};
            ret = _ChangeWindowMessageFilterEx(hwnd, message, action, ref filterstruct);
            if (!ret)
            {
                return (Hresult) Win32Error.GetLastError();
            }

            filterInfo = filterstruct.ExtStatus;
            return Hresult.S_OK;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("gdi32.dll")]
        public static extern CombineRgnResult CombineRgn(IntPtr hrgnDest, IntPtr hrgnSrc1, IntPtr hrgnSrc2, Rgn fnCombineMode);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static string[] CommandLineToArgvW(string cmdLine)
        {
            IntPtr argv = IntPtr.Zero;
            try
            {
                int numArgs = 0;

                argv = _CommandLineToArgvW(cmdLine, out numArgs);
                if (argv == IntPtr.Zero)
                {
                    throw new Win32Exception();
                }

                var result = new string[numArgs];

                for (int i = 0; i < numArgs; i++)
                {
                    IntPtr currArg = Marshal.ReadIntPtr(argv, i * Marshal.SizeOf(typeof(IntPtr)));
                    result[i] = Marshal.PtrToStringUni(currArg);
                }

                return result;
            }
            finally
            {
                IntPtr p = _LocalFree(argv);
                // Otherwise LocalFree failed.
                Assert.AreEqual(IntPtr.Zero, p);
            }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static SafeHbitmap CreateDibSection(SafeDc hdc, ref Bitmapinfo bitmapInfo, out IntPtr ppvBits, IntPtr hSection, int dwOffset)
        {
            const int dibRgbColors = 0;
            SafeHbitmap hBitmap = null;
            if (hdc == null)
            {
                hBitmap = _CreateDIBSectionIntPtr(IntPtr.Zero, ref bitmapInfo, dibRgbColors, out ppvBits, hSection, dwOffset);
            }
            else
            {
                hBitmap = _CreateDIBSection(hdc, ref bitmapInfo, dibRgbColors, out ppvBits, hSection, dwOffset);
            }

            if (hBitmap.IsInvalid)
            {
                Hresult.ThrowLastError();
            }

            return hBitmap;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static IntPtr CreateRoundRectRgn(int nLeftRect, int nTopRect, int nRightRect, int nBottomRect, int nWidthEllipse,
            int nHeightEllipse)
        {
            IntPtr ret = _CreateRoundRectRgn(nLeftRect, nTopRect, nRightRect, nBottomRect, nWidthEllipse, nHeightEllipse);
            if (IntPtr.Zero == ret)
            {
                throw new Win32Exception();
            }

            return ret;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static IntPtr CreateRectRgn(int nLeftRect, int nTopRect, int nRightRect, int nBottomRect)
        {
            IntPtr ret = _CreateRectRgn(nLeftRect, nTopRect, nRightRect, nBottomRect);
            if (IntPtr.Zero == ret)
            {
                throw new Win32Exception();
            }

            return ret;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static IntPtr CreateRectRgnIndirect(Rect lprc)
        {
            IntPtr ret = _CreateRectRgnIndirect(ref lprc);
            if (IntPtr.Zero == ret)
            {
                throw new Win32Exception();
            }

            return ret;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("gdi32.dll")]
        public static extern IntPtr CreateSolidBrush(int crColor);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static IntPtr CreateWindowEx(
            Ws_Ex dwExStyle,
            string lpClassName,
            string lpWindowName,
            Ws dwStyle,
            int x,
            int y,
            int nWidth,
            int nHeight,
            IntPtr hWndParent,
            IntPtr hMenu,
            IntPtr hInstance,
            IntPtr lpParam)
        {
            IntPtr ret = _CreateWindowEx(dwExStyle, lpClassName, lpWindowName, dwStyle, x, y, nWidth, nHeight, hWndParent, hMenu, hInstance,
                lpParam);
            if (IntPtr.Zero == ret)
            {
                Hresult.ThrowLastError();
            }

            return ret;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", CharSet = CharSet.Unicode, EntryPoint = "DefWindowProcW")]
        public static extern IntPtr DefWindowProc(IntPtr hWnd, Wm msg, IntPtr wParam, IntPtr lParam);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool DeleteObject(IntPtr hObject);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool DestroyIcon(IntPtr handle);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool DestroyWindow(IntPtr hwnd);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool IsWindow(IntPtr hwnd);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern void DwmExtendFrameIntoClientArea(IntPtr hwnd, ref Margins pMarInset);

        public static bool DwmGetColorizationColor(out uint pcrColorization, out bool pfOpaqueBlend)
        {
            // Make this call safe to make on downlevel OSes...
            if (Utility.IsOsVistaOrNewer && IsThemeActive())
            {
                Hresult hr = _DwmGetColorizationColor(out pcrColorization, out pfOpaqueBlend);
                if (hr.Succeeded)
                {
                    return true;
                }
            }

            // Default values.  If for some reason the native DWM API fails it's never enough of a reason
            // to bring down the app.  Empirically it still sometimes returns errors even when the theme service is on.
            // We'll still use the boolean return value to allow the caller to respond if they care.
            pcrColorization = 0xFF000000;
            pfOpaqueBlend = true;

            return false;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static bool DwmIsCompositionEnabled()
        {
            // Make this call safe to make on downlevel OSes...
            if (!Utility.IsOsVistaOrNewer)
            {
                return false;
            }

            return _DwmIsCompositionEnabled();
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("dwmapi.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool DwmDefWindowProc(IntPtr hwnd, Wm msg, IntPtr wParam, IntPtr lParam, out IntPtr plResult);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static void DwmSetWindowAttributeFlip3DPolicy(IntPtr hwnd, Dwmflip3D flip3DPolicy)
        {
            Assert.IsTrue(Utility.IsOsVistaOrNewer);
            var dwPolicy = (int) flip3DPolicy;
            _DwmSetWindowAttribute(hwnd, Dwmwa.Flip3DPolicy, ref dwPolicy, sizeof(int));
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static void DwmSetWindowAttributeDisallowPeek(IntPtr hwnd, bool disallowPeek)
        {
            Assert.IsTrue(Utility.IsOsWindows7OrNewer);
            int dwDisallow = (int) (disallowPeek ? Win32Value.TRUE : Win32Value.FALSE);
            _DwmSetWindowAttribute(hwnd, Dwmwa.DisallowPeek, ref dwDisallow, sizeof(int));
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static Mf EnableMenuItem(IntPtr hMenu, Sc uIdEnableItem, Mf uEnable)
        {
            // Returns the previous state of the menu item, or -1 if the menu item does not exist.
            int iRet = _EnableMenuItem(hMenu, uIdEnableItem, uEnable);
            return (Mf) iRet;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static void RemoveMenu(IntPtr hMenu, Sc uPosition, Mf uFlags)
        {
            if (!_RemoveMenu(hMenu, (uint) uPosition, (uint) uFlags))
            {
                throw new Win32Exception();
            }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static void DrawMenuBar(IntPtr hWnd)
        {
            if (!_DrawMenuBar(hWnd))
            {
                throw new Win32Exception();
            }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("kernel32.dll")]
        [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool FindClose(IntPtr handle);

        // Not shimming this SetLastError=true function because callers want to evaluate the reason for failure.
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern SafeFindHandle FindFirstFileW(string lpFileName, [In] [Out] [MarshalAs(UnmanagedType.LPStruct)]
            Win32_Find_Dataw lpFindFileData);

        // Not shimming this SetLastError=true function because callers want to evaluate the reason for failure.
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool FindNextFileW(SafeFindHandle hndFindFile, [In] [Out] [MarshalAs(UnmanagedType.LPStruct)]
            Win32_Find_Dataw lpFindFileData);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static Rect GetClientRect(IntPtr hwnd)
        {
            Rect rc;
            if (!_GetClientRect(hwnd, out rc))
            {
                Hresult.ThrowLastError();
            }

            return rc;
        }

        public static void GetCurrentThemeName(out string themeFileName, out string color, out string size)
        {
            // Not expecting strings longer than MAX_PATH.  We will return the error 
            var fileNameBuilder = new StringBuilder((int) Win32Value.MAX_PATH);
            var colorBuilder = new StringBuilder((int) Win32Value.MAX_PATH);
            var sizeBuilder = new StringBuilder((int) Win32Value.MAX_PATH);

            // This will throw if the theme service is not active (e.g. not UxTheme!IsThemeActive).
            _GetCurrentThemeName(fileNameBuilder, fileNameBuilder.Capacity,
                    colorBuilder, colorBuilder.Capacity,
                    sizeBuilder, sizeBuilder.Capacity)
                .ThrowIfFailed();

            themeFileName = fileNameBuilder.ToString();
            color = colorBuilder.ToString();
            size = sizeBuilder.ToString();
        }

        [DllImport("uxtheme.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool IsThemeActive();

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [Obsolete("Use SafeDC.GetDC instead.", true)]
        public static void GetDc()
        { }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("gdi32.dll")]
        public static extern int GetDeviceCaps(SafeDc hdc, DeviceCap nIndex);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static string GetModuleFileName(IntPtr hModule)
        {
            var buffer = new StringBuilder((int) Win32Value.MAX_PATH);
            while (true)
            {
                int size = _GetModuleFileName(hModule, buffer, buffer.Capacity);
                if (size == 0)
                {
                    Hresult.ThrowLastError();
                }

                // GetModuleFileName returns nSize when it's truncated but does NOT set the last error.
                // MSDN documentation says this has changed in Windows 2000+.
                if (size == buffer.Capacity)
                {
                    // Enlarge the buffer and try again.
                    buffer.EnsureCapacity(buffer.Capacity * 2);
                    continue;
                }

                return buffer.ToString();
            }
        }

        public static IntPtr GetModuleHandle(string lpModuleName)
        {
            IntPtr retPtr = _GetModuleHandle(lpModuleName);
            if (retPtr == IntPtr.Zero)
            {
                Hresult.ThrowLastError();
            }

            return retPtr;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static Monitorinfo GetMonitorInfo(IntPtr hMonitor)
        {
            var mi = new Monitorinfo();
            if (!_GetMonitorInfo(hMonitor, mi))
            {
                throw new Win32Exception();
            }

            return mi;
        }

        public static IntPtr GetStockObject(StockObject fnObject)
        {
            IntPtr retPtr = _GetStockObject(fnObject);
            if (retPtr == null)
            {
                Hresult.ThrowLastError();
            }

            return retPtr;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll")]
        public static extern IntPtr GetSystemMenu(IntPtr hWnd, [MarshalAs(UnmanagedType.Bool)] bool bRevert);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll")]
        public static extern int GetSystemMetrics(Sm nIndex);

        // This is aliased as a macro in 32bit Windows.
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static IntPtr GetWindowLongPtr(IntPtr hwnd, Gwl nIndex)
        {
            IntPtr ret = IntPtr.Zero;
            if (8 == IntPtr.Size)
            {
                ret = GetWindowLongPtr64(hwnd, nIndex);
            }
            else
            {
                ret = new IntPtr(GetWindowLongPtr32(hwnd, nIndex));
            }

            if (IntPtr.Zero == ret)
            {
                throw new Win32Exception();
            }

            return ret;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("uxtheme.dll", PreserveSig = false)]
        public static extern void SetWindowThemeAttribute([In] IntPtr hwnd, [In] Windowthemeattributetype eAttribute,
            [In] ref Wta_Options pvAttribute, [In] uint cbAttribute);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static Windowplacement GetWindowPlacement(IntPtr hwnd)
        {
            Windowplacement wndpl = new Windowplacement();
            if (GetWindowPlacement(hwnd, wndpl))
            {
                return wndpl;
            }

            throw new Win32Exception();
        }

        public static Rect GetWindowRect(IntPtr hwnd)
        {
            Rect rc;
            if (!_GetWindowRect(hwnd, out rc))
            {
                Hresult.ThrowLastError();
            }

            return rc;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("gdiplus.dll")]
        public static extern Status GdipCreateBitmapFromStream(IStream stream, out IntPtr bitmap);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("gdiplus.dll")]
        public static extern Status GdipCreateHBITMAPFromBitmap(IntPtr bitmap, out IntPtr hbmReturn, Int32 background);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("gdiplus.dll")]
        public static extern Status GdipCreateHICONFromBitmap(IntPtr bitmap, out IntPtr hbmReturn);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("gdiplus.dll")]
        public static extern Status GdipImageForceValidation(IntPtr image);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("gdiplus.dll")]
        public static extern Status GdiplusStartup(out IntPtr token, StartupInput input, out StartupOutput output);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("gdiplus.dll")]
        public static extern Status GdiplusShutdown(IntPtr token);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool IsWindowVisible(IntPtr hwnd);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll")]
        public static extern IntPtr MonitorFromWindow(IntPtr hwnd, uint dwFlags);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static void PostMessage(IntPtr hWnd, Wm msg, IntPtr wParam, IntPtr lParam)
        {
            if (!_PostMessage(hWnd, msg, wParam, lParam))
            {
                throw new Win32Exception();
            }
        }

        // Note that this will throw HRESULT_FROM_WIN32(ERROR_CLASS_ALREADY_EXISTS) on duplicate registration.
        // If needed, consider adding a Try* version of this function that returns the error code since that
        // may be ignorable.
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static short RegisterClassEx(ref Wndclassex lpwcx)
        {
            short ret = _RegisterClassEx(ref lpwcx);
            if (ret == 0)
            {
                Hresult.ThrowLastError();
            }

            return ret;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static Wm RegisterWindowMessage(string lpString)
        {
            uint iRet = _RegisterWindowMessage(lpString);
            if (iRet == 0)
            {
                Hresult.ThrowLastError();
            }

            return (Wm) iRet;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static IntPtr SetActiveWindow(IntPtr hwnd)
        {
            Verify.IsNotDefault(hwnd, "hwnd");
            IntPtr ret = _SetActiveWindow(hwnd);
            if (ret == IntPtr.Zero)
            {
                Hresult.ThrowLastError();
            }

            return ret;
        }

        // This is aliased as a macro in 32bit Windows.
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static IntPtr SetClassLongPtr(IntPtr hwnd, Gclp nIndex, IntPtr dwNewLong)
        {
            if (8 == IntPtr.Size)
            {
                return SetClassLongPtr64(hwnd, nIndex, dwNewLong);
            }

            return new IntPtr(SetClassLongPtr32(hwnd, nIndex, dwNewLong.ToInt32()));
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern ErrorModes SetErrorMode(ErrorModes newMode);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static void SetProcessWorkingSetSize(IntPtr hProcess, int dwMinimumWorkingSetSize, int dwMaximumWorkingSetSize)
        {
            if (!_SetProcessWorkingSetSize(hProcess, new IntPtr(dwMinimumWorkingSetSize), new IntPtr(dwMaximumWorkingSetSize)))
            {
                throw new Win32Exception();
            }
        }

        // This is aliased as a macro in 32bit Windows.
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static IntPtr SetWindowLongPtr(IntPtr hwnd, Gwl nIndex, IntPtr dwNewLong)
        {
            if (8 == IntPtr.Size)
            {
                return SetWindowLongPtr64(hwnd, nIndex, dwNewLong);
            }

            return new IntPtr(SetWindowLongPtr32(hwnd, nIndex, dwNewLong.ToInt32()));
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static void SetWindowRgn(IntPtr hWnd, IntPtr hRgn, bool bRedraw)
        {
            int err = _SetWindowRgn(hWnd, hRgn, bRedraw);
            if (0 == err)
            {
                throw new Win32Exception();
            }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy, Swp uFlags)
        {
            if (!_SetWindowPos(hWnd, hWndInsertAfter, x, y, cx, cy, uFlags))
            {
                // If this fails it's never worth taking down the process.  Let the caller deal with the error if they want.
                return false;
            }

            return true;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("shell32.dll", SetLastError = false)]
        public static extern Win32Error SHFileOperation(ref Shfileopstruct lpFileOp);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool ShowWindow(IntPtr hwnd, Sw nCmdShow);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static void SystemParametersInfo(Spi uiAction, int uiParam, string pvParam, Spif fWinIni)
        {
            if (!_SystemParametersInfo_String(uiAction, uiParam, pvParam, fWinIni))
            {
                Hresult.ThrowLastError();
            }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static Nonclientmetrics SystemParameterInfo_GetNONCLIENTMETRICS()
        {
            var metrics = Utility.IsOsVistaOrNewer
                ? Nonclientmetrics.VistaMetricsStruct
                : Nonclientmetrics.XpMetricsStruct;

            if (!_SystemParametersInfo_NONCLIENTMETRICS(Spi.Getnonclientmetrics, metrics.cbSize, ref metrics, Spif.None))
            {
                Hresult.ThrowLastError();
            }

            return metrics;
        }

        [SuppressMessage("Microsoft.Security", "CA2122:DoNotIndirectlyExposeMethodsWithLinkDemands")]
        public static Highcontrast SystemParameterInfo_GetHIGHCONTRAST()
        {
            var hc = new Highcontrast {cbSize = Marshal.SizeOf(typeof(Highcontrast))};

            if (!_SystemParametersInfo_HIGHCONTRAST(Spi.Gethighcontrast, hc.cbSize, ref hc, Spif.None))
            {
                Hresult.ThrowLastError();
            }

            return hc;
        }

        // This function is strange in that it returns a BOOL if TPM_RETURNCMD isn't specified, but otherwise the command Id.
        // Currently it's only used with TPM_RETURNCMD, so making the signature match that.
        [DllImport("user32.dll")]
        public static extern uint TrackPopupMenuEx(IntPtr hmenu, uint fuFlags, int x, int y, IntPtr hwnd, IntPtr lptpm);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static IntPtr SelectObject(SafeDc hdc, IntPtr hgdiobj)
        {
            IntPtr ret = _SelectObject(hdc, hgdiobj);
            if (ret == IntPtr.Zero)
            {
                Hresult.ThrowLastError();
            }

            return ret;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static IntPtr SelectObject(SafeDc hdc, SafeHbitmap hgdiobj)
        {
            IntPtr ret = _SelectObjectSafeHBITMAP(hdc, hgdiobj);
            if (ret == IntPtr.Zero)
            {
                Hresult.ThrowLastError();
            }

            return ret;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", SetLastError = true)]
        public static extern int SendInput(int nInputs, ref Input pInputs, int cbSize);

        // Depending on the message, callers may want to call GetLastError based on the return value.
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr SendMessage(IntPtr hWnd, Wm msg, IntPtr wParam, IntPtr lParam);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static void UnregisterClass(short atom, IntPtr hinstance)
        {
            if (!_UnregisterClassAtom(new IntPtr(atom), hinstance))
            {
                Hresult.ThrowLastError();
            }
        }

        public static void UnregisterClass(string lpClassName, IntPtr hInstance)
        {
            if (!_UnregisterClassName(lpClassName, hInstance))
            {
                Hresult.ThrowLastError();
            }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static void UpdateLayeredWindow(
            IntPtr hwnd,
            SafeDc hdcDst,
            ref Point pptDst,
            ref Size psize,
            SafeDc hdcSrc,
            ref Point pptSrc,
            int crKey,
            ref Blendfunction pblend,
            Ulw dwFlags)
        {
            if (!_UpdateLayeredWindow(hwnd, hdcDst, ref pptDst, ref psize, hdcSrc, ref pptSrc, crKey, ref pblend, dwFlags))
            {
                Hresult.ThrowLastError();
            }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static void UpdateLayeredWindow(
            IntPtr hwnd,
            int crKey,
            ref Blendfunction pblend,
            Ulw dwFlags)
        {
            if (!_UpdateLayeredWindowIntPtr(hwnd, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero, crKey, ref pblend,
                dwFlags))
            {
                Hresult.ThrowLastError();
            }
        }

        #endregion

        #region Private methods

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", EntryPoint = "AdjustWindowRectEx", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool _AdjustWindowRectEx(ref Rect lpRect, Ws dwStyle, [MarshalAs(UnmanagedType.Bool)] bool bMenu,
            Ws_Ex dwExStyle);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", EntryPoint = "ChangeWindowMessageFilter", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool _ChangeWindowMessageFilter(Wm message, Msgflt dwFlag);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", EntryPoint = "ChangeWindowMessageFilterEx", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool _ChangeWindowMessageFilterEx(IntPtr hwnd, Wm message, Msgflt action,
            [In] [Out] [Optional] ref Changefilterstruct pChangeFilterStruct);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("shell32.dll", EntryPoint = "CommandLineToArgvW", CharSet = CharSet.Unicode)]
        private static extern IntPtr _CommandLineToArgvW([MarshalAs(UnmanagedType.LPWStr)] string cmdLine, out int numArgs);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("gdi32.dll", EntryPoint = "CreateDIBSection", SetLastError = true)]
        private static extern SafeHbitmap _CreateDIBSection(SafeDc hdc, [In] ref Bitmapinfo bitmapInfo, int iUsage,
            [Out] out IntPtr ppvBits, IntPtr hSection, int dwOffset);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("gdi32.dll", EntryPoint = "CreateDIBSection", SetLastError = true)]
        private static extern SafeHbitmap _CreateDIBSectionIntPtr(IntPtr hdc, [In] ref Bitmapinfo bitmapInfo, int iUsage,
            [Out] out IntPtr ppvBits, IntPtr hSection, int dwOffset);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("gdi32.dll", EntryPoint = "CreateRoundRectRgn", SetLastError = true)]
        private static extern IntPtr _CreateRoundRectRgn(int nLeftRect, int nTopRect, int nRightRect, int nBottomRect, int nWidthEllipse,
            int nHeightEllipse);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("gdi32.dll", EntryPoint = "CreateRectRgn", SetLastError = true)]
        private static extern IntPtr _CreateRectRgn(int nLeftRect, int nTopRect, int nRightRect, int nBottomRect);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("gdi32.dll", EntryPoint = "CreateRectRgnIndirect", SetLastError = true)]
        private static extern IntPtr _CreateRectRgnIndirect([In] ref Rect lprc);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Unicode, EntryPoint = "CreateWindowExW")]
        private static extern IntPtr _CreateWindowEx(
            Ws_Ex dwExStyle,
            [MarshalAs(UnmanagedType.LPWStr)] string lpClassName,
            [MarshalAs(UnmanagedType.LPWStr)] string lpWindowName,
            Ws dwStyle,
            int x,
            int y,
            int nWidth,
            int nHeight,
            IntPtr hWndParent,
            IntPtr hMenu,
            IntPtr hInstance,
            IntPtr lpParam);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("dwmapi.dll", EntryPoint = "DwmIsCompositionEnabled", PreserveSig = false)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool _DwmIsCompositionEnabled();

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("dwmapi.dll", EntryPoint = "DwmGetColorizationColor", PreserveSig = true)]
        private static extern Hresult _DwmGetColorizationColor(out uint pcrColorization,
            [Out] [MarshalAs(UnmanagedType.Bool)] out bool pfOpaqueBlend);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("dwmapi.dll", EntryPoint = "DwmSetWindowAttribute")]
        private static extern void _DwmSetWindowAttribute(IntPtr hwnd, Dwmwa dwAttribute, ref int pvAttribute, int cbAttribute);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", EntryPoint = "EnableMenuItem")]
        private static extern int _EnableMenuItem(IntPtr hMenu, Sc uIdEnableItem, Mf uEnable);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", EntryPoint = "RemoveMenu", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool _RemoveMenu(IntPtr hMenu, uint uPosition, uint uFlags);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", EntryPoint = "DrawMenuBar", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool _DrawMenuBar(IntPtr hWnd);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", EntryPoint = "GetClientRect", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool _GetClientRect(IntPtr hwnd, [Out] out Rect lpRect);

        [DllImport("uxtheme.dll", EntryPoint = "GetCurrentThemeName", CharSet = CharSet.Unicode)]
        private static extern Hresult _GetCurrentThemeName(
            StringBuilder pszThemeFileName,
            int dwMaxNameChars,
            StringBuilder pszColorBuff,
            int cchMaxColorChars,
            StringBuilder pszSizeBuff,
            int cchMaxSizeChars);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("kernel32.dll", EntryPoint = "GetModuleFileName", CharSet = CharSet.Unicode, SetLastError = true)]
        private static extern int _GetModuleFileName(IntPtr hModule, StringBuilder lpFilename, int nSize);

        [DllImport("kernel32.dll", EntryPoint = "GetModuleHandleW", CharSet = CharSet.Unicode, SetLastError = true)]
        private static extern IntPtr _GetModuleHandle([MarshalAs(UnmanagedType.LPWStr)] string lpModuleName);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", EntryPoint = "GetMonitorInfo", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool _GetMonitorInfo(IntPtr hMonitor, [In] [Out] Monitorinfo lpmi);

        [DllImport("gdi32.dll", EntryPoint = "GetStockObject", SetLastError = true)]
        private static extern IntPtr _GetStockObject(StockObject fnObject);

        [SuppressMessage("Microsoft.Interoperability", "CA1400:PInvokeEntryPointsShouldExist")]
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", EntryPoint = "GetWindowLong", SetLastError = true)]
        private static extern int GetWindowLongPtr32(IntPtr hWnd, Gwl nIndex);

        [SuppressMessage("Microsoft.Interoperability", "CA1400:PInvokeEntryPointsShouldExist")]
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", EntryPoint = "GetWindowLongPtr", SetLastError = true)]
        private static extern IntPtr GetWindowLongPtr64(IntPtr hWnd, Gwl nIndex);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetWindowPlacement(IntPtr hwnd, Windowplacement lpwndpl);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", EntryPoint = "GetWindowRect", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool _GetWindowRect(IntPtr hWnd, out Rect lpRect);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("kernel32.dll", EntryPoint = "LocalFree", SetLastError = true)]
        private static extern IntPtr _LocalFree(IntPtr hMem);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", EntryPoint = "PostMessage", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool _PostMessage(IntPtr hWnd, Wm msg, IntPtr wParam, IntPtr lParam);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", SetLastError = true, EntryPoint = "RegisterClassExW")]
        private static extern short _RegisterClassEx([In] ref Wndclassex lpwcx);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", EntryPoint = "RegisterWindowMessage", SetLastError = true, CharSet = CharSet.Unicode)]
        private static extern uint _RegisterWindowMessage([MarshalAs(UnmanagedType.LPWStr)] string lpString);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", EntryPoint = "SetActiveWindow", SetLastError = true)]
        private static extern IntPtr _SetActiveWindow(IntPtr hWnd);

        [SuppressMessage("Microsoft.Interoperability", "CA1400:PInvokeEntryPointsShouldExist")]
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", EntryPoint = "SetClassLong", SetLastError = true)]
        private static extern int SetClassLongPtr32(IntPtr hWnd, Gclp nIndex, int dwNewLong);

        [SuppressMessage("Microsoft.Interoperability", "CA1400:PInvokeEntryPointsShouldExist")]
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", EntryPoint = "SetClassLongPtr", SetLastError = true)]
        private static extern IntPtr SetClassLongPtr64(IntPtr hWnd, Gclp nIndex, IntPtr dwNewLong);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("kernel32.dll", SetLastError = true, EntryPoint = "SetProcessWorkingSetSize")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool _SetProcessWorkingSetSize(IntPtr hProcess, IntPtr dwMinimiumWorkingSetSize,
            IntPtr dwMaximumWorkingSetSize);

        [SuppressMessage("Microsoft.Interoperability", "CA1400:PInvokeEntryPointsShouldExist")]
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", EntryPoint = "SetWindowLong", SetLastError = true)]
        private static extern int SetWindowLongPtr32(IntPtr hWnd, Gwl nIndex, int dwNewLong);

        [SuppressMessage("Microsoft.Interoperability", "CA1400:PInvokeEntryPointsShouldExist")]
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", EntryPoint = "SetWindowLongPtr", SetLastError = true)]
        private static extern IntPtr SetWindowLongPtr64(IntPtr hWnd, Gwl nIndex, IntPtr dwNewLong);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", EntryPoint = "SetWindowRgn", SetLastError = true)]
        private static extern int _SetWindowRgn(IntPtr hWnd, IntPtr hRgn, [MarshalAs(UnmanagedType.Bool)] bool bRedraw);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", EntryPoint = "SetWindowPos", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool _SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy, Swp uFlags);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", EntryPoint = "SystemParametersInfoW", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool _SystemParametersInfo_String(Spi uiAction, int uiParam, [MarshalAs(UnmanagedType.LPWStr)] string pvParam,
            Spif fWinIni);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", EntryPoint = "SystemParametersInfoW", SetLastError = true, CharSet = CharSet.Unicode)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool _SystemParametersInfo_NONCLIENTMETRICS(Spi uiAction, int uiParam,
            [In] [Out] ref Nonclientmetrics pvParam, Spif fWinIni);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", EntryPoint = "SystemParametersInfoW", SetLastError = true, CharSet = CharSet.Unicode)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool _SystemParametersInfo_HIGHCONTRAST(Spi uiAction, int uiParam, [In] [Out] ref Highcontrast pvParam,
            Spif fWinIni);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("gdi32.dll", EntryPoint = "SelectObject", SetLastError = true)]
        private static extern IntPtr _SelectObject(SafeDc hdc, IntPtr hgdiobj);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("gdi32.dll", EntryPoint = "SelectObject", SetLastError = true)]
        private static extern IntPtr _SelectObjectSafeHBITMAP(SafeDc hdc, SafeHbitmap hgdiobj);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", EntryPoint = "UnregisterClass", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool _UnregisterClassAtom(IntPtr lpClassName, IntPtr hInstance);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", EntryPoint = "UnregisterClass", CharSet = CharSet.Unicode, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool _UnregisterClassName(string lpClassName, IntPtr hInstance);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", SetLastError = true, EntryPoint = "UpdateLayeredWindow")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool _UpdateLayeredWindow(
            IntPtr hwnd,
            SafeDc hdcDst,
            [In] ref Point pptDst,
            [In] ref Size psize,
            SafeDc hdcSrc,
            [In] ref Point pptSrc,
            int crKey,
            ref Blendfunction pblend,
            Ulw dwFlags);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("user32.dll", SetLastError = true, EntryPoint = "UpdateLayeredWindow")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool _UpdateLayeredWindowIntPtr(
            IntPtr hwnd,
            IntPtr hdcDst,
            IntPtr pptDst,
            IntPtr psize,
            IntPtr hdcSrc,
            IntPtr pptSrc,
            int crKey,
            ref Blendfunction pblend,
            Ulw dwFlags);

        #endregion

        #region Dispose

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("gdiplus.dll")]
        public static extern Status GdipDisposeImage(IntPtr image);

        #endregion

        #region Win7 declarations

        [DllImport("shell32.dll", EntryPoint = "SHAddToRecentDocs")]
        private static extern void _SHAddToRecentDocs_String(Shard uFlags, [MarshalAs(UnmanagedType.LPWStr)] string pv);

        // This overload is required.  There's a cast in the Shell code that causes the wrong vtbl to be used
        // if we let the marshaller convert the parameter to an IUnknown.
        [DllImport("shell32.dll", EntryPoint = "SHAddToRecentDocs")]
        private static extern void _SHAddToRecentDocs_ShellLink(Shard uFlags, IShellLinkW pv);

        public static void ShAddToRecentDocs(string path)
        {
            _SHAddToRecentDocs_String(Shard.Pathw, path);
        }

        // Win7 only.
        public static void ShAddToRecentDocs(IShellLinkW shellLink)
        {
            _SHAddToRecentDocs_ShellLink(Shard.Link, shellLink);
        }


        // #define DWM_SIT_DISPLAYFRAME    0x00000001  // Display a window frame around the provided bitmap

        [DllImport("dwmapi.dll", EntryPoint = "DwmGetCompositionTimingInfo")]
        private static extern Hresult _DwmGetCompositionTimingInfo(IntPtr hwnd, ref Dwm_Timing_Info pTimingInfo);

        public static Dwm_Timing_Info? DwmGetCompositionTimingInfo(IntPtr hwnd)
        {
            if (!Utility.IsOsVistaOrNewer)
            {
                // API was new to Vista.
                return null;
            }

            var dti = new Dwm_Timing_Info {cbSize = Marshal.SizeOf(typeof(Dwm_Timing_Info))};
            Hresult hr = _DwmGetCompositionTimingInfo(hwnd, ref dti);
            if (hr == Hresult.E_PENDING)
            {
                // The system isn't yet ready to respond.  Return null rather than throw.
                return null;
            }

            hr.ThrowIfFailed();

            return dti;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern void DwmInvalidateIconicBitmaps(IntPtr hwnd);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern void DwmSetIconicThumbnail(IntPtr hwnd, IntPtr hbmp, Dwm_Sit dwSitFlags);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern void DwmSetIconicLivePreviewBitmap(IntPtr hwnd, IntPtr hbmp, RefPoint pptClient, Dwm_Sit dwSitFlags);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("shell32.dll", PreserveSig = false)]
        public static extern void SHGetItemFromDataObject(IDataObject pdtobj, Dogif dwFlags, [In] ref Guid riid,
            [Out] [MarshalAs(UnmanagedType.Interface)]
            out object ppv);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("shell32.dll", PreserveSig = false)]
        public static extern Hresult SHCreateItemFromParsingName([MarshalAs(UnmanagedType.LPWStr)] string pszPath, IBindCtx pbc,
            [In] ref Guid riid, [Out] [MarshalAs(UnmanagedType.Interface)]
            out object ppv);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("shell32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool Shell_NotifyIcon(Nim dwMessage, [In] Notifyicondata lpdata);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("shell32.dll", PreserveSig = false)]
        public static extern void SetCurrentProcessExplicitAppUserModelID([MarshalAs(UnmanagedType.LPWStr)] string appId);

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DllImport("shell32.dll")]
        public static extern Hresult GetCurrentProcessExplicitAppUserModelID([Out] [MarshalAs(UnmanagedType.LPWStr)]
            out string appId);

        #endregion
    }
}