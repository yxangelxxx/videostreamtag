﻿#region

using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Media;

#endregion

namespace DockManager.Controls.Shell.Standard
{
    internal static class DpiHelper
    {
        #region Static

        private static Matrix _transformToDevice;
        private static Matrix _transformToDip;

        #endregion

        #region Construct

        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline")]
        static DpiHelper()
        {
            using (SafeDc desktop = SafeDc.GetDesktop())
            {
                int pixelsPerInchX = NativeMethods.GetDeviceCaps(desktop, DeviceCap.Logpixelsx);
                int pixelsPerInchY = NativeMethods.GetDeviceCaps(desktop, DeviceCap.Logpixelsy);

                _transformToDip = Matrix.Identity;
                _transformToDip.Scale(96d / (double) pixelsPerInchX, 96d / (double) pixelsPerInchY);
                _transformToDevice = Matrix.Identity;
                _transformToDevice.Scale((double) pixelsPerInchX / 96d, (double) pixelsPerInchY / 96d);
            }
        }

        #endregion

        #region Public

        public static System.Windows.Point LogicalPixelsToDevice(System.Windows.Point logicalPoint)
        {
            return _transformToDevice.Transform(logicalPoint);
        }

        public static System.Windows.Point DevicePixelsToLogical(System.Windows.Point devicePoint)
        {
            return _transformToDip.Transform(devicePoint);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static System.Windows.Rect LogicalRectToDevice(System.Windows.Rect logicalRectangle)
        {
            System.Windows.Point topLeft = LogicalPixelsToDevice(new System.Windows.Point(logicalRectangle.Left, logicalRectangle.Top));
            System.Windows.Point bottomRight = LogicalPixelsToDevice(new System.Windows.Point(logicalRectangle.Right, logicalRectangle.Bottom));

            return new System.Windows.Rect(topLeft, bottomRight);
        }

        public static System.Windows.Rect DeviceRectToLogical(System.Windows.Rect deviceRectangle)
        {
            System.Windows.Point topLeft = DevicePixelsToLogical(new System.Windows.Point(deviceRectangle.Left, deviceRectangle.Top));
            System.Windows.Point bottomRight = DevicePixelsToLogical(new System.Windows.Point(deviceRectangle.Right, deviceRectangle.Bottom));

            return new System.Windows.Rect(topLeft, bottomRight);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static System.Windows.Size LogicalSizeToDevice(System.Windows.Size logicalSize)
        {
            System.Windows.Point pt = LogicalPixelsToDevice(new System.Windows.Point(logicalSize.Width, logicalSize.Height));

            return new System.Windows.Size {Width = pt.X, Height = pt.Y};
        }

        public static System.Windows.Size DeviceSizeToLogical(System.Windows.Size deviceSize)
        {
            System.Windows.Point pt = DevicePixelsToLogical(new System.Windows.Point(deviceSize.Width, deviceSize.Height));

            return new System.Windows.Size(pt.X, pt.Y);
        }

        #endregion
    }
}