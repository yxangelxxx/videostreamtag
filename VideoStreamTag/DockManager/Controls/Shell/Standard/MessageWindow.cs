﻿#region

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Threading;

#endregion

namespace DockManager.Controls.Shell.Standard
{
    internal sealed class MessageWindow : DispatcherObject, IDisposable
    {
        #region Static

        private static readonly WndProc SWndProc = _WndProc;
        private static readonly Dictionary<IntPtr, MessageWindow> SWindowLookup = new Dictionary<IntPtr, MessageWindow>();

        #endregion

        #region Private fields

        private string _className;

        private WndProc _wndProcCallback;

        #endregion

        #region Construct

        [SuppressMessage("Microsoft.Security", "CA2122:DoNotIndirectlyExposeMethodsWithLinkDemands")]
        public MessageWindow(Cs classStyle, Ws style, Ws_Ex exStyle, System.Windows.Rect location, string name, WndProc callback)
        {
            _wndProcCallback = callback;
            _className = "MessageWindowClass+" + Guid.NewGuid().ToString();

            var wc = new Wndclassex
            {
                cbSize = Marshal.SizeOf(typeof(Wndclassex)),
                style = classStyle,
                lpfnWndProc = SWndProc,
                hInstance = NativeMethods.GetModuleHandle(null),
                hbrBackground = NativeMethods.GetStockObject(StockObject.NullBrush),
                lpszMenuName = "",
                lpszClassName = _className,
            };

            NativeMethods.RegisterClassEx(ref wc);

            GCHandle gcHandle = default(GCHandle);
            try
            {
                gcHandle = GCHandle.Alloc(this);
                IntPtr pinnedThisPtr = (IntPtr) gcHandle;

                Handle = NativeMethods.CreateWindowEx(
                    exStyle,
                    _className,
                    name,
                    style,
                    (int) location.X,
                    (int) location.Y,
                    (int) location.Width,
                    (int) location.Height,
                    IntPtr.Zero,
                    IntPtr.Zero,
                    IntPtr.Zero,
                    pinnedThisPtr);
            }
            finally
            {
                gcHandle.Free();
            }
        }

        ~MessageWindow()
        {
            _Dispose(false, false);
        }

        #endregion

        #region Public

        public IntPtr Handle { get; private set; }

        #endregion

        #region Private methods

        [SuppressMessage("Microsoft.Usage", "CA1816:CallGCSuppressFinalizeCorrectly")]
        private static IntPtr _WndProc(IntPtr hwnd, Wm msg, IntPtr wParam, IntPtr lParam)
        {
            IntPtr ret = IntPtr.Zero;
            MessageWindow hwndWrapper = null;

            if (msg == Wm.Create)
            {
                var createStruct = (Createstruct) Marshal.PtrToStructure(lParam, typeof(Createstruct));
                GCHandle gcHandle = GCHandle.FromIntPtr(createStruct.lpCreateParams);
                hwndWrapper = (MessageWindow) gcHandle.Target;
                SWindowLookup.Add(hwnd, hwndWrapper);
            }
            else
            {
                if (!SWindowLookup.TryGetValue(hwnd, out hwndWrapper))
                {
                    return NativeMethods.DefWindowProc(hwnd, msg, wParam, lParam);
                }
            }

            Assert.IsNotNull(hwndWrapper);

            WndProc callback = hwndWrapper._wndProcCallback;
            if (callback != null)
            {
                ret = callback(hwnd, msg, wParam, lParam);
            }
            else
            {
                ret = NativeMethods.DefWindowProc(hwnd, msg, wParam, lParam);
            }

            if (msg == Wm.Ncdestroy)
            {
                hwndWrapper._Dispose(true, true);
                GC.SuppressFinalize(hwndWrapper);
            }

            return ret;
        }

        private static object _DestroyWindow(IntPtr hwnd, string className)
        {
            Utility.SafeDestroyWindow(ref hwnd);
            NativeMethods.UnregisterClass(className, NativeMethods.GetModuleHandle(null));
            return null;
        }

        #endregion

        #region Dispose

        private bool _isDisposed;

        public void Dispose()
        {
            _Dispose(true, false);
            GC.SuppressFinalize(this);
        }

        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "disposing")]
        private void _Dispose(bool disposing, bool isHwndBeingDestroyed)
        {
            if (_isDisposed)
            {
                return;
            }

            _isDisposed = true;

            IntPtr hwnd = Handle;
            string className = _className;

            if (isHwndBeingDestroyed)
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                    (DispatcherOperationCallback) (arg => _DestroyWindow(IntPtr.Zero, className)));
            }
            else if (Handle != IntPtr.Zero)
            {
                if (CheckAccess())
                {
                    _DestroyWindow(hwnd, className);
                }
                else
                {
                    Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                        (DispatcherOperationCallback) (arg => _DestroyWindow(hwnd, className)));
                }
            }

            SWindowLookup.Remove(hwnd);

            _className = null;
            Handle = IntPtr.Zero;
        }

        #endregion
    }
}