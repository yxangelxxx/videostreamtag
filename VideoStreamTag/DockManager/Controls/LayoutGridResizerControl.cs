﻿#region

using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

#endregion

namespace DockManager.Controls
{
    public class LayoutGridResizerControl : Thumb
    {
        #region Construct

        static LayoutGridResizerControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(LayoutGridResizerControl),
                new FrameworkPropertyMetadata(typeof(LayoutGridResizerControl)));
            HorizontalAlignmentProperty.OverrideMetadata(typeof(LayoutGridResizerControl),
                new FrameworkPropertyMetadata(HorizontalAlignment.Stretch, FrameworkPropertyMetadataOptions.AffectsParentMeasure));
            VerticalAlignmentProperty.OverrideMetadata(typeof(LayoutGridResizerControl),
                new FrameworkPropertyMetadata(VerticalAlignment.Stretch, FrameworkPropertyMetadataOptions.AffectsParentMeasure));
            BackgroundProperty.OverrideMetadata(typeof(LayoutGridResizerControl), new FrameworkPropertyMetadata(Brushes.Transparent));
            IsHitTestVisibleProperty.OverrideMetadata(typeof(LayoutGridResizerControl), new FrameworkPropertyMetadata(true, null));
        }

        #endregion


        #region BackgroundWhileDragging

        public static readonly DependencyProperty BackgroundWhileDraggingProperty =
            DependencyProperty.Register("BackgroundWhileDragging", typeof(Brush), typeof(LayoutGridResizerControl),
                new FrameworkPropertyMetadata((Brush) Brushes.Black));

        public Brush BackgroundWhileDragging
        {
            get { return (Brush) GetValue(BackgroundWhileDraggingProperty); }
            set { SetValue(BackgroundWhileDraggingProperty, value); }
        }

        #endregion

        #region OpacityWhileDragging

        public static readonly DependencyProperty OpacityWhileDraggingProperty =
            DependencyProperty.Register("OpacityWhileDragging", typeof(double), typeof(LayoutGridResizerControl),
                new FrameworkPropertyMetadata((double) 0.5));

        public double OpacityWhileDragging
        {
            get { return (double) GetValue(OpacityWhileDraggingProperty); }
            set { SetValue(OpacityWhileDraggingProperty, value); }
        }

        #endregion
    }
}