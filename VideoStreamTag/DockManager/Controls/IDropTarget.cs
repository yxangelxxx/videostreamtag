﻿#region

using System.Windows;
using System.Windows.Media;
using DockManager.Layout;

#endregion

namespace DockManager.Controls
{
    internal interface IDropTarget
    {
        #region Public

        Geometry GetPreviewPath(OverlayWindow overlayWindow, LayoutFloatingWindow floatingWindow);

        bool HitTest(Point dragPoint);

        void Drop(LayoutFloatingWindow floatingWindow);

        void DragEnter();

        void DragLeave();

        DropTargetType Type { get; }

        #endregion
    }
}