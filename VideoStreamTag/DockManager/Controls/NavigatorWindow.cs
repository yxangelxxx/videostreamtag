﻿#region

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using DockManager.Layout;
using DockManager.Themes;

#endregion

namespace DockManager.Controls
{
    public class NavigatorWindow : Window
    {
        #region Private fields

        private DockingManager _manager;

        #endregion

        #region Internals

        internal NavigatorWindow(DockingManager manager)
        {
            _manager = manager;

            _internalSetSelectedDocument = true;
            SetAnchorables(_manager.Layout.Descendents().OfType<LayoutAnchorable>().Where(a => a.IsVisible)
                .Select(d => (LayoutAnchorableItem) _manager.GetLayoutItemFromModel(d)).ToArray());
            SetDocuments(_manager.Layout.Descendents().OfType<LayoutDocument>()
                .OrderByDescending(d => d.LastActivationTimeStamp.GetValueOrDefault())
                .Select(d => (LayoutDocumentItem) _manager.GetLayoutItemFromModel(d)).ToArray());
            _internalSetSelectedDocument = false;

            if (Documents.Length > 1)
                InternalSetSelectedDocument(Documents[1]);

            this.DataContext = this;

            this.Loaded += OnLoaded;
            this.Unloaded += OnUnloaded;

            UpdateThemeResources();
        }


        internal void UpdateThemeResources(Theme oldTheme = null)
        {
            if (oldTheme != null)
            {
                var resourceDictionaryToRemove =
                    Resources.MergedDictionaries.FirstOrDefault(r => r.Source == oldTheme.GetResourceUri());
                if (resourceDictionaryToRemove != null)
                    Resources.MergedDictionaries.Remove(
                        resourceDictionaryToRemove);
            }

            if (_manager.Theme != null)
            {
                Resources.MergedDictionaries.Add(new ResourceDictionary() {Source = _manager.Theme.GetResourceUri()});
            }
        }


        internal void SelectNextDocument()
        {
            if (SelectedDocument != null)
            {
                int docIndex = Documents.IndexOf<LayoutDocumentItem>(SelectedDocument);
                docIndex++;
                if (docIndex == Documents.Length)
                    docIndex = 0;
                InternalSetSelectedDocument(Documents[docIndex]);
            }
        }

        #endregion

        #region Construct

        static NavigatorWindow()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(NavigatorWindow), new FrameworkPropertyMetadata(typeof(NavigatorWindow)));
            ShowActivatedProperty.OverrideMetadata(typeof(NavigatorWindow), new FrameworkPropertyMetadata(false));
            ShowInTaskbarProperty.OverrideMetadata(typeof(NavigatorWindow), new FrameworkPropertyMetadata(false));
        }

        #endregion

        #region Private methods

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= OnLoaded;

            this.Focus();

            //this.SetParentToMainWindowOf(_manager);
            WindowStartupLocation = WindowStartupLocation.CenterOwner;
        }

        private void OnUnloaded(object sender, RoutedEventArgs e)
        {
            this.Unloaded -= OnUnloaded;

            //_hwndSrc.RemoveHook(_hwndSrcHook);
            //_hwndSrc.Dispose();
            //_hwndSrc = null;
        }

        #endregion

        #region Protected Методы

        protected override void OnKeyDown(System.Windows.Input.KeyEventArgs e)
        {
            base.OnKeyDown(e);
        }

        protected override void OnPreviewKeyDown(System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Tab)
            {
                SelectNextDocument();
                e.Handled = true;
            }


            base.OnPreviewKeyDown(e);
        }

        protected override void OnPreviewKeyUp(System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key != System.Windows.Input.Key.Tab)
            {
                if (SelectedAnchorable != null &&
                    SelectedAnchorable.ActivateCommand.CanExecute(null))
                    SelectedAnchorable.ActivateCommand.Execute(null);

                if (SelectedAnchorable == null &&
                    SelectedDocument != null &&
                    SelectedDocument.ActivateCommand.CanExecute(null))
                    SelectedDocument.ActivateCommand.Execute(null);
                Close();
                e.Handled = true;
            }


            base.OnPreviewKeyUp(e);
        }

        #endregion

        //protected virtual IntPtr FilterMessage(
        //    IntPtr hwnd,
        //    int msg,
        //    IntPtr wParam,
        //    IntPtr lParam,
        //    ref bool handled
        //    )
        //{
        //    handled = false;

        //    switch (msg)
        //    {
        //        case Win32Helper.WM_ACTIVATE:
        //            if (((int)wParam & 0xFFFF) == Win32Helper.WA_INACTIVE)
        //            {
        //                if (lParam == new WindowInteropHelper(this.Owner).Handle)
        //                {
        //                    Win32Helper.SetActiveWindow(_hwndSrc.Handle);
        //                    handled = true;
        //                }

        //            }
        //            break;
        //    }

        //    return IntPtr.Zero;
        //}


        #region Documents

        private static readonly DependencyPropertyKey DocumentsPropertyKey
            = DependencyProperty.RegisterReadOnly("Documents", typeof(IEnumerable<LayoutDocumentItem>), typeof(NavigatorWindow),
                new FrameworkPropertyMetadata(null));

        public static readonly DependencyProperty DocumentsProperty
            = DocumentsPropertyKey.DependencyProperty;

        public LayoutDocumentItem[] Documents
        {
            get { return (LayoutDocumentItem[]) GetValue(DocumentsProperty); }
        }

        protected void SetDocuments(LayoutDocumentItem[] value)
        {
            SetValue(DocumentsPropertyKey, value);
        }

        #endregion

        #region Anchorables

        private static readonly DependencyPropertyKey AnchorablesPropertyKey
            = DependencyProperty.RegisterReadOnly("Anchorables", typeof(IEnumerable<LayoutAnchorableItem>), typeof(NavigatorWindow),
                new FrameworkPropertyMetadata((IEnumerable<LayoutAnchorableItem>) null));

        public static readonly DependencyProperty AnchorablesProperty
            = AnchorablesPropertyKey.DependencyProperty;

        public IEnumerable<LayoutAnchorableItem> Anchorables
        {
            get { return (IEnumerable<LayoutAnchorableItem>) GetValue(AnchorablesProperty); }
        }

        protected void SetAnchorables(IEnumerable<LayoutAnchorableItem> value)
        {
            SetValue(AnchorablesPropertyKey, value);
        }

        #endregion

        #region SelectedDocument

        public static readonly DependencyProperty SelectedDocumentProperty =
            DependencyProperty.Register("SelectedDocument", typeof(LayoutDocumentItem), typeof(NavigatorWindow),
                new FrameworkPropertyMetadata((LayoutDocumentItem) null,
                    OnSelectedDocumentChanged));

        public LayoutDocumentItem SelectedDocument
        {
            get { return (LayoutDocumentItem) GetValue(SelectedDocumentProperty); }
            set { SetValue(SelectedDocumentProperty, value); }
        }

        private static void OnSelectedDocumentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((NavigatorWindow) d).OnSelectedDocumentChanged(e);
        }

        protected virtual void OnSelectedDocumentChanged(DependencyPropertyChangedEventArgs e)
        {
            if (_internalSetSelectedDocument)
                return;

            if (SelectedDocument != null &&
                SelectedDocument.ActivateCommand.CanExecute(null))
            {
                System.Diagnostics.Trace.WriteLine("OnSelectedDocumentChanged()");
                SelectedDocument.ActivateCommand.Execute(null);
                Hide();
            }
        }

        private bool _internalSetSelectedDocument = false;

        private void InternalSetSelectedDocument(LayoutDocumentItem documentToSelect)
        {
            _internalSetSelectedDocument = true;
            SelectedDocument = documentToSelect;
            _internalSetSelectedDocument = false;
        }

        #endregion

        #region SelectedAnchorable

        public static readonly DependencyProperty SelectedAnchorableProperty =
            DependencyProperty.Register("SelectedAnchorable", typeof(LayoutAnchorableItem), typeof(NavigatorWindow),
                new FrameworkPropertyMetadata((LayoutAnchorableItem) null,
                    OnSelectedAnchorableChanged));

        public LayoutAnchorableItem SelectedAnchorable
        {
            get { return (LayoutAnchorableItem) GetValue(SelectedAnchorableProperty); }
            set { SetValue(SelectedAnchorableProperty, value); }
        }

        private static void OnSelectedAnchorableChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((NavigatorWindow) d).OnSelectedAnchorableChanged(e);
        }

        protected virtual void OnSelectedAnchorableChanged(DependencyPropertyChangedEventArgs e)
        {
            var selectedAnchorable = e.NewValue as LayoutAnchorableItem;
            if (SelectedAnchorable != null &&
                SelectedAnchorable.ActivateCommand.CanExecute(null))
            {
                SelectedAnchorable.ActivateCommand.Execute(null);
                Close();
            }
        }

        #endregion
    }
}