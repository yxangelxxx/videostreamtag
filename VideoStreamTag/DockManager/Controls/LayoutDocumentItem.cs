﻿#region

using System.Windows;
using DockManager.Layout;

#endregion

namespace DockManager.Controls
{
    public class LayoutDocumentItem : LayoutItem
    {
        #region Private fields

        private LayoutDocument _document;

        #endregion

        #region Internals

        internal LayoutDocumentItem()
        { }

        internal override void Attach(LayoutContent model)
        {
            _document = model as LayoutDocument;
            base.Attach(model);
        }

        internal override void Detach()
        {
            _document = null;
            base.Detach();
        }

        #endregion

        #region Protected Методы

        protected override void Close()
        {
            var dockingManager = _document.Root.Manager;
            dockingManager._ExecuteCloseCommand(_document);
        }

        #endregion

        #region Description

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(LayoutDocumentItem),
                new FrameworkPropertyMetadata((string) null,
                    OnDescriptionChanged));

        public string Description
        {
            get { return (string) GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        private static void OnDescriptionChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((LayoutDocumentItem) d).OnDescriptionChanged(e);
        }

        protected virtual void OnDescriptionChanged(DependencyPropertyChangedEventArgs e)
        {
            _document.Description = (string) e.NewValue;
        }

        #endregion
    }
}