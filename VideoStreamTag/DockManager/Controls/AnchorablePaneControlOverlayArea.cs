﻿#region

using System.Windows;

#endregion

namespace DockManager.Controls
{
    public class AnchorablePaneControlOverlayArea : OverlayArea
    {
        #region Private fields

        private LayoutAnchorablePaneControl _anchorablePaneControl;

        #endregion

        #region Internals

        internal AnchorablePaneControlOverlayArea(
            IOverlayWindow overlayWindow,
            LayoutAnchorablePaneControl anchorablePaneControl)
            : base(overlayWindow)
        {
            _anchorablePaneControl = anchorablePaneControl;
            base.SetScreenDetectionArea(new Rect(
                _anchorablePaneControl.PointToScreenDpi(new Point()),
                _anchorablePaneControl.TransformActualSizeToAncestor()));
        }

        #endregion
    }
}