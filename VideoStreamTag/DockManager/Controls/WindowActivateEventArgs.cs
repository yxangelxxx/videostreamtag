﻿#region

using System;

#endregion

namespace DockManager.Controls
{
    internal class WindowActivateEventArgs : EventArgs
    {
        #region Construct

        public WindowActivateEventArgs(IntPtr hwndActivating)
        {
            HwndActivating = hwndActivating;
        }

        #endregion

        #region Public

        public IntPtr HwndActivating { get; private set; }

        #endregion
    }
}