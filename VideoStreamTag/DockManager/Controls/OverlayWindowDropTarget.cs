﻿#region

using System.Windows;

#endregion

namespace DockManager.Controls
{
    public class OverlayWindowDropTarget : IOverlayWindowDropTarget
    {
        #region Private fields

        Rect IOverlayWindowDropTarget.ScreenDetectionArea
        {
            get { return _screenDetectionArea; }
        }

        OverlayWindowDropTargetType IOverlayWindowDropTarget.Type
        {
            get { return _type; }
        }

        private IOverlayWindowArea _overlayArea;

        private Rect _screenDetectionArea;

        private OverlayWindowDropTargetType _type;

        #endregion

        #region Internals

        internal OverlayWindowDropTarget(IOverlayWindowArea overlayArea, OverlayWindowDropTargetType targetType, FrameworkElement element)
        {
            _overlayArea = overlayArea;
            _type = targetType;
            _screenDetectionArea = new Rect(element.TransformToDeviceDpi(new Point()), element.TransformActualSizeToAncestor());
        }

        #endregion
    }
}