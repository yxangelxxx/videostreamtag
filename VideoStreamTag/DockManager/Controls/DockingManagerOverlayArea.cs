﻿#region

using System.Windows;

#endregion

namespace DockManager.Controls
{
    public class DockingManagerOverlayArea : OverlayArea
    {
        #region Private fields

        private DockingManager _manager;

        #endregion

        #region Internals

        internal DockingManagerOverlayArea(IOverlayWindow overlayWindow, DockingManager manager)
            : base(overlayWindow)
        {
            _manager = manager;

            base.SetScreenDetectionArea(new Rect(
                _manager.PointToScreenDpi(new Point()),
                _manager.TransformActualSizeToAncestor()));
        }

        #endregion
    }
}