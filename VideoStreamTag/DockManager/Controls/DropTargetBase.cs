﻿#region

using System.Windows;

#endregion

namespace DockManager.Controls
{
    internal abstract class DropTargetBase : DependencyObject
    {
        #region IsDraggingOver

        public static readonly DependencyProperty IsDraggingOverProperty =
            DependencyProperty.RegisterAttached("IsDraggingOver", typeof(bool), typeof(DropTargetBase),
                new FrameworkPropertyMetadata((bool) false));

        public static bool GetIsDraggingOver(DependencyObject d)
        {
            return (bool) d.GetValue(IsDraggingOverProperty);
        }

        public static void SetIsDraggingOver(DependencyObject d, bool value)
        {
            d.SetValue(IsDraggingOverProperty, value);
        }

        #endregion
    }
}