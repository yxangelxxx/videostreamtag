﻿#region

using System.Windows;

#endregion

namespace DockManager.Controls
{
    public abstract class OverlayArea : IOverlayWindowArea
    {
        #region Private fields

        Rect IOverlayWindowArea.ScreenDetectionArea
        {
            get { return _screenDetectionArea.Value; }
        }

        private IOverlayWindow _overlayWindow;

        private Rect? _screenDetectionArea;

        #endregion

        #region Internals

        internal OverlayArea(IOverlayWindow overlayWindow)
        {
            _overlayWindow = overlayWindow;
        }

        #endregion

        #region Protected Методы

        protected void SetScreenDetectionArea(Rect rect)
        {
            _screenDetectionArea = rect;
        }

        #endregion
    }
}