﻿#region

using System;
using System.Collections.Generic;

#endregion

namespace DockManager.Controls
{
    internal class WeakDictionary<TK, TV> where TK : class
    {
        #region Private fields

        private List<WeakReference> _keys = new List<WeakReference>();
        private List<TV> _values = new List<TV>();

        #endregion

        #region Construct

        public WeakDictionary()
        { }

        #endregion

        #region Public

        public bool ContainsKey(TK key)
        {
            CollectGarbage();
            return -1 != _keys.FindIndex(k => k.GetValueOrDefault<TK>() == key);
        }

        public void SetValue(TK key, TV value)
        {
            CollectGarbage();
            int vIndex = _keys.FindIndex(k => k.GetValueOrDefault<TK>() == key);
            if (vIndex > -1)
                _values[vIndex] = value;
            else
            {
                _values.Add(value);
                _keys.Add(new WeakReference(key));
            }
        }

        public bool GetValue(TK key, out TV value)
        {
            CollectGarbage();
            int vIndex = _keys.FindIndex(k => k.GetValueOrDefault<TK>() == key);
            value = default(TV);
            if (vIndex == -1)
                return false;
            value = _values[vIndex];
            return true;
        }

        #endregion

        #region Private methods

        private void CollectGarbage()
        {
            int vIndex = 0;

            do
            {
                vIndex = _keys.FindIndex(vIndex, k => !k.IsAlive);
                if (vIndex >= 0)
                {
                    _keys.RemoveAt(vIndex);
                    _values.RemoveAt(vIndex);
                }
            } while (vIndex >= 0);
        }

        #endregion

        #region Other members

        public TV this[TK key]
        {
            get
            {
                TV valueToReturn;
                if (!GetValue(key, out valueToReturn))
                    throw new ArgumentException();
                return valueToReturn;
            }
            set { SetValue(key, value); }
        }

        #endregion
    }
}