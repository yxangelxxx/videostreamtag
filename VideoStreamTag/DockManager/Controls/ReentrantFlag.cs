﻿#region

using System;

#endregion

namespace DockManager.Controls
{
    internal class ReentrantFlag
    {
        #region Private fields

        private bool _flag = false;

        #endregion

        #region Public

        public ReentrantFlagHandler Enter()
        {
            if (_flag)
                throw new InvalidOperationException();
            return new ReentrantFlagHandler(this);
        }

        public bool CanEnter
        {
            get { return !_flag; }
        }

        #endregion

        #region Other members

        public class ReentrantFlagHandler : IDisposable
        {
            #region Private fields

            private ReentrantFlag _owner;

            #endregion

            #region Construct

            public ReentrantFlagHandler(ReentrantFlag owner)
            {
                _owner = owner;
                _owner._flag = true;
            }

            #endregion

            #region Dispose

            public void Dispose()
            {
                _owner._flag = false;
            }

            #endregion
        }

        #endregion
    }
}