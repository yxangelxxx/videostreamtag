﻿#region

using System.Windows;

#endregion

namespace DockManager.Controls
{
    internal interface IOverlayWindowArea
    {
        #region Public

        Rect ScreenDetectionArea { get; }

        #endregion
    }
}