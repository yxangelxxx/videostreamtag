﻿#region

using System.Windows;

#endregion

namespace DockManager.Controls
{
    public enum DropAreaType
    {
        DockingManager,

        DocumentPane,

        DocumentPaneGroup,

        AnchorablePane,
    }


    public interface IDropArea
    {
        #region Public

        Rect DetectionRect { get; }
        DropAreaType Type { get; }

        #endregion
    }

    public class DropArea<T> : IDropArea where T : FrameworkElement
    {
        #region Private fields

        private Rect _detectionRect;

        private T _element;

        private DropAreaType _type;

        #endregion

        #region Internals

        internal DropArea(T areaElement, DropAreaType type)
        {
            _element = areaElement;
            _detectionRect = areaElement.GetScreenArea();
            _type = type;
        }

        #endregion

        #region Public

        public Rect DetectionRect
        {
            get { return _detectionRect; }
        }

        public DropAreaType Type
        {
            get { return _type; }
        }

        public T AreaElement
        {
            get { return _element; }
        }

        #endregion
    }
}