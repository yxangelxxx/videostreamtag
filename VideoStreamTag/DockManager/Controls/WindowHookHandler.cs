﻿#region

using System;

#endregion

namespace DockManager.Controls
{
    internal class FocusChangeEventArgs : EventArgs
    {
        #region Construct

        public FocusChangeEventArgs(IntPtr gotFocusWinHandle, IntPtr lostFocusWinHandle)
        {
            GotFocusWinHandle = gotFocusWinHandle;
            LostFocusWinHandle = lostFocusWinHandle;
        }

        #endregion

        #region Public

        public IntPtr GotFocusWinHandle { get; private set; }

        public IntPtr LostFocusWinHandle { get; private set; }

        #endregion
    }

    internal class WindowHookHandler
    {
        #region Events

        public event EventHandler<FocusChangeEventArgs> FocusChanged;

        #endregion

        #region Private fields

        private Win32Helper.HookProc _hookProc;

        //public event EventHandler<WindowActivateEventArgs> Activate;

        private ReentrantFlag _insideActivateEvent = new ReentrantFlag();

        private IntPtr _windowHook;

        #endregion

        #region Construct

        public WindowHookHandler()
        { }

        #endregion

        #region Public

        public void Attach()
        {
            _hookProc = this.HookProc;
            _windowHook = Win32Helper.SetWindowsHookEx(
                Win32Helper.HookType.WhCbt,
                _hookProc,
                IntPtr.Zero,
                (int) Win32Helper.GetCurrentThreadId());
        }


        public void Detach()
        {
            Win32Helper.UnhookWindowsHookEx(_windowHook);
        }

        public int HookProc(int code, IntPtr wParam, IntPtr lParam)
        {
            if (code == Win32Helper.HCBT_SETFOCUS)
            {
                if (FocusChanged != null)
                    FocusChanged(this, new FocusChangeEventArgs(wParam, lParam));
            }
            else if (code == Win32Helper.HCBT_ACTIVATE)
            {
                if (_insideActivateEvent.CanEnter)
                {
                    using (_insideActivateEvent.Enter())
                    {
                        //if (Activate != null)
                        //    Activate(this, new WindowActivateEventArgs(wParam));
                    }
                }
            }


            return Win32Helper.CallNextHookEx(_windowHook, code, wParam, lParam);
        }

        #endregion
    }
}