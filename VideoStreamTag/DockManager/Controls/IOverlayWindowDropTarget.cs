﻿#region

using System.Windows;

#endregion

namespace DockManager.Controls
{
    internal interface IOverlayWindowDropTarget
    {
        #region Public

        Rect ScreenDetectionArea { get; }

        OverlayWindowDropTargetType Type { get; }

        #endregion
    }
}