﻿#region

using System;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Imaging;

#endregion

namespace DockManager.Converters
{
    public class UriSourceToBitmapImageConverter : IValueConverter
    {
        #region Public

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return Binding.DoNothing;
            //return (Uri)value;
            return new Image() {Source = new BitmapImage((Uri) value)};
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}