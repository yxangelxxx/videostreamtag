﻿#region

using System;
using System.Windows.Controls;
using System.Windows.Data;
using DockManager.Layout;

#endregion

namespace DockManager.Converters
{
    [ValueConversion(typeof(AnchorSide), typeof(Orientation))]
    public class AnchorSideToOrientationConverter : IValueConverter
    {
        #region Public

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            AnchorSide side = (AnchorSide) value;
            if (side == AnchorSide.Left ||
                side == AnchorSide.Right)
                return Orientation.Vertical;

            return Orientation.Horizontal;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}