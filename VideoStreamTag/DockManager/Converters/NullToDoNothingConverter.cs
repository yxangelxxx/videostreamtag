﻿#region

using System;
using System.Windows.Data;

#endregion

namespace DockManager.Converters
{
    public class NullToDoNothingConverter : IValueConverter
    {
        #region Public

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return Binding.DoNothing;

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}