﻿#region

using System;
using DockManager.Layout;

#endregion

namespace DockManager
{
    internal class LayoutEventArgs : EventArgs
    {
        #region Construct

        public LayoutEventArgs(LayoutRoot layoutRoot)
        {
            LayoutRoot = layoutRoot;
        }

        #endregion

        #region Public

        public LayoutRoot LayoutRoot { get; private set; }

        #endregion
    }
}