﻿#region

using System;
using System.Windows.Media;

#endregion

namespace VideoStreamTag.Helpers
{
    public static class ColorHelper
    {
        #region Public

        public static Brush ToBrush(this string str)
        {
            var brushConverter = new BrushConverter();
            var brush = (Brush) brushConverter.ConvertFrom(str);
            if (brush != null)
                return brush;

            throw new Exception("Не удалось конвертировать строку " + str + " в SolidColorBrush");
        }

        public static SolidColorBrush ToSolidColorBrush(this string str)
        {
            var brushConverter = new BrushConverter();
            var brush = (SolidColorBrush) brushConverter.ConvertFrom(str);
            if (brush != null)
                return brush;

            throw new Exception("Не удалось конвертировать строку " + str + " в SolidColorBrush");
        }

        #endregion
    }
}