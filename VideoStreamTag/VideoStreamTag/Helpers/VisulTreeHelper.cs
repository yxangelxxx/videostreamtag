﻿#region

using System.Net;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

#endregion

namespace VideoStreamTag.Helpers
{
    public static class VisulTreeHelper
    {
        #region Public

        public static T FindVisualChild<T>(DependencyObject obj) where T : DependencyObject
        {
            for (var i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                var child = VisualTreeHelper.GetChild(obj, i);
                if (child is T variable)
                    return variable;

                var childOfChild = FindVisualChild<T>(child);
                if (childOfChild != null)
                    return childOfChild;
            }

            return null;
        }

        public static T FindVisualParent<T>(DependencyObject sender) where T : DependencyObject
        {
            if (sender == null)
            {
                return null;
            }

            var parent = VisualTreeHelper.GetParent(sender);

            if (parent is T)
            {
                return VisualTreeHelper.GetParent(sender) as T;
            }

            return FindVisualParent<T>(parent);
        }

        public static T FindChild<T>(this DependencyObject parent, string name = "") where T : DependencyObject
        {
            if (parent == null)
            {
                return default(T);
            }

            T foundChild = default(T);

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);

            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);

                T childType = child as T;
                if (childType != null)
                {
                    if (string.IsNullOrEmpty(name))
                    {
                        foundChild = childType;
                        return foundChild;
                    }

                    var frameworkElement = child as FrameworkElement;
                    if (frameworkElement != null && frameworkElement.Name == name)
                    {
                        foundChild = childType;
                        return foundChild;
                    }
                }

                foundChild = FindChild<T>(child, name);
                if (foundChild != null) break;
            }

            return foundChild;
        }

        public static T FindChildByName<T>(this DependencyObject sender, string name) where T : FrameworkElement
        {
            if (sender == null)
            {
                return null;
            }

            var t = typeof(T);

            int childrenCount = VisualTreeHelper.GetChildrenCount(sender);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(sender, i);
                var ast = child as T;

                if (ast == null && VisualTreeHelper.GetChildrenCount(child) > 0)
                {
                    var res = FindChildByName<T>(child, name);
                    if (res != null)
                    {
                        return res;
                    }
                }

                if (ast != null && ast.Name == name)
                {
                    return ast;
                }
            }

            return null;
        }

        public static T FindVisualParent<T>(this DependencyObject sender, string name = null) where T : DependencyObject
        {
            if (sender == null)
            {
                return null;
            }

            var parent = VisualTreeHelper.GetParent(sender) as FrameworkElement;

            if (!string.IsNullOrEmpty(name))
            {
                if (parent is T && !string.IsNullOrEmpty(name) && parent.Name == name)
                {
                    return VisualTreeHelper.GetParent(sender) as T;
                }
            }
            else
            {
                if (parent is T)
                {
                    return VisualTreeHelper.GetParent(sender) as T;
                }
            }


            return FindVisualParent<T>(parent);
        }

        public static string GetWebHeaderCollectionValue(this WebHeaderCollection headers, string name)
        {
            for (int i = 0; i < headers.Count; ++i)
                if (headers.Keys[i] == name)
                {
                    return headers[i];
                }

            return null;
        }

        public static void SetBinding(this object source, string path, DependencyObject target, DependencyProperty dependencyProperty)
        {
            Binding binding = new Binding
            {
                Source = source,
                Path = new PropertyPath(path),
                Mode = BindingMode.TwoWay
            };

            BindingOperations.SetBinding(target, dependencyProperty, binding);
        }

        #endregion
    }
}