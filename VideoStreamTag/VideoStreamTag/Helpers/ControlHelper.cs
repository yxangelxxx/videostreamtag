﻿#region

using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

#endregion

namespace VideoStreamTag.Helpers
{
    public static class ControlHelper
    {
        #region Public

        public static Size MeasureString(string text, TextBlock textBlock)
        {
            var formattedText = new FormattedText(
                text,
                CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                new Typeface(textBlock.FontFamily, textBlock.FontStyle, textBlock.FontWeight, textBlock.FontStretch),
                textBlock.FontSize,
                Brushes.Black,
                new NumberSubstitution(), TextFormattingMode.Display);

            return new Size(formattedText.Width, formattedText.Height);
        }

        public static string GetName(this DependencyObject dependencyObject)
        {
            return dependencyObject?.GetValue(FrameworkElement.NameProperty).ToString();
        }

        #endregion
    }
}