﻿#region

using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;

#endregion

namespace VideoStreamTag.Behaviors
{
    public class BehaviorScrollBar : Behavior<ScrollViewer>
    {
        #region Private methods

        private void ParentOnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            var scv = sender as ScrollViewer;
            if (scv == null) return;
            scv.ScrollToVerticalOffset(scv.VerticalOffset - e.Delta);
            e.Handled = true;
        }

        #endregion

        #region Protected Методы

        protected override void OnAttached()
        {
            if (AssociatedObject is ScrollViewer scrollViewer)
            {
                scrollViewer.PreviewMouseWheel += ParentOnMouseWheel;
            }
        }

        protected override void OnDetaching()
        {
            if (AssociatedObject is ScrollViewer scrollViewer)
            {
                scrollViewer.PreviewMouseWheel -= ParentOnMouseWheel;
            }
        }

        #endregion
    }
}