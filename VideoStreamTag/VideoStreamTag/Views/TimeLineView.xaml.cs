﻿#region

using System.Windows.Controls;
using VideoStreamTag.ViewModels;

#endregion

namespace VideoStreamTag.Views
{
    /// <summary>
    /// Interaction logic for TimeLineView.xaml
    /// </summary>
    public partial class TimeLineView : UserControl
    {
        #region Private fields

        private TimeLineViewModel Vm { get; set; }

        #endregion

        #region Construct

        public TimeLineView()
        {
            InitializeComponent();
            Vm = new TimeLineViewModel();
            DataContext = Vm;
        }

        #endregion
    }
}