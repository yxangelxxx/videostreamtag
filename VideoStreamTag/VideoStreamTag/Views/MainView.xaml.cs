﻿#region

using System.IO;
using System.Windows;
using System.Windows.Controls;
using DockManager.Layout.Serialization;

#endregion

namespace VideoStreamTag.Views
{
    /// <summary>
    /// Логика взаимодействия для MainView.xaml
    /// </summary>
    public partial class MainView : UserControl
    {
        #region Private fields

        private string configPath = @".\\UI.config";

        #endregion

        #region Construct

        public MainView()
        {
            InitializeComponent();

            Loaded += OnLoaded;
            Unloaded += OnUnloaded;
        }

        #endregion

        #region Private methods

        private void OnUnloaded(object sender, RoutedEventArgs e)
        {
            var serializer = new XmlLayoutSerializer(NDockManager);
            serializer.Serialize(configPath);
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            var serializer = new XmlLayoutSerializer(NDockManager);
            serializer.LayoutSerializationCallback += (s, args) => { args.Content = args.Content; };

            if (File.Exists(configPath))
                serializer.Deserialize(configPath);
        }

        #endregion
    }
}