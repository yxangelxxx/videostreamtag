﻿#region

using System.Windows.Controls;
using VideoStreamTag.ViewModels;

#endregion

namespace VideoStreamTag.Views
{
    /// <summary>
    /// Логика взаимодействия для FootballGridView.xaml
    /// </summary>
    public partial class FootballGridView : UserControl
    {
        #region Construct

        public FootballGridView()
        {
            InitializeComponent();

            DataContext = new FootballGridModel();
        }

        #endregion
    }
}