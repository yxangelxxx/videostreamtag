﻿#region

using System;
using System.Windows.Controls;
using VideoStreamTag.Models.ListButtons;
using VideoStreamTag.ViewModels;

#endregion

namespace VideoStreamTag.Views
{
    /// <summary>
    /// Логика взаимодействия для ListButtonsView.xaml
    /// </summary>
    public partial class ListButtonsView : UserControl
    {
        #region Private fields

        private LIstRecButtonsModel Vm { get; set; }

        #endregion

        #region Construct

        public ListButtonsView()
        {
            InitializeComponent();

            Vm = new LIstRecButtonsModel();
            DataContext = Vm;
        }

        #endregion

        #region Messages

        private void ListButtons_OnRemoveButtonClickHandler(object sender, int idx)
        {
            Vm.ListRecButtons.RemoveAt(idx);
        }

        private void ListButtons_OnAddButtonClickHandler(object sender, EventArgs e)
        {
            Vm.ListRecButtons.Add(new PeriodRecButton("button22", "button32", 1, "20:20", "asdf"));
        }

        #endregion
    }
}