﻿#region

using System.Windows;
using System.Windows.Controls;

#endregion

namespace VideoStreamTag.Controls
{
    public class RecButton : Button
    {
        #region Private fields

        private State _curState = State.start;

        #endregion

        #region Construct

        static RecButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(RecButton), new FrameworkPropertyMetadata(typeof(RecButton)));
        }

        public RecButton()
        {
            Loaded += OnLoaded;
            Click += OnClick;
        }

        #endregion

        #region Private methods

        private void OnClick(object sender, RoutedEventArgs e)
        {
            if (_curState == State.start)
            {
                SetState(State.record);
            }
            else if (_curState == State.record)
            {
                SetState(State.stop);
            }
            else if (_curState == State.stop)
            {
                SetState(State.start);
            }
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            Style = (Style) FindResource("KRecButtonStyle");
            //            ItemContainerGenerator.StatusChanged += ItemContainerGeneratorOnStatusChanged;
        }

        private void SetState(State state)
        {
            _curState = state;

            switch (state)
            {
                case State.start:
                    VisualStateManager.GoToState(this, "StartVisualState", false);
                    break;
                case State.record:
                    VisualStateManager.GoToState(this, "RecVisualState", false);
                    break;
                case State.stop:
                    VisualStateManager.GoToState(this, "StopVisualState", false);
                    break;
            }
        }

        #endregion

        #region Other members

        public enum State
        {
            record,
            start,
            stop,
        }

        #endregion
    }
}