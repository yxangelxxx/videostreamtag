﻿#region

using System;
using System.Collections;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using VideoStreamTag.Helpers;

#endregion

namespace VideoStreamTag.Controls
{
    public class ListButtons : Control
    {
        #region Events

        public event EventHandler AddButtonClickHandler;
        public event EventHandler<int> RemoveButtonClickHandler;

        #endregion

        #region Static

        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register(nameof(ItemsSource), typeof(IEnumerable), typeof(ListButtons),
                new PropertyMetadata(""));

        #endregion

        #region Private fields

        private Button _addButton;

        private ListBox _listBox;

        #endregion

        #region Construct

        static ListButtons()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ListButtons), new FrameworkPropertyMetadata(typeof(ListButtons)));
        }

        public ListButtons()
        {
            Loaded += OnLoaded;
        }

        #endregion

        #region Public

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            GetParts();
        }

        public IEnumerable ItemsSource
        {
            get => (string) GetValue(ItemsSourceProperty);
            set
            {
                if (value == null)
                    ClearValue(ItemsSourceProperty);
                else
                    SetValue(ItemsSourceProperty, value);
            }
        }

        #endregion

        #region Private methods

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            Style = (Style) FindResource("StyListButtons");
        }

        private void ItemContainerGeneratorFirstInitOnStatusChanged(object sender, EventArgs e)
        {
            if (sender is ItemContainerGenerator containerGenerator &&
                containerGenerator.Status == GeneratorStatus.ContainersGenerated)
            {
                _listBox.ItemContainerGenerator.StatusChanged -= ItemContainerGeneratorFirstInitOnStatusChanged;

                for (var i = 0; i < _listBox.ItemContainerGenerator.Items.Count; i++)
                {
                    var item = _listBox.ItemContainerGenerator.Items[i];

                    var myListBoxItem = (ListBoxItem) _listBox.ItemContainerGenerator.ContainerFromItem(item);
                    var myContentPresenter = VisulTreeHelper.FindVisualChild<ContentPresenter>(myListBoxItem);
                    var removeButton = (Button) myContentPresenter.ContentTemplate.FindName("PART_RemoveButton", myContentPresenter);
                    removeButton.Click += RemoveButtonOnClick;
                }
            }
        }

        private void RemoveButtonOnClick(object sender, RoutedEventArgs e)
        {
            if (sender is Button button)
            {
                var listBoxItem = VisulTreeHelper.FindVisualParent<ListBoxItem>(button);
                var itemidx = _listBox.ItemContainerGenerator.IndexFromContainer(listBoxItem);
                RemoveButtonClickHandler?.Invoke(_listBox, itemidx);
            }
        }

        private void GetParts()
        {
            _listBox = (ListBox) Template.FindName("PART_ListButtons", this);
            if (_listBox == null)
                throw new NullReferenceException(nameof(ListButtons) + " PART_ListButtons not found");

            _listBox.ItemContainerGenerator.StatusChanged += ItemContainerGeneratorFirstInitOnStatusChanged;
            _listBox.ItemContainerGenerator.ItemsChanged += ItemContainerGeneratorOnItemsChanged;

            _addButton = (Button) Template.FindName("PART_AddButton", this);
            if (_addButton == null)
                throw new NullReferenceException(nameof(ListButtons) + " PART_AddButton not found");

            _addButton.Click += AddButtonOnClick;


            //            _listBox.ApplyTemplate();
            //            _timeLineScrollViewer = (ScrollViewer)_listBox.Template.FindName("PART_ScrollViewer", _listBox);
            //            _timeLineScrollViewer.MouseWheel += ScrollViewerOnMouseWheel;
            //            _timeLineScrollViewer.ScrollChanged += ScrollViewerOnScrollChanged;
            //
            //            _listBox.ItemContainerGenerator.StatusChanged += ItemContainerGeneratorFirstInitOnStatusChanged;
            //
            //            _headerScrollViewer = (ScrollViewer)Template.FindName("PART_HeaderScrollViewer", this);
            //
            //            _actionList = (ActionList)Template.FindName("PART_ActionList", this);
            //            if (_actionList == null)
            //                throw new NullReferenceException("TimeLine PART_ActionList not found");
            //
            //            _actionList.Loaded += ActionListOnLoaded;
        }

        private void ItemContainerGeneratorOnItemsChanged(object sender, ItemsChangedEventArgs e)
        {
            if (_listBox.ItemContainerGenerator.Status == GeneratorStatus.ContainersGenerated
                && e.Action == NotifyCollectionChangedAction.Add)
            {
                _listBox.ItemContainerGenerator.StatusChanged += ItemContainerGeneratorOnStatusChanged;
            }
        }

        private void ItemContainerGeneratorOnStatusChanged(object sender, EventArgs e)
        {
            if (_listBox.ItemContainerGenerator.Status == GeneratorStatus.ContainersGenerated)
            {
                _listBox.ItemContainerGenerator.StatusChanged -= ItemContainerGeneratorOnStatusChanged;

                var myListBoxItem =
                    (ListBoxItem) _listBox.ItemContainerGenerator.ContainerFromIndex(_listBox.ItemContainerGenerator.Items.Count - 1);
                var myContentPresenter = VisulTreeHelper.FindVisualChild<ContentPresenter>(myListBoxItem);
                var removeButton = (Button) myContentPresenter.ContentTemplate.FindName("PART_RemoveButton", myContentPresenter);
                removeButton.Click += RemoveButtonOnClick;
            }
        }

        private void AddButtonOnClick(object sender, RoutedEventArgs e)
        {
            AddButtonClickHandler?.Invoke(this, e);
        }

        #endregion
    }
}