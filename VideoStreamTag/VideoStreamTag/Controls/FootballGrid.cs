﻿#region

using System;
using System.Collections;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using VideoStreamTag.Helpers;
using VideoStreamTag.Models.FootballGrid;

#endregion

namespace VideoStreamTag.Controls
{
    public class FootballGrid : Control
    {
        #region Static

        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register(nameof(ItemsSource), typeof(IEnumerable), typeof(FootballGrid),
                new FrameworkPropertyMetadata(null, ItemsChanged));

        public static readonly DependencyProperty ButtonLeftNameProperty =
            DependencyProperty.Register(nameof(ButtonLeftName), typeof(string), typeof(FootballGrid),
                new FrameworkPropertyMetadata(null, ButtonLeftNameChanged));

        public static readonly DependencyProperty ButtonRightNameProperty =
            DependencyProperty.Register(nameof(ButtonRightName), typeof(string), typeof(FootballGrid),
                new FrameworkPropertyMetadata(null, ButtonRightNameChanged));

        #endregion

        #region Private fields

        private Button _buttonLeft;
        private Button _buttonRight;

        private Canvas _canvas;
        private Brush _footballGridFill;
        private Brush _footballGridStroke;

        private Brush _pointCircleFill;
        private Brush _pointCircleStrokeTeam1;
        private Brush _pointCircleStrokeTeam2;

        #endregion

        #region Construct

        static FootballGrid()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(FootballGrid), new FrameworkPropertyMetadata(typeof(FootballGrid)));
        }

        public FootballGrid()
        {
            Loaded += OnLoaded;
        }

        #endregion

        #region Public

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            GetParts();

            _canvas.SizeChanged += CanvasOnSizeChanged;
            _canvas.Loaded += CanvasOnLoaded;

            _buttonLeft.Content = ButtonLeftName;
            _buttonRight.Content = ButtonRightName;
        }

        public IEnumerable ItemsSource
        {
            get => (IEnumerable) GetValue(ItemsSourceProperty);
            set
            {
                if (value == null)
                    ClearValue(ItemsSourceProperty);
                else
                    SetValue(ItemsSourceProperty, value);
            }
        }

        public string ButtonLeftName
        {
            get => (string) GetValue(ButtonLeftNameProperty);
            set => SetValue(ButtonLeftNameProperty, value);
        }

        public string ButtonRightName
        {
            get => (string) GetValue(ButtonRightNameProperty);
            set => SetValue(ButtonRightNameProperty, value);
        }

        #endregion

        #region Private methods

        private static void ButtonLeftNameChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is FootballGrid footballGrid && e.NewValue is string str && !string.IsNullOrEmpty(str) &&
                footballGrid._buttonLeft != null)
            {
                footballGrid._buttonLeft.Content = str;
            }
        }

        private static void ButtonRightNameChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is FootballGrid footballGrid && e.NewValue is string str && !string.IsNullOrEmpty(str) &&
                footballGrid._buttonRight != null)
            {
                footballGrid._buttonRight.Content = str;
            }
        }

        private static void ItemsChanged(DependencyObject d, DependencyPropertyChangedEventArgs ev)
        {
            if (d is FootballGrid footballGrid && ev.NewValue is IEnumerable list && footballGrid._canvas != null)
            {
                CreatePointsCircles(list, (int) footballGrid._canvas.ActualWidth, (int) footballGrid._canvas.ActualHeight, footballGrid);
            }
        }


        private static void CreatePointsCircles(IEnumerable enumerable, int width, int height, FootballGrid footballGrid)
        {
            if (footballGrid._canvas != null)
            {
                foreach (var item in enumerable)
                {
                    if (item is PointCircle p)
                    {
                        CreatePointCircle((int) p.Pos.X, (int) p.Pos.Y, p.Num,
                            width, height, footballGrid._canvas,
                            footballGrid._pointCircleFill,
                            footballGrid._pointCircleStrokeTeam1);
                    }
                }
            }
        }

        private static void CreatePointCircle(int x, int y, int number, int width, int height, Canvas canvas, Brush fill, Brush stroke)
        {
            var d = Math.Min(width, height);
            var c = d / 10.0;
            var fontsize = 15 * d / 330;

            if (x + c > width || y + c > height)
                return;

            Canvas ncanva = new Canvas();

            Ellipse ellipse = new Ellipse();
            ellipse.Width = c;
            ellipse.Height = c;
            ellipse.Fill = fill;
            ellipse.Stroke = stroke;
            ellipse.StrokeThickness = 2;

            string text = number.ToString();
            TextBlock textBlock = new TextBlock();
            textBlock.Text = text;
            textBlock.Foreground = stroke;
            textBlock.FontSize = fontsize;

            var ht = ControlHelper.MeasureString(text, textBlock);

            Canvas.SetLeft(textBlock, c / 2.0 - ht.Width / 2.0 - 1);
            Canvas.SetTop(textBlock, c / 2.0 - ht.Height / 2.0 - 1);

            ncanva.Children.Add(ellipse);
            ncanva.Children.Add(textBlock);

            Canvas.SetLeft(ncanva, x);
            Canvas.SetTop(ncanva, y);

            canvas.Children.Add(ncanva);
        }


        private static void CreateShapes(int width, int height, Canvas canvas, Brush fill, Brush stroke)
        {
            int x = 4;
            int y = 4;
            int w = width - 6;
            int h = height - 6;


            CreateMainRect(x, y, w, h, canvas, fill, stroke);
            CreateDivEllipse(x, y, w, h, canvas, fill, stroke);
            CreateInRect1(x, y, w, h, canvas, fill, stroke);
            CreateInRect2(x, y, w, h, canvas, fill, stroke);
            CreateOutRect(x, y, w, h, canvas, fill, stroke);
            CreateCentreCircle(x, y, w, h, canvas, stroke);
        }

        private static void CreateDivEllipse(int x, int y, int width, int height, Canvas canvas, Brush fill, Brush stroke)
        {
            var c = Math.Min(width, height) / 6;

            var w = width / 6;

            Ellipse ellipse1 = new Ellipse();
            ellipse1.Width = c;
            ellipse1.Height = c;
            ellipse1.Stroke = stroke;

            Canvas.SetTop(ellipse1, y + (height / 2.0 - c / 2.0));
            Canvas.SetLeft(ellipse1, x + (w - c / 2.0) - 7);

            canvas.Children.Add(ellipse1);

            Ellipse ellipse2 = new Ellipse();
            ellipse2.Width = c;
            ellipse2.Height = c;
            ellipse2.Stroke = stroke;

            Canvas.SetTop(ellipse2, y + (height / 2.0 - c / 2.0));
            Canvas.SetLeft(ellipse2, x + (width - w - c / 2.0) + 6);

            canvas.Children.Add(ellipse2);
        }

        private static void CreateOutRect(int x, int y, int width, int height, Canvas canvas, Brush fill, Brush stroke)
        {
            var w = 2;
            var h = height / 6;
            var strokeThickness = 4;
            var offset = 2;

            Rectangle rect1 = new Rectangle();
            rect1.Width = w;
            rect1.Height = h;
            rect1.Stroke = stroke;
            rect1.Fill = fill;
            rect1.StrokeThickness = strokeThickness;

            Canvas.SetTop(rect1, y + (height / 2.0 - h / 2.0));
            Canvas.SetLeft(rect1, x - offset);

            canvas.Children.Add(rect1);

            Rectangle rect2 = new Rectangle();
            rect2.Width = w;
            rect2.Height = h;
            rect2.Stroke = stroke;
            rect2.Fill = fill;
            rect2.StrokeThickness = strokeThickness;

            Canvas.SetTop(rect2, y + (height / 2.0 - h / 2.0));
            Canvas.SetLeft(rect2, x + (width - (w + 1)) + offset);

            canvas.Children.Add(rect2);
        }

        private static void CreateCentreCircle(int x, int y, int width, int height, Canvas canvas, Brush stroke)
        {
            var c = Math.Min(width, height) / 4;

            Ellipse ellipse = new Ellipse();
            ellipse.Width = c;
            ellipse.Height = c;
            ellipse.Stroke = stroke;

            Canvas.SetTop(ellipse, y + (height / 2.0 - c / 2.0));
            Canvas.SetLeft(ellipse, x + (width / 2.0 - c / 2.0) - 1);

            canvas.Children.Add(ellipse);
        }

        private static void CreateInRect1(int x, int y, int width, int height, Canvas canvas, Brush fill, Brush stroke)
        {
            var w = width / 6;
            var h = height / 1.7;

            Rectangle rect1 = new Rectangle();
            rect1.Width = w;
            rect1.Height = h;
            rect1.Stroke = stroke;
            rect1.Fill = fill;
            rect1.StrokeThickness = 1;

            Canvas.SetTop(rect1, y + (height / 2.0 - h / 2.0));
            Canvas.SetLeft(rect1, x);

            canvas.Children.Add(rect1);

            Rectangle rect2 = new Rectangle();
            rect2.Width = w;
            rect2.Height = h;
            rect2.Stroke = stroke;
            rect2.Fill = fill;
            rect2.StrokeThickness = 1;

            Canvas.SetTop(rect2, y + (height / 2.0 - h / 2.0));
            Canvas.SetLeft(rect2, x + (width - (w + 1)));

            canvas.Children.Add(rect2);
        }

        private static void CreateInRect2(int x, int y, int width, int height, Canvas canvas, Brush fill, Brush stroke)
        {
            var w = width / 13;
            var h = height / 4;

            Rectangle rect1 = new Rectangle();
            rect1.Width = w;
            rect1.Height = h;
            rect1.Stroke = stroke;
            rect1.Fill = fill;
            rect1.StrokeThickness = 1;

            Canvas.SetTop(rect1, y + (height / 2.0 - h / 2.0));
            Canvas.SetLeft(rect1, x);

            canvas.Children.Add(rect1);

            Rectangle rect2 = new Rectangle();
            rect2.Width = w;
            rect2.Height = h;
            rect2.Stroke = stroke;
            rect2.Fill = fill;
            rect2.StrokeThickness = 1;

            Canvas.SetTop(rect2, y + (height / 2.0 - h / 2.0));
            Canvas.SetLeft(rect2, x + (width - (w + 1)));

            canvas.Children.Add(rect2);
        }

        private static void CreateMainRect(int x, int y, int width, int height, Canvas canvas, Brush fill, Brush stroke)
        {
            Rectangle rectMainLeft = new Rectangle();
            rectMainLeft.Width = width / 2.0;
            rectMainLeft.Height = height;
            rectMainLeft.Stroke = stroke;
            rectMainLeft.Fill = fill;
            rectMainLeft.StrokeThickness = 1;

            Canvas.SetTop(rectMainLeft, y);
            Canvas.SetLeft(rectMainLeft, x);

            canvas.Children.Add(rectMainLeft);


            Rectangle rectMainRight = new Rectangle();
            rectMainRight.Width = width / 2.0;
            rectMainRight.Height = height;
            rectMainRight.Stroke = stroke;
            rectMainRight.Fill = fill;
            rectMainRight.StrokeThickness = 1;

            Canvas.SetTop(rectMainRight, y);
            Canvas.SetLeft(rectMainRight, x + (width / 2 - 1));

            canvas.Children.Add(rectMainRight);
        }

        private void CanvasOnLoaded(object sender, RoutedEventArgs e)
        {
            CreateShapes((int) _canvas.ActualWidth, (int) _canvas.ActualHeight, _canvas, _footballGridFill, _footballGridStroke);
            CreatePointsCircles(ItemsSource, (int) _canvas.ActualWidth, (int) _canvas.ActualHeight, this);
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            var style = (Style) FindResource("FootballGridStyle");
            Style = style ?? throw new NullReferenceException(nameof(FootballGrid) + " FootballGridStyle");
        }

        private void GetParts()
        {
            _canvas = (Canvas) Template.FindName("PART_Canvas", this);
            if (_canvas == null)
                throw new NullReferenceException(nameof(FootballGrid) + " PART_Canvas not found");

            _pointCircleFill = (Brush) FindResource("KPointCircle_Fill_Cb");
            if (_pointCircleFill == null)
                throw new NullReferenceException(nameof(FootballGrid) + " KPointCircle_Fill_Cb not found");

            _pointCircleStrokeTeam1 = (Brush) FindResource("KPointCircle_StrokeTeam1_Cb");
            if (_pointCircleStrokeTeam1 == null)
                throw new NullReferenceException(nameof(FootballGrid) + " KPointCircle_StrokeTeam1_Cb not found");

            _pointCircleStrokeTeam2 = (Brush) FindResource("KPointCircle_StrokeTeam2_Cb");
            if (_pointCircleStrokeTeam2 == null)
                throw new NullReferenceException(nameof(FootballGrid) + " KPointCircle_StrokeTeam2_Cb not found");

            _footballGridFill = (Brush) FindResource("KFootballGrid_Fill_Cb");
            if (_footballGridFill == null)
                throw new NullReferenceException(nameof(FootballGrid) + " KFootballGrid_Fill_Cb not found");

            _footballGridStroke = (Brush) FindResource("KFootballGrid_Stroke_Cb");
            if (_footballGridStroke == null)
                throw new NullReferenceException(nameof(FootballGrid) + " KFootballGrid_Stroke_Cb not found");

            _buttonLeft = (Button) Template.FindName("PART_ButtonLeft", this);
            if (_buttonLeft == null)
                throw new NullReferenceException(nameof(FootballGrid) + " PART_ButtonLeft not found");

            _buttonRight = (Button) Template.FindName("PART_ButtonRight", this);
            if (_buttonRight == null)
                throw new NullReferenceException(nameof(FootballGrid) + " PART_ButtonRight not found");
        }

        private void CanvasOnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            _canvas.Children.Clear();
            CreateShapes((int) e.NewSize.Width, (int) e.NewSize.Height, _canvas, _footballGridFill, _footballGridStroke);
            CreatePointsCircles(ItemsSource, (int) e.NewSize.Width, (int) e.NewSize.Height, this);
        }

        #endregion
    }
}