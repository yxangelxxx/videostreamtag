﻿#region

using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;
using VideoStreamTag.Helpers;
using VideoStreamTag.Models.TimeLine;

#endregion

namespace VideoStreamTag.Controls
{
    public class TimeLine : Control
    {
        #region Const

        private const double TimeFragmentCorrectLeftRightButtons = 19 * 2;

        #endregion

        #region Static

        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register(nameof(ItemsSource), typeof(IEnumerable), typeof(TimeLine),
                new PropertyMetadata(""));

        #endregion

        #region Private fields

        private ActionList _actionList;
        private ScrollViewer _actionListScrollViewer;

        private Canvas _canvasHeader;

        private StateTimeFragment _currentStateTimeFragment = StateTimeFragment.stop;

        private ScrollViewer _headerScrollViewer;

        private bool _isMouseLeave;
        private ListBox _listBox;

        private Control _selectedElement;

        private Point _startMousePosition;
        private ScrollViewer _timeLineScrollViewer;

        #endregion

        #region Construct

        static TimeLine()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TimeLine), new FrameworkPropertyMetadata(typeof(TimeLine)));
        }

        public TimeLine()
        {
            Loaded += OnLoaded;
        }

        #endregion

        #region Public

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            GetParts();
        }

        public IEnumerable ItemsSource
        {
            get => (string) GetValue(ItemsSourceProperty);
            set
            {
                if (value == null)
                    ClearValue(ItemsSourceProperty);
                else
                    SetValue(ItemsSourceProperty, value);
            }
        }

        #endregion

        #region Private methods

        private static double GetTimeFragmentRealLength(TimeFragment timeFragment)
        {
            return timeFragment.Length + TimeFragmentCorrectLeftRightButtons;
        }

        private static double ConvertLenTimeFragmentHideToReal(double length)
        {
            return length + TimeFragmentCorrectLeftRightButtons;
        }

        private static double ConvertLenTimeFragmentRealToHide(double length)
        {
            return length - TimeFragmentCorrectLeftRightButtons;
        }

        private void ItemContainerGeneratorOnStatusChanged(object sender, EventArgs e)
        {
            if (sender is ItemContainerGenerator containerGenerator && containerGenerator.Status == GeneratorStatus.ContainersGenerated)
            {
                CreateTimeLineHeaderAndFragments();
            }
        }

        private void GetParts()
        {
            _canvasHeader = (Canvas) Template.FindName("PART_CanvasHeader", this);
            if (_canvasHeader == null)
                throw new NullReferenceException("TimeLine PART_CanvasHeader not found");

            _listBox = (ListBox) Template.FindName("PART_ListBox", this);
            if (_listBox == null)
                throw new NullReferenceException("TimeLine PART_ListBox not found");

            _listBox.ApplyTemplate();
            _timeLineScrollViewer = (ScrollViewer) _listBox.Template.FindName("PART_ScrollViewer", _listBox);
            _timeLineScrollViewer.ScrollChanged += ScrollViewerOnScrollChanged;

            _listBox.ItemContainerGenerator.StatusChanged += ItemContainerGeneratorOnStatusChanged;

            _headerScrollViewer = (ScrollViewer) Template.FindName("PART_HeaderScrollViewer", this);

            _actionList = (ActionList) Template.FindName("PART_ActionList", this);
            if (_actionList == null)
                throw new NullReferenceException("TimeLine PART_ActionList not found");

            _actionList.Loaded += ActionListOnLoaded;
        }

        private void ActionListOnLoaded(object sender, RoutedEventArgs e)
        {
            _actionList.ApplyTemplate();
            _actionListScrollViewer = (ScrollViewer) _actionList.Template.FindName("PART_ScrollViewer", _actionList);
            _actionListScrollViewer.ScrollChanged += ActionListScrollViewerOnScrollChanged;
        }

        private void ActionListScrollViewerOnScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            _timeLineScrollViewer?.ScrollToVerticalOffset(e.VerticalOffset);
        }

        private void ScrollViewerOnScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            _headerScrollViewer?.ScrollToHorizontalOffset(e.HorizontalOffset);
            _actionListScrollViewer?.ScrollToVerticalOffset(e.VerticalOffset);
        }

        private async void CreateTimeLineHeaderAndFragments()
        {
            await CreateTimeLineHeader();

            var dtTimeFragmentBaseState = (ControlTemplate) FindResource("DtTimeFragmentBaseState");
            var dtTimeFragmentMoveState = (ControlTemplate) FindResource("DtTimeFragmentMoveState");

            foreach (var item in _listBox.Items)
            {
                var timeLineModel = (TimeLineModel) item;

                if (timeLineModel == null)
                    continue;

                var canvas = GetCanvas(timeLineModel);
                if (canvas == null) continue;

                await CreateLines(canvas);
                await CreateFragment(timeLineModel.TimeFragments, canvas, dtTimeFragmentBaseState,
                    dtTimeFragmentMoveState);
            }
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            Style = (Style) FindResource("StyTimeLine");
            //            ItemContainerGenerator.StatusChanged += ItemContainerGeneratorOnStatusChanged;
            PreviewMouseMove += OnMouseMove;
            PreviewMouseUp += OnMouseUp;
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (_selectedElement != null &&
                e.LeftButton == MouseButtonState.Pressed &&
                _selectedElement.Tag is TimeFragment fragment &&
                _currentStateTimeFragment != StateTimeFragment.stop
            )
            {
                var canvaParent = (UIElement) VisualTreeHelper.GetParent(_selectedElement);
                var position = e.GetPosition(canvaParent);
                var npos = position.X - _startMousePosition.X;

                switch (_currentStateTimeFragment)
                {
                    case StateTimeFragment.changeWidthLeft:
                        if (!fragment.ParentTimeLine.FindIntersectsTimeFragmentLeft(fragment, npos))
                        {
                            if (ChangeElementWidth(fragment.Length + fragment.Position - npos))
                                Canvas.SetLeft(_selectedElement, npos);
                        }

                        break;
                    case StateTimeFragment.changeWidthRight:
                        if (!fragment.ParentTimeLine.FindIntersectsTimeFragmentRight(fragment, npos, fragment.Length))
                        {
                            ChangeElementWidth(fragment.Length - fragment.Position + npos);
                        }

                        break;

                    case StateTimeFragment.move:
                        using (Dispatcher.DisableProcessing())
                        {
                            if (!fragment.ParentTimeLine.FindIntersectsTimeFragment(fragment, npos, fragment.Length))
                            {
                                fragment.Position = Canvas.GetLeft(_selectedElement);
                                Canvas.SetLeft(_selectedElement, npos);
                            }
                        }

                        break;
                }
            }
        }

        private void OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (_currentStateTimeFragment != StateTimeFragment.stop && _selectedElement != null &&
                _selectedElement is Control control && control.Tag is TimeFragment fragment)
            {
                _selectedElement.ReleaseMouseCapture();

                fragment.Position = (int) Canvas.GetLeft(_selectedElement);
                fragment.Length = ConvertLenTimeFragmentRealToHide(_selectedElement.Width);

                if (_isMouseLeave)
                {
                    TimeFragmentBaseState(control, fragment);
                }

                _currentStateTimeFragment = StateTimeFragment.stop;
            }
        }

        private async Task CreateTimeLineHeader()
        {
            await Task.Factory.StartNew(delegate
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                    (ThreadStart) delegate
                    {
                        var timeLineTextHeaderBrush = (Brush) FindResource("KTimeLineTextHeader_Cb");
                        var timeLineLinesBrush = (Brush) FindResource("KTimeLineLines_Cb");

                        for (var i = 1; i < 1000; i++)
                        {
                            var text = i.ToString();

                            var textBlock = new TextBlock();
                            textBlock.Text = text;
                            textBlock.FontSize = 10;
                            textBlock.Background = new SolidColorBrush(Colors.Transparent);
                            textBlock.Foreground = timeLineTextHeaderBrush;

                            var stringWidth = ControlHelper.MeasureString(text, textBlock).Width / 2;
                            var x = i * 100;

                            Canvas.SetLeft(textBlock, x - Math.Floor(stringWidth));
                            Canvas.SetTop(textBlock, 0);

                            var line = new Line();
                            line.X1 = x;
                            line.X2 = x;
                            line.Y1 = 15;
                            line.Y2 = 20;
                            line.Stroke = timeLineLinesBrush;
                            line.StrokeThickness = 1;

                            _canvasHeader.Children.Add(textBlock);
                            _canvasHeader.Children.Add(line);

                            Dispatcher.Yield(DispatcherPriority.Render);
                        }
                    }
                );
            });
        }

        private Canvas GetCanvas(TimeLineModel item)
        {
            var myListBoxItem = (ListBoxItem) _listBox.ItemContainerGenerator.ContainerFromItem(item);
            if (myListBoxItem == null)
                throw new NullReferenceException(nameof(myListBoxItem));

            var myContentPresenter = VisulTreeHelper.FindVisualChild<ContentPresenter>(myListBoxItem);
            var canvas = (Canvas) myContentPresenter.ContentTemplate.FindName("NLineFragmentsCanvas", myContentPresenter);
            return canvas;
        }

        private async Task CreateLines(Canvas canvas)
        {
            await Task.Factory.StartNew(delegate
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                    (ThreadStart) delegate
                    {
                        var timeLineLinesBrush = (Brush) FindResource("KTimeLineLines_Cb");
                        for (var k = 1; k < 1000; k++)
                        {
                            var x = k * 100;

                            var line = new Line();
                            line.X1 = x;
                            line.X2 = x;
                            line.Y1 = 0;
                            line.Y2 = 30;
                            line.Stroke = timeLineLinesBrush;
                            line.StrokeThickness = 1;

                            canvas.Children.Add(line);
                        }
                    }
                );
            });
        }

        private async Task CreateFragment(ObservableCollection<TimeFragment> fragments, Canvas canvas,
            ControlTemplate fragmentBaseState, ControlTemplate fragmentMoveState)
        {
            await Task.Factory.StartNew(delegate
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                    (ThreadStart) delegate
                    {
                        foreach (var fragment in fragments)
                        {
                            fragment.ParentCanvas = canvas;

                            var timeFragmentControl = new Control();
                            timeFragmentControl.Width = GetTimeFragmentRealLength(fragment);
                            timeFragmentControl.Height = 30;
                            timeFragmentControl.Background = fragment.Color;
                            timeFragmentControl.Tag = fragment;

                            fragment.BaseStateTemplate = fragmentBaseState;
                            fragment.EnterStateTemplate = fragmentMoveState;
                            timeFragmentControl.MouseEnter += TimeFragmentControlOnMouseEnter;
                            timeFragmentControl.MouseLeave += TimeFragmentControlOnMouseLeave;
                            timeFragmentControl.PreviewMouseDown += TimeFragmentControlOnMouseDown;

                            timeFragmentControl.Template = fragment.BaseStateTemplate;

                            Canvas.SetLeft(timeFragmentControl, fragment.Position);
                            Canvas.SetTop(timeFragmentControl, 0);

                            fragment.Element = timeFragmentControl;

                            fragment.ParentCanvas.Children.Add(timeFragmentControl);
                        }
                    }
                );
            });
        }

        private void TimeFragmentControlOnMouseLeave(object sender, MouseEventArgs e)
        {
            _isMouseLeave = true;

            if (sender is Control control && control.Tag is TimeFragment fragment &&
                _currentStateTimeFragment == StateTimeFragment.stop)
            {
                TimeFragmentBaseState(control, fragment);
            }
        }

        private void TimeFragmentBaseState(Control control1, TimeFragment timeFragment)
        {
            control1.Template = timeFragment.BaseStateTemplate;
            _selectedElement = null;
        }

        private void TimeFragmentControlOnMouseEnter(object sender, MouseEventArgs e)
        {
            _isMouseLeave = false;

            if (sender is Control control && control.Tag is TimeFragment fragment &&
                _currentStateTimeFragment == StateTimeFragment.stop)
            {
                TimeFragmentSeletedState(control, fragment);
            }
        }

        private void TimeFragmentSeletedState(Control control1, TimeFragment timeFragment)
        {
            control1.Template = timeFragment.EnterStateTemplate;
            _selectedElement = control1;
        }

        private void TimeFragmentControlOnMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (_currentStateTimeFragment == StateTimeFragment.stop &&
                e.LeftButton == MouseButtonState.Pressed)
            {
                var control = (Control) sender;

                var element = (UIElement) control.InputHitTest(e.GetPosition(control));
                var parentElement = VisualTreeHelper.GetParent(element);

                var name = parentElement.GetName();

                switch (name)
                {
                    case "NCenterMoveButtonCanvas":
                        _currentStateTimeFragment = StateTimeFragment.move;
                        break;

                    case "NLeftButtonCanva":
                        _currentStateTimeFragment = StateTimeFragment.changeWidthLeft;
                        break;

                    case "NRightButtonCanva":
                        _currentStateTimeFragment = StateTimeFragment.changeWidthRight;
                        break;

                    default:
                        throw new Exception(
                            "TimeFragmentControlOnMouseDown: Unable to determine the type of movement (NCenterMoveButtonCanvas, NLeftButtonCanva, NRightButtonCanva)");
                }

                _startMousePosition = e.GetPosition(control);
                _selectedElement.CaptureMouse();
            }
        }


        private bool ChangeElementWidth(double width)
        {
            if (width > 5.0)
            {
                _selectedElement.Width = ConvertLenTimeFragmentHideToReal(width);
                return true;
            }

            return false;
        }

        #endregion

        #region Other members

        private enum StateTimeFragment
        {
            changeWidthLeft,
            changeWidthRight,
            move,
            stop
        }

        #endregion
    }
}