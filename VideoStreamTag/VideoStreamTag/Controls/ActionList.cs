﻿#region

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

#endregion

namespace VideoStreamTag.Controls
{
    public class ActionList : ListBox
    {
        #region Construct

        static ActionList()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ActionList), new FrameworkPropertyMetadata(typeof(ActionList)));
        }

        public ActionList()
        {
            Loaded += OnLoaded;
        }

        #endregion

        #region Private methods

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            Style = (Style) FindResource("StyActionList");
//            ItemContainerGenerator.StatusChanged += ItemContainerGeneratorOnStatusChanged;
        }

        private void ItemContainerGeneratorOnStatusChanged(object sender, EventArgs e)
        {
            if (sender is ItemContainerGenerator containerGenerator &&
                containerGenerator.Status == GeneratorStatus.ContainersGenerated)
            {
                ItemContainerGenerator.StatusChanged -= ItemContainerGeneratorOnStatusChanged;

                for (var i = 0; i < ItemContainerGenerator.Items.Count; i++)
                {
                    var item = ItemContainerGenerator.Items[i];

                    var myListBoxItem = (ListBoxItem) ItemContainerGenerator.ContainerFromItem(item);
                }
            }
        }

        #endregion
    }
}