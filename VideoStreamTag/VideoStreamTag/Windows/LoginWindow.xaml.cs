﻿#region

using System;
using System.Windows;
using VideoStreamTag.Models.LoginWindow;
using VideoStreamTag.ViewModels;

#endregion

namespace VideoStreamTag.Windows
{
    /// <summary>
    /// Логика взаимодействия для LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        #region Events

        public event EventHandler<UserItem> UserSelectedEvent;

        #endregion

        #region Construct

        public LoginWindow()
        {
            InitializeComponent();

            DataContext = new LoginWindowViewModel();
        }

        #endregion

        #region Protected Методы

        protected virtual void OnUserSelectedEvent(UserItem e)
        {
            UserSelectedEvent?.Invoke(this, e);
        }

        #endregion

        #region Messages

        private void NEnterButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (NWaitComboBox.SelectedItem is UserItem userItem)
            {
                OnUserSelectedEvent(userItem);
            }
        }

        #endregion
    }
}