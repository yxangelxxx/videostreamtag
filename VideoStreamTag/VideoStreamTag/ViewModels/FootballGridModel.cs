﻿#region

using System.Collections.ObjectModel;
using System.Windows;
using VideoStreamTag.Models.FootballGrid;

#endregion

namespace VideoStreamTag.ViewModels
{
    public class FootballGridModel : ViewModelBase
    {
        #region Private fields

        private ObservableCollection<PointCircle> _listPointCircles;

        #endregion

        #region Construct

        public FootballGridModel()
        {
            ListPointCircles = new ObservableCollection<PointCircle>
            {
                new PointCircle(new Point(10, 10), 10),
                new PointCircle(new Point(60, 60), 10),
            };
        }

        #endregion

        #region Public

        public ObservableCollection<PointCircle> ListPointCircles
        {
            get => _listPointCircles;
            set => SetProperty(ref _listPointCircles, value);
        }

        #endregion
    }
}