﻿#region

using System.Linq;
using ThemesLib.InStat.Helpers.VirtualizingCollection;
using VideoStreamTag.Models.LoginWindow;
using VideoStreamTag.Properties;

#endregion

namespace VideoStreamTag.ViewModels
{
    public class LoginWindowViewModel : ViewModelBase
    {
        #region Private fields

        private int _selectedIndexForWaitComboBox;


        private string _selectedUserItemStringBind;

        #endregion

        #region Construct

        public LoginWindowViewModel()
        {
            var customerProvider = new UsersProvider();
            UserListSource = new AsyncVirtualizingCollection<UserItem>(customerProvider, 2, 300 * 1000);
            //            UserListSource = new AsyncVirtualizingCollection<UserItem>(new UsersProvider(), 20, 2 * 1000);


            if (Settings.Default.LAST_USER_ID != 0)
            {
                UserItem ui = customerProvider.MainList.FirstOrDefault(p => p.Id == Settings.Default.LAST_USER_ID);
                if (ui != null)
                {
                    SelectedUserItemStringBind = ui.ToString();
                }
            }
        }

        #endregion

        #region Public

        public AsyncVirtualizingCollection<UserItem> UserListSource { get; }

        public int SelectedIndexForWaitComboBox
        {
            get { return _selectedIndexForWaitComboBox; }
            set
            {
                if (!Equals(_selectedIndexForWaitComboBox, value))
                {
                    _selectedIndexForWaitComboBox = value;
                    OnPropertyChanged(nameof(SelectedIndexForWaitComboBox));
                }
            }
        }

        public string SelectedUserItemStringBind
        {
            get { return _selectedUserItemStringBind; }
            set
            {
                if (!Equals(_selectedUserItemStringBind, value))
                {
                    _selectedUserItemStringBind = value;
                    OnPropertyChanged(nameof(SelectedUserItemStringBind));
                }
            }
        }

        #endregion
    }
}