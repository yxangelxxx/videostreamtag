﻿#region

using System.Collections.Generic;
using System.Collections.ObjectModel;
using VideoStreamTag.Helpers;
using VideoStreamTag.Models.TimeLine;

#endregion

namespace VideoStreamTag.ViewModels
{
    public class TimeLineViewModel : ViewModelBase
    {
        #region Private fields

        private ObservableCollection<TimeLineModel> _listTime;

        #endregion

        #region Construct

        public TimeLineViewModel()
        {
            ListTime = new ObservableCollection<TimeLineModel>();

            TimeLineModel line0 = new TimeLineModel(1, "line0");
            line0.SetTimeFragments(new List<TimeFragment>());

            TimeLineModel line1 = new TimeLineModel(1, "play1");
            var list1 = new List<TimeFragment>
            {
                new TimeFragment(line1, 1, 50, "#FFCC3333".ToBrush()),
                new TimeFragment(line1, 130, 60, "#FFCC3333".ToBrush()),
                new TimeFragment(line1, 60, 150, "#FFCC3333".ToBrush())
            };

            line1.SetTimeFragments(list1);

            TimeLineModel line2 = new TimeLineModel(2, "play2");
            var list2 = new List<TimeFragment>
            {
                new TimeFragment(line2, 1, 50, "#FFCC3333".ToBrush()),
                new TimeFragment(line2, 130, 60, "#FFCC3333".ToBrush()),
                new TimeFragment(line2, 60, 150, "#FFCC3333".ToBrush())
            };

            line2.SetTimeFragments(list2);

            TimeLineModel line3 = new TimeLineModel(3, "play3");
            var list3 = new List<TimeFragment>
            {
                new TimeFragment(line3, 1, 50, "#FFCC3333".ToBrush()),
                new TimeFragment(line3, 130, 60, "#FFCC3333".ToBrush()),
                new TimeFragment(line3, 60, 150, "#FFCC3333".ToBrush())
            };

            line3.SetTimeFragments(list3);

//            ListTime.Add(line0);
            ListTime.Add(line1);
            ListTime.Add(line2);
            ListTime.Add(line3);
        }

        #endregion

        #region Public

        public ObservableCollection<TimeLineModel> ListTime
        {
            get => _listTime;
            set => SetProperty(ref _listTime, value);
        }

        #endregion
    }
}