﻿#region

using System.Collections.ObjectModel;
using VideoStreamTag.Models.ListButtons;

#endregion

namespace VideoStreamTag.ViewModels
{
    public class LIstRecButtonsModel : ViewModelBase
    {
        #region Private fields

        private ObservableCollection<PeriodRecButton> _listRecButtons;

        #endregion

        #region Construct

        public LIstRecButtonsModel()
        {
            ListRecButtons = new ObservableCollection<PeriodRecButton>
            {
                new PeriodRecButton("button1", "button2", 1, "20:20", "1 тайм"),
                new PeriodRecButton("button1", "button2", 1, "21:20", "2 тайм"),
                new PeriodRecButton("button1", "button2", 1, "00:20", "3 тайм"),
                new PeriodRecButton("button1", "button2", 1, "10:20", "4 тайм"),
            };
        }

        #endregion

        #region Public

        public ObservableCollection<PeriodRecButton> ListRecButtons
        {
            get => _listRecButtons;
            set => SetProperty(ref _listRecButtons, value);
        }

        #endregion
    }
}