﻿#region

using System.Windows;
using VideoStreamTag.Models.LoginWindow;
using VideoStreamTag.Windows;

#endregion

namespace VideoStreamTag
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region Private fields

        private MainWindow _mainWindow;

        #endregion

        #region Private methods

        private void LoginWindowOnUserSelectedEvent(object sender, UserItem e)
        {
            if (sender is LoginWindow loginWindow)
            {
                loginWindow.Hide();

                _mainWindow = new MainWindow();
                Current.MainWindow = _mainWindow;
                _mainWindow.Show();
            }
        }

        #endregion

        #region Protected Методы

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            Application.Current.ShutdownMode = ShutdownMode.OnMainWindowClose;

            LoginWindow loginWindow = new LoginWindow();
            loginWindow.UserSelectedEvent += LoginWindowOnUserSelectedEvent;
            Current.MainWindow = loginWindow;
            loginWindow.Show();
        }

        #endregion
    }
}