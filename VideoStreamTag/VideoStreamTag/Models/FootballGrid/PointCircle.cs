﻿#region

using System.Windows;

#endregion

namespace VideoStreamTag.Models.FootballGrid
{
    public class PointCircle
    {
        #region Construct

        public PointCircle(Point pos, int num)
        {
            Pos = pos;
            Num = num;
        }

        #endregion

        #region Public

        public Point Pos;
        public int Num;

        #endregion
    }
}