﻿namespace VideoStreamTag.Models.ListButtons
{
    public class PeriodRecButton
    {
        #region Construct

        public PeriodRecButton(string buttonName1, string buttonName2, int id, string periodTime, string comment)
        {
            ButtonName1 = buttonName1;
            ButtonName2 = buttonName2;
            Comment = comment;

            Id = id;
            PeriodTime = periodTime;
        }

        #endregion

        #region Public

        public override string ToString()
        {
            return ButtonName1 + ButtonName2;
        }

        public string ButtonName1 { get; set; }
        public string ButtonName2 { get; set; }
        public int Id { get; set; }
        public string PeriodTime { get; set; }
        public string Comment { get; set; }

        #endregion
    }
}