﻿#region

using System.Collections.Generic;
using ThemesLib.InStat.Providers;
using Uniso.InStat;

#endregion

namespace VideoStreamTag.Models.LoginWindow
{
    public class UsersProvider : WaitComboProvider<UserItem>
    {
        #region Public

        public override void CreateMainList()
        {
            List<User> usersList = Uniso.InStat.Web.MsSqlService.GetUserList();
            for (var index = 0; index < usersList.Count; index++)
            {
                User item = usersList[index];
                if (!string.IsNullOrEmpty(item.Login))
                {
                    UserItem pi = new UserItem(item.Id, item.Login, null);
                    MainList.Add(pi);
                    _filterList.Add(pi);
                }
            }

            //for (var index = 0; index < 10000; index++)
            //{
            //    UserItem pi = new UserItem(index, "asdf", null);
            //    MainList.Add(pi);
            //    _filterList.Add(pi);
            //}
        }

        #endregion
    }
}