﻿#region

using ThemesLib.InStat.Helpers.VirtualizingCollection;

#endregion

namespace VideoStreamTag.Models.LoginWindow
{
    public class UserItem : IVirtualItem
    {
        #region Construct

        public UserItem(int id, string name, object data)
        {
            Id = id;
            Name = name;
            //Data = data;
        }

        #endregion

        #region Public

        public override string ToString()
        {
            return Name;
        }

        public string Name { get; set; }
        public int Id { get; set; }

        #endregion
    }
}