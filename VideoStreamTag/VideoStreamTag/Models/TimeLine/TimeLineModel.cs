﻿#region

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

#endregion

namespace VideoStreamTag.Models.TimeLine
{
    public class TimeLineModel
    {
        #region Construct

        public TimeLineModel(int index, string nameOfFragments)
        {
            WidthTimeLine = 100;
            Index = index;
            NameOfFragments = nameOfFragments;
        }

        #endregion

        #region Public

        public bool FindIntersectsTimeFragmentRight(TimeFragment fragment, double npos, double nlen)
        {
            TimeFragment right = null;

            for (int i = 0; i < TimeFragments.Count; i++)
            {
                if (!TimeFragments[i].Equal(fragment)) continue;

                if (i < TimeFragments.Count - 1)
                {
                    right = TimeFragments[i + 1];
                }
            }

            if (right != null && npos + nlen >= right.X1)
            {
                return true;
            }

            return false;
        }

        public bool FindIntersectsTimeFragmentLeft(TimeFragment fragment, double npos)
        {
            TimeFragment left = null;

            for (int i = 0; i < TimeFragments.Count; i++)
            {
                if (!TimeFragments[i].Equal(fragment)) continue;

                if (i > 0)
                {
                    left = TimeFragments[i - 1];
                }
            }

            if (left != null && left.X2 >= npos)
            {
                return true;
            }

            return false;
        }

        public bool FindIntersectsTimeFragment(TimeFragment fragment, double npos, double nlen)
        {
            TimeFragment left = null;
            TimeFragment right = null;

            for (int i = 0; i < TimeFragments.Count; i++)
            {
                if (!TimeFragments[i].Equal(fragment)) continue;

                if (i > 0)
                {
                    left = TimeFragments[i - 1];
                }

                if (i < TimeFragments.Count - 1)
                {
                    right = TimeFragments[i + 1];
                }
            }

            if (left != null && left.X2 >= npos)
            {
                return true;
            }

            if (right != null && npos + nlen >= right.X1)
            {
                return true;
            }

            return false;
        }

        public void SetTimeFragments(List<TimeFragment> timeFragments)
        {
            switch (timeFragments.Count)
            {
                case 0:
                    return;
                case 1:
                    TimeFragments = new ObservableCollection<TimeFragment>(timeFragments);
                    return;
            }

            var list = timeFragments.OrderBy(x => x.Position).ToList();
            var res = new ObservableCollection<TimeFragment> {list[0]};

            for (var i = 1; i < list.Count(); i++)
            {
                list[i].Position = list[i - 1].Position + list[i - 1].Length + 1;
                res.Add(list[i]);
            }

            TimeFragments = res;
        }

        /// <summary>
        /// The number of the time line in the array
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// List of time fragment
        /// </summary>
        public ObservableCollection<TimeFragment> TimeFragments { get; set; }

        public string NameOfFragments { get; set; }

        public int WidthTimeLine { get; set; }

        #endregion
    }
}