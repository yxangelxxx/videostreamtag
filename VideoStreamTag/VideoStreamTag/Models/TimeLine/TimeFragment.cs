﻿#region

using System;
using System.Windows.Controls;
using System.Windows.Media;

#endregion

namespace VideoStreamTag.Models.TimeLine
{
    public class TimeFragment
    {
        #region Construct

        public TimeFragment(TimeLineModel parentTimeLine, int position, int length, Brush color)
        {
            ParentTimeLine = parentTimeLine;
            Position = position;
            Length = length;
            Color = color;
        }

        #endregion

        #region Public

        public bool Equal(TimeFragment timeFragment)
        {
            return Math.Abs(Position - timeFragment.Position) <= 0 && Math.Abs(Length - timeFragment.Length) <= 0;
        }

        public TimeLineModel ParentTimeLine { get; }

        public double X1 => (int) Position;
        public double X2 => X1 + Length;

        public double Position { get; set; }
        public double Length { get; set; }
        public Brush Color { get; set; }

        public Canvas ParentCanvas { get; set; }

        public ControlTemplate BaseStateTemplate { get; set; }
        public ControlTemplate EnterStateTemplate { get; set; }
        public Control Element { get; set; }

        #endregion
    }
}